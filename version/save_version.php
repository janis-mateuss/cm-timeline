<?php
/* For rights see LICENSE.TXT */

if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

if ($_SESSION['login_user'] === 'admin' && isset($_POST['module']) && isset($_POST['isUpdated']))
{	
	require('../h/postgres_cmp.php');

	$selectQ = "SELECT module FROM version WHERE module LIKE :module";

	try
	{
		$pdo = $pgc->prepare($selectQ);
		$pdo->bindValue(':module', $_POST['module'], PDO::PARAM_STR);
		$pdo->execute();
		$res = $pdo->fetchAll(PDO::FETCH_NUM);

		if ( $pdo->rowCount() > 0 )
		{
			$updateQ = "UPDATE version SET is_updated = :isUpdated, date_checked = :date_checked 
				WHERE module = :module";

			try
			{
				$pdo = $pgc->prepare($updateQ);
				$pdo->bindValue(':module', $_POST['module'], PDO::PARAM_STR);
				$pdo->bindValue(':isUpdated', $_POST['isUpdated'], PDO::PARAM_INT);
				$pdo->bindValue(':date_checked', date("Y-m-d")); 
				$pdo->execute();
				
			}
			catch(PDOException $e)
			{
			    $pgc = NULL;
			    die('error in gc function => ' . $e->getMessage());
			}	
		}
		else 
		{
			$insertQ = "INSERT INTO version (module, is_updated, date_checked) 
				VALUES (:module, :isUpdated, :date_checked)";
			
			try 
			{
				$pdo = $pgc->prepare($insertQ);
				$pdo->bindValue(':module', $_POST['module'], PDO::PARAM_STR);
				$pdo->bindValue(':isUpdated', $_POST['isUpdated'], PDO::PARAM_BOOL);
				$pdo->bindValue(':date_checked', date("Y-m-d"));
				$pdo->execute();
			}
			catch(PDOException $e)
			{
			    $pgc = NULL;
			    die('error in gc function => ' . $e->getMessage());
			}
		}

	}
	catch(PDOException $e)
	{
	    $pgc = NULL;
	    die('error in gc function => ' . $e->getMessage());
	}

	$pdo = NULL;
	$pgc = NULL;
}

?>