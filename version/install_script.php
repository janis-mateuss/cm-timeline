<?php
/* For rights see LICENSE.TXT */

const VERSION_FILE_PATH = '../version.txt';
const INSTALL_SCRIPTS_PATH = '../php/install_scripts';
$scriptFiles = scandir(INSTALL_SCRIPTS_PATH);

if (file_exists(VERSION_FILE_PATH)) {
    $systemVersion = file_get_contents(VERSION_FILE_PATH);

    foreach ($scriptFiles as $script) {
        if (strpos($script, 'script-') === false) {
            continue;
        }

        $scriptVersion = str_replace(array('script-', '.php'), '',  $script);

        if (version_compare($scriptVersion, trim($systemVersion), '>=' )) {
            include INSTALL_SCRIPTS_PATH  . DIRECTORY_SEPARATOR .  $script;
        }
    }
}
