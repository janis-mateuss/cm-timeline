<?php
/* For rights see LICENSE.TXT */

if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

if ($_SESSION['login_user'] === 'admin' && isset($_POST['module']))
{
	require('../h/postgres_cmp.php');

	$selectQ = "SELECT * FROM version WHERE module LIKE :module";

	try
	{
		$pdo = $pgc->prepare($selectQ);
		$pdo->bindValue(':module', $_POST['module'], PDO::PARAM_STR);
		$pdo->execute();
		$res = $pdo->fetchAll(PDO::FETCH_ASSOC);

		if ( $pdo->rowCount() > 0 )
		{
			if ($res[0]['date_checked'] !== date("Y-m-d") || !$res[0]['is_updated']) {

				$updateQ = "UPDATE version SET is_updated = false, date_checked = :date_checked 
					WHERE module = :module";
				try
				{
					$pdo = $pgc->prepare($updateQ);
					$pdo->bindValue(':date_checked', date("Y-m-d"));
					$pdo->bindValue(':module', 'core', PDO::PARAM_STR);
					$pdo->execute();

					echo json_encode(array('status' => 1));
					
				}
				catch(PDOException $e)
				{
				    $pgc = NULL;
				    die('error in gc function => ' . $e->getMessage());
				}	
			}
			else {
				echo json_encode(array('status' => 0));
			}
		}
		else 
		{
			$insertQ = "INSERT INTO version (module, is_updated, date_checked) 
				VALUES (:module, true, :date_checked)";	
			
			try 
			{
				$pdo = $pgc->prepare($insertQ);
				$pdo->bindValue(':module', $_POST['module'], PDO::PARAM_STR);
				$pdo->bindValue(':date_checked', date("Y-m-d"));
				$pdo->execute();

				echo json_encode(array('status' => 0));
			}
			catch(PDOException $e)
			{
			    $pgc = NULL;
			    die('error in gc function => ' . $e->getMessage());
			}
		}
	}
	catch(PDOException $e)
	{
	    $pgc = NULL;
	    die('error in gc function => ' . $e->getMessage());
	}

	$pdo = NULL;
	$pgc = NULL;
}
?>