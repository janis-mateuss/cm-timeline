<?php  
/* For rights see LICENSE.TXT */

session_start();
ob_start();

$GIT_DIR = "";
$PROJECT_DIR = "";

if ($_SESSION['login_user'] === 'admin') {
	require('../h/postgres_cmp.php');

	$selectQ = "SELECT config_name, config_value FROM cm_config WHERE config_name LIKE 'vc_path' OR config_name LIKE 'project_path'";

	try {
		$pdo = $pgc->prepare($selectQ);
		$pdo->execute();
		$res = $pdo->fetchAll(PDO::FETCH_ASSOC);

		if ( $pdo->rowCount() > 0 ) {
			foreach ($res as $key => $value) {
				if ($res[$key]['config_name'] === 'vc_path') {
					$GIT_DIR = $res[$key]['config_value'];
				} else if ($res[$key]['config_name'] === 'project_path') {
					$PROJECT_DIR = $res[$key]['config_value'];
				}
			}
		}
	} catch(PDOException $e) {
	    $pgc = NULL;
	    die('error in gc function => ' . $e->getMessage());
	}

	$pdo = NULL;
	$pgc = NULL;
}

if ($GIT_DIR != "" && $PROJECT_DIR != "") {
	if (isset($_POST['action']) && $_POST['action'] === 'check' && $_SESSION['login_user'] == 'admin' ) {
		passthru('"' . $GIT_DIR . '" --login -c "cd ' . $PROJECT_DIR . ' && git fetch && git log ..origin/master"');
		$output = ob_get_clean();

		if (strpos($output, 'Author:') !== false && strpos($output, 'Date:') !== false) {
			echo json_encode(array('status' => 1, 'message' => 'Vai iegūt atjauninājumus?'));
		} else {
			echo json_encode(array('status' => 0, 'message' => 'Nav atjauninājumu.'));
		}
	} elseif ( isset($_POST['action']) && $_POST['action'] === 'update' && $_SESSION['login_user'] == 'admin' ) {
		exec('"' . $GIT_DIR . '" --login -c "cd ' . $PROJECT_DIR . ' && git pull origin master"');
	}

	include 'install_script.php';
} else {
	echo json_encode(array('status' => 0, 'message' => 'Kļūda. Atjauninājums nevar notikt.'));
}