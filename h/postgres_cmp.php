<?php

$env = include('env.php');

$pgc = new PDO(
    sprintf("%s:host=%s;dbname=%s", $env['db']['type'], $env['db']['host'], $env['db']['dbname']),
    $env['db']['username'],
    $env['db']['password']
);