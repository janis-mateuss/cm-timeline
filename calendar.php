<?php
/* For rights see LICENSE.TXT */
include('php/session.php');
if ($_SESSION['login_user'] != 'admin') {
    header("location: index.php");
    die();
}
$title = 'Kalendārs · Culimeta';
include("header.php");
?>
<div class="container-other">
    <div id="ajax_loader">
        <div class="spinner">
            <div class="rect1"></div>
            <div class="rect2"></div>
            <div class="rect3"></div>
            <div class="rect4"></div>
            <div class="rect5"></div>
        </div>
    </div>
    <div class="panel panel-primary" style="margin-top: 20px;">
        <div class="panel-heading">Kalendārs</div>
        <div class="panel-body">
            <div class="form-inline">
                <div class="form-group">
                    <label for="bttn-add-year">Pievienot gadu</label>
                    <div class="form-inline">
                        <input type="text" class="form-control" id="add-year" maxlength="4" placeholder="Gads">
                        <button type="submit" class="btn btn-default" id="bttn-add-year" style="margin-right: 20px;">
                            Labi
                        </button>
                    </div>
                </div>

                <div class="form-group">
                    <label for="bttn-show-year">Parādīt gadu</label>
                    <div class="form-inline">
                        <select class="form-control" id="getYear">
                            <?php
                            require('h/postgres_cmp.php');

                            $selectQ = "SELECT DISTINCT EXTRACT(YEAR FROM week_day) FROM calendar ORDER BY EXTRACT(YEAR FROM week_day) ASC";

                            try {
                                $pdo = $pgc->prepare($selectQ);
                                $pdo->execute();
                                $res = $pdo->fetchAll(PDO::FETCH_NUM);

                                foreach ($res as $key => $value) {
                                    // id this is current year then set it as combo box default value
                                    if (date('Y') == $value[0]) {
                                        echo '<option value="' . $value[0] . '" selected="selected">' . $value[0] . '</option>';
                                    } else {
                                        echo '<option value="' . $value[0] . '">' . $value[0] . '</option>';
                                    }
                                }

                            } catch (PDOException $e) {
                                $pgc = NULL;
                                die('error in gc function => ' . $e->getMessage());
                            }
                            $pdo = NULL;
                            $pgc = NULL;
                            ?>
                        </select>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <div id="message"></div>

    <div class="table-responsive" style="margin-top: 20px;">
        <table class="table table-striped" id="calendarTable">
            <h2>Kalendārs</h2>
            <thead class="persist-header">
            <tr>
                <th>#</th>
                <th>Datums</th>
                <th>1. maiņa 22:00 - 06:00</th>
                <th>2. maiņa 06:00 - 14:00</th>
                <th>3. maiņa 14:00 - 22:00</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $_currentYear = date("Y");
            include('php/calendar_load.php');
            ?>
            </tbody>
        </table>
    </div>
</div><!-- main container -->

<?php include("footer.html"); ?>
<script type="text/javascript" src="js/jquery.toaster.js"></script>
<script type="text/javascript" src="js/calendar.js"></script>
<script type="text/javascript" src="js/totop.js"></script>
</html>