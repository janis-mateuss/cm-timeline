<?php
/* For rights see LICENSE.TXT */
include('php/session.php');
$title = 'Mašīnas · Culimeta';
include("header.php");
?>
<div class="container-other">

    <div class="table-responsive">
        <div id="ajax_loader">
            <div class="spinner">
                <div class="rect1"></div>
                <div class="rect2"></div>
                <div class="rect3"></div>
                <div class="rect4"></div>
                <div class="rect5"></div>
            </div>
        </div>

        <div class="form-inline" style="margin: 0px auto; border-bottom: 1px solid #DDD;">
            <label for="bttn-add-product" id="labelProduct">Mašīnas
                <?php if ($_SESSION['login_user'] == 'admin'): ?>
                <button type="button" class="btn btn-default" id="bttn-add-product"><span
                            class="glyphicon glyphicon-plus" aria-hidden="true"></span></button>
                <?php endif; ?>
            </label>
        </div>

        <div id="message"></div>

        <table class="table table-striped machines-table" id="productsTable">
            <thead class="persist-header">
            <tr>
                <th>#</th>
                <th>Nosaukums</th>
                <th>Apraksts</th>
                <th>Aktīvs</th>
                <th>Grupēts</th>
                <th style="width: 50px;"></th>
                <th style="width: 50px;"></th>
            </tr>
            </thead>
            <tbody>
            <?php
            include('php/machine_load.php');
            ?>
            </tbody>
        </table>
    </div>
</div><!-- main container -->

<?php include("footer.html"); ?>

<script type="text/javascript" src="js/jquery.toaster.js"></script>
<script type="text/javascript" src="js/machines.js"></script>
<script type="text/javascript" src="js/totop.js"></script>

</html>