<?php /* For rights see LICENSE.TXT */ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
    <meta charset="UTF-8">
    <meta name="robots" content="noindex, nofollow" />
    <meta name="viewport" content="width=device-width, user-scalable=no"/>
    <!-- "position: fixed" fix for Android 2.2+ -->
    <link rel="icon" href="css/images/logo.ico" type="image/ico"/>
    <link rel="stylesheet" href="css/style.css" type="text/css" media="screen"/>
    <link rel="stylesheet" media="screen" href="bootstrap/css/bootstrap.min.css">
    <link href="datepicker/jquery.datepick.css" rel="stylesheet" media="screen">
    <script src="js/jquery-1.11.3.min.js"></script>
    <script type="text/javascript" src="js/utils.js"></script>
    <title><?php echo $title; ?></title>
</head>

<body>
<nav class="navbar navbar-default navbar-fixed-top" id="mainNavBar">
    <div class="container-fluid">
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <div class="navbar-header">
                <a class="navbar-brand" href="planning.php"><img src="css/images/logoLogin.png"></span></a>
            </div>

            <ul class="nav navbar-nav">
                <?php if ($_SESSION['login_user'] !== 'uzlimes'): ?>
                    <li class="main-menu-drawer">
                        <a href="#"
                           class="dropdown-toggle"
                           data-toggle="dropdown"
                           role="button"
                           aria-haspopup="true"
                           aria-expanded="false">
                            <button style="display: block" type="button" class="navbar-toggle collapsed"
                                    aria-expanded="false">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <span>Izvēle</span>
                        </a>
                        <ul class="dropdown-menu">
                            <?php if ($_SESSION['login_user'] !== 'uzlimes'): ?>
                                <li><a href="products.php">Produkti</a></li>
                            <?php endif; ?>
                            <?php if ($_SESSION['login_user'] === 'admin'): ?>
                                <li><a href="machines.php">Mašīnas</a></li>
                                <li><a href="calendar.php">Kalendārs</a></li>
                            <?php endif; ?>
                        </ul>
                    </li>
                <?php endif; ?>


                <?php if ($_SESSION['login_user'] === 'admin'): ?>
                    <li>
                        <a href="#"
                           class="dropdown-toggle"
                           data-toggle="dropdown"
                           role="button"
                           aria-haspopup="true"
                           aria-expanded="false">Ražošanas uzdevumi / plāni <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu">
                            <li><a href="texturing.php">Uzdevumi</a></li>
                            <li><a href="plan-summary.php">Plāni</a></li>
                        </ul>
                    </li>
                <?php endif;

                if ($_SESSION['login_user'] === 'uzlimes'): ?>
                    <li><a href="texturing.php">Ražošanas uzdevumi</a></li>
                <?php endif; ?>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li class="logout">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                       aria-expanded="false"><?php echo $_SESSION['login_user']; ?> <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="logout.php"><span class="glyphicon glyphicon glyphicon-off"></span>Iziet</a></li>
                    </ul>
                </li>
            </ul>
                <?php $phpself = explode('/', $_SERVER['PHP_SELF']);
                if (end($phpself) == 'planning.php' || end($phpself) == 'planning') {
                    if ($_SESSION['login_user'] == 'admin') {
                        echo '<form class="navbar-form navbar-right" role="form"><div class="form-group">';
                        echo '<button type="button" class="btn btn-sm btn-danger" id="undo-gen-prod-bttn"><span class="glyphicon glyphicon-circle-arrow-left"></span> <span id="undo-text">Atcelt</span></button>
            <button type="button" class="btn btn-sm btn-success" id="save-bttn"><span class="glyphicon glyphicon-floppy-save"></span> Saglabāt</button>';
                        echo '</div></form>';

                        echo '<form class="navbar-form navbar-right" role="form"><div class="form-group">';
                        echo '<div class="dropdown" style="display:inline">
            <button class="btn btn-sm btn-default dropdown-toggle" type="button" id="create-plan-bttn" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
              <span class="glyphicon glyphicon glyphicon-list-alt"></span> Izveidot plānu
              <span class="caret"></span>
            </button>
            <ul class="dropdown-menu plan-menu" aria-labelledby="create-plan-bttn" style="margin-top: 8px;">
              <li class="plan-create"><a href="#">Pievienot jaunu<button type="button" class="btn btn-xs btn-default"><i class="glyphicon glyphicon-plus"></i></button></a></li>
            </ul>
          </div>';
                        echo '<button type="button" class="btn btn-sm btn-default" id="production-bttn"><span class="glyphicon glyphicon-time"></span> Ražošana</button>';
                        echo '</div></form>';
                    }
                } else {
                    if ($_SESSION['login_user'] == 'admin') {
                        if ((end($phpself) == 'calendar.php' || end($phpself) == 'calendar')) {
                            echo '<form class="navbar-form navbar-right" role="form"><div class="form-group">';
                            echo '<button type="button" class="btn btn-sm btn-success" id="save-bttn"><span class="glyphicon glyphicon-floppy-save"></span> Saglabāt</button>';
                            echo '</div></form>';
                        }
                    }
                }
                ?>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>
