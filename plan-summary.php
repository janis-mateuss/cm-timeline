<?php
/* For rights see LICENSE.TXT */
include('php/session.php');
$title = 'Plāni · Culimeta';
include("header.php");
?>
<div class="container-other">

    <div class="table-responsive" style="margin-top: 20px;">
        <div class="form-inline" style="margin: 0px auto; border-bottom: 1px solid #DDD;">
            <label for="bttn-add-product" id="labelProduct">Plānu kopsavilkumi
                <div class="form-inline" style="margin-left: 10px; float: right;">
                    <div class="input-daterange input-group col-xs-8" id="datepicker">
                        <span class="input-group-addon">no</span>
                        <input id="datepickerFrom" type="text" class="input-sm form-control popupDatepicker"
                               name="start"/>
                        <span class="input-group-addon">līdz</span>
                        <input id="datepickerFrom" type="text" class="input-sm form-control popupDatepicker"
                               name="end"/>
                    </div>
                    <button type="button" class="btn btn-sm btn-default" id="load-interval-bttn" data-placement="bottom"
                            title="">Atlasīt
                    </button>
                </div>
        </div>
        <p style="margin-top: -20px;text-align: right;font-size: 12px;color: #AAA;">Jaunākie 50 plāni</p>

        <div id="message"></div>

        <table class="table table-striped" id="productsTable">
            <div id="ajax_loader">
                <div class="spinner">
                    <div class="rect1"></div>
                    <div class="rect2"></div>
                    <div class="rect3"></div>
                    <div class="rect4"></div>
                    <div class="rect5"></div>
                </div>
            </div>
            <thead class="persist-header">
            <tr>
                <th style="width: 100px;">Pārskats</th>
                <th class="product-name">Plāna datums</th>
                <th>Plāna apzīmējums</th>
                <th>Pievienošanas datums</th>
                <th style="width: 50px;"></th>
                <th style="width: 50px;"></th>
            </tr>
            </thead>
            <tbody>
            <?php
            include('php/plansummary_load.php');
            ?>
            </tbody>
        </table>
    </div>
</div><!-- main container -->


<div class="modal" id="plansummaryModal" data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Plāna kopsavilkums</h4>
            </div>
            <div class="modal-body">
                <table id="texturingHeaderTable">
                    <tr>
                        <td class="header-img"><img src="css/images/logoLogin.png"></td>
                        <td class="header-mid">FB-82-002T</td>
                        <td class="header-left">
				<span class="level-1">Aizpildīts no: ___.___.______.<br/>
				<span class="level-2">(DD . MM . GGGG )</span><br/>
				<span class="level-3">līdz  ___.___.______.</span><br/>
				<span class="level-4">(DD . MM . GGGG )</span>
				</span>
                        </td>
                    </tr>
                </table>

                <table id="footer">
                    <tr>
                        <td class="left-foot">SIA "Culimeta Baltics" formulārs<br/>
                            Pavairot un nodot uzņēmumam nepiederošām personām, arī daļēji, tikai ar rakstisku uzņēmuma
                            vadības atļauju.
                        </td>
                        <td class="right-foot"><b><span class="revision-info">Rev. 10.08.2011</span></b></td>
                    </tr>
                </table>
                <div class="summary-body"></div>
            </div>
            <div class="modal-footer">
                <button style="margin-right: 20px;" type="button" class="btn btn-default" data-dismiss="modal">Aizvērt
                </button>
                <button id="print-bttn" type="button" class="btn btn btn-default">Drukāt <span
                            class="glyphicon glyphicon-print" aria-hidden="true"></span></button>
                <button type="button" class="btn btn-success" id="bttn-save-modal">Saglabāt</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<?php include("footer.html"); ?>
<link rel="stylesheet" href="css/paper-header.css" type="text/css" media="screen"/>
<link rel="stylesheet" href="css/print.css" type="text/css" media="print"/>
<link rel="stylesheet" href="css/paper-header-print.css" type="text/css" media="print"/>
<script type="text/javascript" src="datepicker/jquery.plugin.js"></script>
<script type="text/javascript" src="datepicker/jquery.datepick.min.js"></script>
<script type="text/javascript" src="datepicker/jquery.datepick-lv.js"></script>
<script type="text/javascript" src="js/jquery.toaster.js"></script>
<script type="text/javascript" src="js/plan-summary.js"></script>
<script type="text/javascript" src="js/totop.js"></script>

</html>