/* For rights see LICENSE.TXT */

// scope the jQuery
(function ($) {

    $(document).ready(function () {

        var openedTaskId;
        var confirmOptions = {
            title: "Dzēst uzdevumu?<br />(darbība ir neatgriezeniska) ",
            btnOkLabel: "Jā",
            btnCancelLabel: "Nē",
            singleton: true
        };

        $(".delete").confirmation(confirmOptions);

        $('.popupDatepicker').datepick({dateFormat: 'dd/mm/yyyy'});

        // shows delete icon in the end of the row
        $('#productsTable').on('click', 'tr', function (e) {
            if ($(this).hasClass('errorRow')) {
                $('#productsTable tr').removeClass('selectedRow');
            } else {
                $('#productsTable tr').removeClass('selectedRow, errorRow');
                $(this).addClass('selectedRow');
            }

            $('#productsTable tr').removeClass('selectedRow');
            $(this).addClass('selectedRow');
        });


        $("#productsTable").on('click', '.info', function () {
            openedTaskId = $(this).attr('name');

            $.post("php/plansummary_load.php", {id: $(this).attr('name')})
                // when post is finished
                .done(function (data) {
                    $('#plansummaryModal .modal-body .summary-body').html(data);
                    $('#plansummaryModal').modal('show');
                })
                .fail(function (data) {
                    showNotification("Nevar saglabāt datus.", 'danger');
                });
        });

        $('#load-interval-bttn').click(function () {
            var startDate = $('[name="start"]').val();
            var endDate = $('[name="end"]').val();
            // format to: mm/dd/yyyy input: dd/mm/yyyy
            startDate = startDate.split('/')[1] + '/' + startDate.split('/')[0] + '/' + startDate.split('/')[2];
            endDate = endDate.split('/')[1] + '/' + endDate.split('/')[0] + '/' + endDate.split('/')[2];

            var sd = new Date(startDate), ed = new Date(endDate);
            // if start date is less or equal to end date the draw table
            if (sd <= ed) {
                // get postgre time format
                startDate = startDate.split('/')[2] + '-' + startDate.split('/')[0] + '-' + startDate.split('/')[1];
                endDate = endDate.split('/')[2] + '-' + endDate.split('/')[0] + '-' + endDate.split('/')[1];

                $.post("php/plansummary_load.php", {startDate: startDate, endDate: endDate})
                    .done(function (data) {
                        $('#productsTable tbody').html(data);
                        $(".delete").confirmation(confirmOptions);
                    })
                    .fail(function (data) {
                        showNotification("Nevar ielādēt ražošanas uzdevumus.", 'danger');
                    });
            } else {
                showNotification('Nav korekti ievadīts datums.', 'danger');
            }
        });

        $("#bttn-save-modal").click(function () {
            $.post("php/plansummary_save.php", {
                id: openedTaskId,
                planContent: $('#plansummaryModal .modal-body .summary-body').html(),
                action: '_u'
            })
                .done(function (data) {
                    showNotification("Plāns saglabāts.", 'success');
                })
                .fail(function (data) {
                    showNotification("Nevar saglabāt datus.", 'danger');
                });
        });


        $('#productsTable').on('click', '.delete', function () {
            var clickedBtn = $(this);
            $.post("php/plansummary_save.php", {id: $(this).attr('name'), action: '_d'})
                .done(function (data) {
                    clickedBtn.parent().parent().remove();
                    showNotification("Plāns idzēsts.", 'success');
                })
                .fail(function (data) {
                    showNotification("Problēmas dzēšanā.", 'danger');
                });
        })

        $('#print-bttn').click(function () {
            window.print();
        })

        var clonedHeaderRow;
        // for static header on top. clone
        $("#productsTable").each(function () {
            clonedHeaderRow = $(".persist-header", this);
            clonedHeaderRow
                .before(clonedHeaderRow.clone())
                .css("width", clonedHeaderRow.width())
                .addClass("floatingHeader");
        });

        $(window)
            .scroll(UpdateTableHeaders)
            .trigger("scroll");
    })

    $(document).on('click', '#prioritiesTable tfoot button', function () {
        $('#prioritiesTable tbody').append($('#prioritiesTable tbody tr:last-child')[0].outerHTML);
        $('#prioritiesTable tbody tr:last-child td:last-child')
            .html(parseInt($('#prioritiesTable tbody tr:last-child td:last-child').html(), 10) + 1);
    });
})(jQuery);

function showNotification(text, type) {
    var title = '';
    if (type == 'danger') title = 'Kļūda! ';
    else if (type == 'success') title = 'Paziņojums! ';

    $.toaster({priority: type, title: title, message: text});
}

function isNumber(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
}

function UpdateTableHeaders() {
    $("#productsTable").each(function () {

        var el = $(this),
            offset = el.offset(),
            scrollTop = $(window).scrollTop(),
            floatingHeader = $(".floatingHeader", this)

        if ((scrollTop > offset.top - 36) && (scrollTop < offset.top - 36 + el.height())) {
            // get satic thead the same width as original table thead
            for (var i = 1; i <= 4; i++) {
                $('.floatingHeader th:nth-child(' + i + ')').width($('.persist-header th:nth-child(' + i + ')').width());
            }

            $('.floatingHeader').width($('.persist-header').width());
            floatingHeader.css('height', '40px');

            floatingHeader.css({
                "visibility": "visible"
            });
        } else {
            floatingHeader.css({
                "visibility": "hidden"
            });
        }
        ;
    });
}
