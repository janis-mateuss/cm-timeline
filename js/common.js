/* For rights see LICENSE.TXT */

$(document).ready(function () {

    // use: when post/get data then ajax gif loader shows
    $(document).ajaxStop(function (ev) {
        setTimeout(function () {
            if (ev.currentTarget.activeElement.id != 'product') {
                $("#ajax_loader").fadeOut(100);
            }
        }, 250)
    });

    $(document).ajaxStart(function (ev) {
        if (ev.currentTarget.activeElement.id != 'product' && !window.ajaxOff) {
            $("#ajax_loader").show();
        }
    });

    var loc = location.pathname.split('/')
    $('#mainNavBar').find('a[href="' + loc[loc.length - 1] + '"]').parent('li').addClass('active');
})