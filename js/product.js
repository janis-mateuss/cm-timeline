/* For rights see LICENSE.TXT */
var Product = {};

// use: when post/get data then ajax gif loader shows

// scope the jQuery
(function ($) {

    $(document).ready(function () {
        var curPid;

        var confirmOptions = {
            title: "Dzēst?<br />(darbība ir neatgriezeniska)",
            btnOkLabel: "Jā",
            btnCancelLabel: "Nē",
            singleton: true
        };


        // init yes/no box on delete
        $(".delete").confirmation(confirmOptions);

        $('#productsTable').on('click', 'tbody tr', function (e) {
            if ($(this).hasClass('errorRow')) {
                $('#productsTable tr').removeClass('selectedRow');
            } else {
                $('#productsTable tr').removeClass('selectedRow, errorRow');
                $(this).addClass('selectedRow');
            }

            $('#productsTable tr').removeClass('selectedRow');
            $(this).addClass('selectedRow');
        });

        $('body').on('focus', '[contenteditable]', function () {

        }).on('blur keyup paste input', '[contenteditable]', function () {
            var t = $(this).parent();
            var v1 = t.find('td:nth-child(3)')[0].innerHTML;
            var v2 = t.find('td:nth-child(4)')[0].innerHTML;
            var v3 = t.find('td:nth-child(6)')[0].innerHTML;
            var v4 = t.find('td:nth-child(8)')[0].innerHTML;
            var kgh = (v2 * 60 * v1 / 1000 / 1000 * v4 / 100).toFixed(3);
            t.find('td:nth-child(5)')[0].innerHTML = kgh;
            t.find('td:nth-child(7)')[0].innerHTML = (kgh * 1).toFixed(3);
        });


        // info modal for products
        $("#productsTable").on('click', '.info', function () {
            curPid = $(this).attr('name');
            $.post("php/products_info_load.php", {pid: curPid})
                // when post is finished
                .done(function (data) {
                    $('#productModal .modal-body').html(data);
                    $('#productModal').modal('show');
                })
                .fail(function (data) {
                    showNotification("Nevar ielādēt sarakstu.", 'danger');
                });
        });


        $('#productModal').on('hidden.bs.modal', function () {
            $('#overallComment').val('');
        })


        var clonedHeaderRow;
        // for static header on top. clone
        $("#productsTable").each(function () {
            clonedHeaderRow = $(".persist-header", this);
            clonedHeaderRow
                .before(clonedHeaderRow.clone())
                .css("width", clonedHeaderRow.width())
                .addClass("floatingHeader");
        });

        $(window)
            .scroll(UpdateTableHeaders)
            .trigger("scroll");

        // new added row html
        var newRow = '<tr><td><button type="button" class="btn btn-sm btn-success info" aria-label="Left Align" id="" name="new" disabled>' +
            '<span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span> Atvērt</button></td>' +
            '<td class="td1" contenteditable="true"></td><td class="td2" contenteditable="true">0</td>' +
            '<td class="td3" contenteditable="true">0</td><td class="td4" style="color: #888"></td>' +
            '<td class="td5" style="color: #888">1</td><td class="td6" style="color: #888">' +
            '</td><td class="td7" contenteditable="true">90</td>' +
            '<td class="td8" contenteditable="true">400</td><td class="td9" contenteditable="true">...</td>' +
            '<td class="td10" contenteditable="true">-</td><td class="td11" contenteditable="true">-</td>' +
            '<td><button type="button" class="btn btn-success save" aria-label="Left Align">' +
            '<span class="glyphicon glyphicon-floppy-disk"></span></button>' +
            '</td>' +
            '<td><button type="button" class="btn btn-danger delete" aria-label="Left Align">' +
            '<span class="glyphicon glyphicon-remove"></span></button>' +
            '</td></tr>';

        // add new row on + click
        $("#bttn-add-product").click(function () {
            $("#productsTable tbody").prepend(newRow);
            $(".delete").confirmation(confirmOptions);
        });


        $("#bttn-save-modal").click(function () {
            var Modal = {};
            Modal['info'] = $('#overallComment').val();
            // empty modal textarea
            $('#overallComment').val('');

            Modal['machines'] = {};

            $("#productsInfoTable tbody tr").each(function () {
                var cbLabel = $(this).find('td:nth-child(1) label').data('id');
                var comment = $(this).find('td:nth-child(2) input').val();
                // if checkbox is checked
                if ($(this).find('td:nth-child(1) input').is(":checked")) {
                    if (comment.trim() !== "") {
                        Modal['machines'][cbLabel] = {'comment': comment, 'check': true};
                    }
                } else {
                    Modal['machines'][cbLabel] = {'comment': comment, 'check': false};
                }
            });
            $.post("php/products_save.php", {modal: JSON.stringify(Modal), pid: curPid, action: '_u_modal'})
                // when post is finished
                .done(function (data) {
                    try {
                        var json = $.parseJSON(data);
                        if (json['error'] == 0) {
                            showNotification("Izmaiņas saglabātas.", 'success');
                        }
                    } catch (err) {
                        showNotification("Nevar saglabāt datus.", 'danger');
                    }
                })
                .fail(function (data) {
                    showNotification("Nevar saglabāt datus.", 'danger');
                });
        });

        $("#productsTable").on('click', '.save', function () {
            var row = $(this).parent().parent();
            var rowCells = $(this).parent().parent().find('td');
            var error = false;
            $.each(rowCells, function () {
                var td = $(this);
                if (td.hasClass('td2') || td.hasClass('td3') || td.hasClass('td7') || td.hasClass('td8')) {
                    if (!isNumber(td.text())) {
                        showNotification("Neatļauta vērtība '" + td.text() + "'", 'danger', row);
                        error = true;
                        return;
                    } else {
                        if (parseFloat(td.text()) < 0) {
                            showNotification("Neatļauta vērtība '" + td.text() + "'", 'danger', row);
                            return;
                        }
                    }
                }
            });
            if (error) return;

            var pid = ($(this).attr('name') !== undefined) ? $(this).attr('name') : "";
            var action = (pid == "") ? '_i' : '_u',
                jsonTable = {
                    name: row.find('.td1').text(),
                    weight: row.find('.td2').text(),
                    m_min: row.find('.td3').text(),
                    eff: row.find('.td7').text(),
                    pallet: row.find('.td8').text(),
                    descript: row.find('.td9').text(),
                    pid: pid,
                    stickerColor1: row.find('.td10').text(),
                    stickerColor2: row.find('.td11').text()
                };

            $.post("php/products_save.php", {table: JSON.stringify(jsonTable), action: action})
                // when post is finished
                .done(function (data) {
                    if (data != "") {
                        json = $.parseJSON(data);
                        if (json['error'] == 1) {
                            showNotification(json['msg'], 'danger', row);
                        } else {
                            showNotification("Produkts '" + row.find('.td1').text() + "' saglabāts.", 'success', row);
                            row.find('.info').prop('disabled', false);
                            row.find('.info, .save, .delete').attr('name', json['msg']);
                        }
                        return;
                    }
                    showNotification("Izmaiņas saglabātas", 'success', row);
                })
                .fail(function (data) {
                    showNotification("Nevar saglabāt datus.", 'danger');
                });

        });

        $("#productsTable").on('click', '.delete', function () {
            var pid = $(this).attr('name'),
                row = $(this).parent().parent();

            $.post("php/products_save.php", {pid: pid, action: '_d'})
                // when post is finished
                .done(function (data) {
                    showNotification("Produkts izdzēsts", 'success');
                    row.remove();
                })
                .fail(function (data) {
                    showNotification("Nevar izdzēst produktu.", 'danger');
                });
        });

        $("#generate-colors").click(function () {
            $('#ajax_loader').show();

            $.post("php/products_generate_colors.php", {action: '_gen'})
                .done(function (data) {
                    showNotification("Krāsas uzģenerētas.<br><br><strong>Pārlādē lapu, lai redzētu izmaiņas</strong>",
                        'success');
                })
                .fail(function (data) {
                    showNotification("Kaut kas nogājis greizi.", 'danger');
                })
                .always(function () {
                    $('#ajax_loader').hide();
                });
        });

        var $colors = $('#productsTable .td10, #productsTable .td11');

        $.each($colors, function () {
            new jscolor(this, {value: this.innerHTML, hash: true, uppercase: false, borderRadius: 2});
        });
    });

})(jQuery);

function showNotification(text, type, row) {
    var title = '';
    if (type == 'danger') {
        title = 'Kļūda! ';
        if (row !== undefined) row.removeClass('selectedRow').addClass('errorRow');
    } else if (type == 'success') {
        title = 'Paziņojums! ';
        if (row !== undefined) row.removeClass('errorRow').addClass('selectedRow');
    }

    $.toaster({priority: type, title: title, message: text});
}

function isNumber(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
}

function UpdateTableHeaders() {
    $("#productsTable").each(function () {

        var el = $(this),
            offset = el.offset(),
            scrollTop = $(window).scrollTop(),
            floatingHeader = $(".floatingHeader", this);

        if ((scrollTop > offset.top - 41) && (scrollTop < offset.top - 41 + el.height())) {
            // get satic thead the same width as original table thead
            for (var i = 1; i <= 12; i++) {
                $('.floatingHeader th:nth-child(' + i + ')').width($('.persist-header th:nth-child(' + i + ')').width());
            }
            ;
            $('.floatingHeader').width($('.persist-header').width());
            floatingHeader.css('height', '40px');

            floatingHeader.css({
                "visibility": "visible"
            });
        } else {
            floatingHeader.css({
                "visibility": "hidden"
            });
        }
        ;
    });
}
