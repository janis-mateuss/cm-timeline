/* For rights see LICENSE.TXT */

var dragRow, dragActioned = false;
function start(){
    dragRow = event.target;
    dragActioned = true;
}
function dragover(){
    var e = event;
    e.preventDefault();

    let children= Array.from(e.target.parentNode.parentNode.children);

    if (children.indexOf(e.target.parentNode) > children.indexOf(dragRow))
        jQuery(e.target).closest('tr')[0].after(dragRow);
    else
        jQuery(e.target).closest('tr')[0].before(dragRow);
}

(function ($) {
    $(document).ready(function () {
        var confirmOptions = {
            title: "Dzēst?<br />(darbība ir neatgriezeniska)",
            btnOkLabel: "Jā",
            btnCancelLabel: "Nē",
            singleton: true
        };


        // init yes/no box on delete
        $(".delete").confirmation(confirmOptions);

        $('#productsTable').on('click', 'tbody tr', function () {
            if ($(this).hasClass('errorRow')) {
                $('#productsTable tr').removeClass('selectedRow');
            } else {
                $('#productsTable tr').removeClass('selectedRow, errorRow');
                $(this).addClass('selectedRow');
            }

            $('#productsTable tr').removeClass('selectedRow');
            $(this).addClass('selectedRow');
        });

        var clonedHeaderRow;
        // for static header on top. clone
        $("#productsTable").each(function () {
            clonedHeaderRow = $(".persist-header", this);
            clonedHeaderRow
                .before(clonedHeaderRow.clone())
                .css("width", clonedHeaderRow.width())
                .addClass("floatingHeader");
        });

        $(window)
            .scroll(UpdateTableHeaders)
            .trigger("scroll");

        // new added row html
        var newRow = '<tr draggable="true" ondragstart="start()" ondragover="dragover()">' +
            '<td class="td3 not-editable"></td>' +
            '<td class="td2" contenteditable="true">---</td>' +
            '<td class="td5" contenteditable="true"></td>' +
            '<td class="td1"><div class="checkbox"><label><label class="checkbox-inline">' +
            '<input type="checkbox" value="1" checked>' +
            '</label></label></div></td>'+
            '<td class="td4"><select>' +
            '<option value="no">Nē</option>' +
            '<option value="yes">Jā</option>' +
            '<option value="start">Grupas sākums</option>' +
            '<option value="end">Grupas beigas</option>' +
            '</select></td>' +
            '<td><button type="button" class="btn btn-success save" aria-label="Left Align">' +
            '<span class="glyphicon glyphicon-floppy-disk"></span></button>' +
            '</td>' +
            '<td><button type="button" class="btn btn-danger delete" aria-label="Left Align">' +
            '<span class="glyphicon glyphicon-remove"></span></button>' +
            '</td></tr>';

        // add new row on + click
        $("#bttn-add-product").click(function () {
            $("#productsTable tbody").append(newRow);
            $(".delete").confirmation(confirmOptions);
        });

        $("#productsTable").on('click', '.save', function () {
            var row = $(this).parent().parent();
            var rowCells = row.find('td');
            var error = false;
            $.each(rowCells, function () {
                var td = $(this);

                if (td.hasClass('td2') && td.text().trim() === "") {
                    showNotification("Mašīnas nosaukumam nav piešķirta vērtība", 'danger', row);
                    error = true;
                    return;
                }
            });
            if (error) return;

            var id = ($(this).attr('name') !== undefined) ? $(this).attr('name') : "";
            var action = (id == "") ? '_i' : '_u',
                jsonTable = {
                    active: row.find('.td1 [type="checkbox"]').prop('checked'),
                    name: row.find('.td2').text().trim(),
                    grouped: row.find('.td4 select').val(),
                    description: row.find('.td5').text().trim(),
                    id: id
                };

            if (action === '_i') {
                jsonTable['sort_order'] = row.closest('tbody').find('tr').index(row[0]);
            }

            $.post("php/machine_save.php", {table: JSON.stringify(jsonTable), action: action})
                // when post is finished
                .done(function (data) {
                    if (data != "") {
                        json = $.parseJSON(data);
                        if (json['error'] == 1) {
                            showNotification(json['msg'], 'danger', row);
                        } else {
                            showNotification("Mašīna '" + row.find('.td2').text() + "' saglabāta.", 'success', row);
                            row.find('.save, .delete').attr('name', json['msg']);

                            // save order
                            if (dragActioned || action === '_i') {
                                dragActioned = false;
                                saveOrder();
                            }
                        }
                        return;
                    }
                    showNotification("Izmaiņas saglabātas", 'success', row);
                })
                .fail(function (data) {
                    showNotification("Nevar saglabāt datus.", 'danger');
                });
        });

        $("#productsTable").on('click', '.delete', function () {
            var id = $(this).attr('name'),
                row = $(this).parent().parent();


            $.post("php/machine_save.php", {id: id, action: '_d'})
                // when post is finished
                .done(function () {
                    showNotification("Mašīna izdzēsta", 'success');
                    row.remove();
                })
                .fail(function () {
                    showNotification("Nevar izdzēst mašīnu.", 'danger');
                });
        });
    });

    function saveOrder()  {
        var orderItems = $('.machines-table tbody tr td .btn-success[name]'),
            orderArray = {};

        $.each(orderItems, function (index) {
            orderArray[this.name] = $(this).closest('tbody').find('tr').index($(this).closest('tr')[0]);
        });

        $.post("php/machine_save.php", {table: JSON.stringify(orderArray), action: '_order'})
            // when post is finished
            .done(function () {
                showNotification("Mašīnu secība tika saglabāta.", 'success');
            })
            .fail(function (data) {
                showNotification("Nevar saglabāt datus.", 'danger');
                console.log(data);
            });

    }

})(jQuery);

function showNotification(text, type, row) {
    var title = '';
    if (type == 'danger') {
        title = 'Kļūda! ';
        if (row !== undefined) row.removeClass('selectedRow').addClass('errorRow');
    } else if (type == 'success') {
        title = 'Paziņojums! ';
        if (row !== undefined) row.removeClass('errorRow').addClass('selectedRow');
    }

    $.toaster({priority: type, title: title, message: text});
}

function isNumber(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
}

function UpdateTableHeaders() {
    $("#productsTable").each(function () {

        var el = $(this),
            offset = el.offset(),
            scrollTop = $(window).scrollTop(),
            floatingHeader = $(".floatingHeader", this);

        if ((scrollTop > offset.top - 41) && (scrollTop < offset.top - 41 + el.height())) {
            // get satic thead the same width as original table thead
            for (var i = 1; i <= 12; i++) {
                $('.floatingHeader th:nth-child(' + i + ')').width($('.persist-header th:nth-child(' + i + ')').width());
            }
            $('.floatingHeader').width($('.persist-header').width());
            floatingHeader.css('height', '40px');

            floatingHeader.css({
                "visibility": "visible"
            });
        } else {
            floatingHeader.css({
                "visibility": "hidden"
            });
        }
    });
}
