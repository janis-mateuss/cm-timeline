/* For rights see LICENSE.TXT */

// $(document).ready(function () {
//     setTimeout(function () {
//         window.ajaxOff = true;
//         checkIfUpdated(false);
//     }, 1000);
// });

function updateSystem() {
    $.ajax({
        type: 'POST',
        url: "version/update.php",
        data: {action: 'update'},
        success: function () {
            saveUpdate('core', true);
            alert('Sistēma atjaunota veiksmīgi.');
            $('.logout-badge').remove();
        },
        error: function () {
            alert('Kļūda veicot atjauninājumus.');
            saveUpdate('core', false);
        }
    });
}

function checkForUpdates(manual) {
    $.ajax({
        type: 'POST',
        url: "version/update.php",
        data: {action: 'check'},
        success: function (data) {
            if (data != '') {
                var jsonData = JSON.parse(data);
                if (jsonData.status) {
                    if (manual) {
                        if (confirm(jsonData.message)) {
                            $('.update-loader-text').html('Veic atjaunošanu...');
                            updateSystem();
                        } else {
                            saveUpdate('core', false);
                        }
                    } else {
                        $('.logout .dropdown-toggle').append('<span class="logout-badge badge">1</span>');
                        $('.system-update-label').append('<span class="logout-badge badge">1</span>');
                        saveUpdate('core', false);
                    }
                } else {
                    if (manual) {
                        alert(jsonData.message);
                        saveUpdate('core', true);

                    } else {
                        $('.logout-badge').remove();
                        saveUpdate('core', true);
                    }
                }
            }

            $('.update-loader-text').remove();
            window.ajaxOff = false;
        },
        error: function () {
            $('.update-loader-text').remove();
            window.ajaxOff = false;
        }
    });
}

function checkIfUpdated() {
    $.ajax({
        type: 'POST',
        url: "version/load_version.php",
        data: {module: 'core'},
        success: function (data) {
            if (data != '') {
                var jsonData = JSON.parse(data);
                if (jsonData.status) {
                    checkForUpdates(false);
                } else {
                    window.ajaxOff = false;
                }
            }
        }
    });
}

function saveUpdate(m, u) {
    $.ajax({
        type: 'POST',
        url: "version/save_version.php",
        data: {module: m, isUpdated: u},
        success: function () {
        }
    });
}