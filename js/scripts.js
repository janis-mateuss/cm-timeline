/* For rights see LICENSE.TXT */

var tableHeaders = ['22-06', '06-14', '14-22'];
var monthNamesShort = ['jan', 'feb', 'mar', 'apr', 'mai', 'jūn', 'jūl', 'aug', 'sep', 'okt', 'nov', 'dec'];
var machineCount = 0;
var machineMapping = {};
var historyEndCell = 1;

var loadedTiles = {}; // loaded products from DB

var markedMachines = {}; // marked machines where to place products
var markedShift = 0; // marked employee shift
var groupMachines = {};
var group = 0;

var undoProducts = []; // used to undo last 30 placed products
var texturingTasks = [];

var markedProducts = {};
var productsColor = {};

var Produce = {uniqueProducts: {}, table: {}};

var Move = {}; // namespace for marked products variable
var PBuffer = {cut: {}, copy: {}}; // namespace for buffer, that is, copy, paste, cut and stuff

var planLayout = {};

window.ajaxOff = false;

// scope the jQuery
(function ($) {

    $(document).ready(function () {
        $('#successModal').draggable();

        var draggableDiv = $('#produceModal').draggable();
        $('#produceTable', draggableDiv).mousedown(function () {
            draggableDiv.draggable('disable');
        }).mouseup(function () {
            draggableDiv.draggable('enable');
        });

        var statusBar = {'marked': {'label': 'Iezīmēts', 'count': 0}};
        var activeChkBoxTime = {};
        var intervalDates = {};
        var lastAddedTile;
        var currentPlanSummary = false;
        var planHtmlHolder = '';


        // Button-checkbox
        /////////////////////////////////////
        function initBttnCheckbox(which) {

            if (which == 0 || which == 1) {
                $('.button-checkbox-time').each(function () {

                    // Settings
                    var $widget = $(this),
                        $button = $widget.find('button'),
                        $checkbox = $widget.find('input:checkbox'),
                        color = $button.data('color'),
                        settings = {
                            on: {icon: 'glyphicon glyphicon-check'},
                            off: {icon: 'glyphicon glyphicon-unchecked'}
                        };

                    // Event Handlers
                    $button.unbind('click').bind('click', function () {
                        var bttnState = $button.data('state');

                        $('.machine-col').removeClass('machine-col');

                        if (bttnState == 'off') {
                            $checkbox.prop('checked', true);
                            $('#table2 tr:nth-child(n) td:nth-child(' + $button.closest('td')[0].cellIndex + ')').addClass('machine-col');
                        } else if (bttnState == 'on') {
                            $checkbox.prop('checked', false);
                        }

                        $checkbox.triggerHandler('change');

                        updateDisplay(true);
                    });

                    // Actions
                    function updateDisplay(clicked) {
                        if (clicked) {
                            var isChecked = $checkbox.is(':checked');

                            if (activeChkBoxTime.btn === undefined) {
                                activeChkBoxTime.btn = $button;
                                activeChkBoxTime.chkbox = $checkbox;
                            } else {
                                // unmark last marked checkbox
                                activeChkBoxTime.btn.data('state', "off");
                                activeChkBoxTime.btn.find('.state-icon').removeClass().addClass('state-icon ' + settings['off'].icon);
                                activeChkBoxTime.btn.removeClass('btn-' + color + ' active').addClass('btn-default');
                                activeChkBoxTime.chkbox.prop('checked', false);
                                activeChkBoxTime.chkbox.triggerHandler('change');
                            }
                            // Set the button's state
                            $button.data('state', (isChecked) ? "on" : "off");

                            // Set the button's icon
                            $button.find('.state-icon').removeClass().addClass('state-icon ' + settings[$button.data('state')].icon);

                            markedShift = 0;
                            // Update the button's color
                            if (isChecked) {
                                $button.removeClass('btn-default').addClass('btn-' + color + ' active');
                                // get index of pressed bttn-chkbox cell
                                markedShift = $widget.parent().index();
                            }
                            activeChkBoxTime.btn = $button;
                            activeChkBoxTime.chkbox = $checkbox;
                        } else {
                            $button.data('state', "off");
                            $button.find('.state-icon').removeClass().addClass('state-icon ' + settings[$button.data('state')].icon);
                            $button.removeClass('btn-' + color + ' active').addClass('btn-default');
                        }
                    }

                    // Initialization
                    function init() {
                        updateDisplay();
                        // Inject the icon if applicable
                        if ($button.find('.state-icon').length == 0) {
                            $button.prepend('<i class="state-icon ' + settings[$button.data('state')].icon + '"></i>');
                        }
                    }

                    init();
                });
            }

            if (which == 0 || which == 2) {
                $('.button-checkbox').each(function () {

                    // Settings
                    var $widget = $(this),
                        $button = $widget.find('button'),
                        $checkbox = $widget.find('input:checkbox'),
                        color = $button.data('color'),
                        settings = {
                            on: {icon: 'glyphicon glyphicon-check'},
                            off: {icon: 'glyphicon glyphicon-unchecked'}
                        };

                    // Event Handlers
                    $button.on('click', function () {
                        $checkbox.prop('checked', !$checkbox.is(':checked'));
                        $checkbox.triggerHandler('change');
                        updateDisplay2(true);
                    });
                    $checkbox.on('change', function () {
                        updateDisplay2();
                    });

                    // Actions
                    function updateDisplay2(clicked) {
                        var isChecked = $checkbox.is(':checked');

                        // Set the button's state
                        $button.data('state', (isChecked) ? "on" : "off");

                        // Set the button's icon
                        $button.find('.state-icon').removeClass().addClass('state-icon ' + settings[$button.data('state')].icon);

                        // Update the button's color
                        if (isChecked) {
                            $button.removeClass('btn-default').addClass('btn-' + color + ' active');

                            var toggledChkBox = $button.data('id'),
                                toggleRowIndex = $('.button-checkbox [data-id="' + toggledChkBox + '"]').closest('tr')[0].rowIndex + 1;
                            // color row with this number
                            $("#table2 tbody tr:nth-child(" + toggleRowIndex + ") td").addClass('machine-row');
                            markedMachines[toggledChkBox] = true;
                        } else {
                            $button.removeClass('btn-' + color + ' active').addClass('btn-default');

                            if (clicked == true) {
                                var toggledChkBox = $button.data('id'),
                                    toggleRowIndex = $('.button-checkbox [data-id="' + toggledChkBox + '"]').closest('tr')[0].rowIndex + 1;
                                // color row with this number
                                $("#table2 tbody tr:nth-child(" + toggleRowIndex + ") td").removeClass('machine-row');
                                delete markedMachines[toggledChkBox];
                            }
                        }
                    }

                    // $( "#table2 tbody tr:nth-child(2) td:nth-child(456)").index()
                    // Initialization
                    function init2() {
                        updateDisplay2();
                        // Inject the icon if applicable
                        if ($button.find('.state-icon').length == 0) {
                            $button.prepend('<i class="state-icon ' + settings[$button.data('state')].icon + '"></i>');
                        }
                    }

                    init2();
                });
            }
        }

        // End button-checkbox
        /////////////////////////////////////////////////////////
        $('.popupDatepicker').datepick({dateFormat: 'dd/mm/yyyy'});


        // Scroll. Fixed vertical and horizontal header
        ////////////////////////////////////////////////////////
        var initHeaderOffset = $('.table1-header').offset();

        var prevTop = 0;
        var prevLeft = 0;
        $(window).scroll(function () {
            var currentTop = $(window).scrollTop();
            var currentLeft = $(window).scrollLeft();

            if (prevTop != currentTop) { // vertical scroll
                prevTop = currentTop;
                var prevOffset = $('.table1-header').offset();
                var prevOffsetLeft = $('.left-header').offset();
                $('.table1-header').offset({top: initHeaderOffset.top + currentTop, left: prevOffset.left});
                $('.left-header').offset({top: 154, left: prevOffsetLeft.left});
            }
        });

        //////////////////////////////////////////////////////

        var getDates = function (startDate, endDate) {
            var dates = [],
                currentDate = startDate,
                addDays = function (days) {
                    var date = new Date(this.valueOf());
                    date.setDate(date.getDate() + days);
                    return date;
                };

            while (currentDate <= endDate) {
                dates.push(currentDate);
                currentDate = addDays.call(currentDate, 1);
            }
            if (dates.length > 0) {
                var lastdate = dates[dates.length - 1];
                lastdate.setHours(0, 0, 0, 0);
                if (lastdate < endDate.setHours(0, 0, 0, 0))
                    dates.push(addDays.call(lastdate, 1));
            }
            return dates;
        };

        function drawTable(startDate, endDate) {
            var dates = getDates(new Date(startDate), new Date(endDate));
            var header_time_html = '';
            var header_day_html = '';

            dates.forEach(function (d) // timeline header
            {
                for (var i = 0; i < tableHeaders.length; i++) { // time
                    checkBox_html = '<span class="button-checkbox-time">' +
                        '<button style="width:100%;" type="button" class="btn btn-xs" data-color="success">' + tableHeaders[i] + '</button>' +
                        '<input type="checkbox" class="hidden" />' +
                        '</span>';
                    header_time_html += '<td class="">' + checkBox_html + '</td>';
                }

                var dateStr = d.getDate() + "." + monthNamesShort[d.getMonth()] + "  " + d.getFullYear();
                // format date for header date id
                var date_formated = d.getFullYear() + '-' + pad((d.getMonth() + 1)) + '-' + pad(d.getDate()) + 'H';
                header_day_html += '<td id="' + date_formated + '" class="day-td" colspan="3">' + dateStr + '</td>'; //date
            });

            $('.table1-header .header-time').html('<td class="blank"></td>' + header_time_html);
            $('.table1-header .header-day').html('<td class="blank"></td>' + header_day_html);

            var left_header_html = ''; //left static column header
            var table2_html = '';   // table cells
            var checkBox_html = '';

            // generate table cells
            for (var i = 0; i < machineCount; i++) {
                // length for employee change times. There are 3 in 1 day
                var cellLen = dates.length * 3,
                    groupedMachineClass =
                        machineMapping[i]['grouped'] !== "no" &&
                        machineMapping[i]['grouped'] !== ""
                            ? 'grouped' : '';

                if (groupedMachineClass === 'grouped' && machineMapping[i]['grouped'] === "start") {
                    groupedMachineClass += ' start';
                } else if (groupedMachineClass === 'grouped' && machineMapping[i]['grouped'] === "end") {
                    groupedMachineClass += ' end';
                }

                checkBox_html = '<span class="button-checkbox ' + groupedMachineClass + '">' +
                    '<button style="width:100%;" type="button" class="btn btn-xs" data-id="' + machineMapping[i]['id'] + '" data-color="success">'
                    + (machineMapping[i]['name']) + '</button>' +
                    '<input type="checkbox" class="hidden"/>' +
                    '</span>';
                left_header_html += '<tr><th class="redips-mark dark">' + checkBox_html + '</th></tr>';
                table2_html += '<tr>';

                // counts to 3
                var data_counter = 0;
                for (var j = 0; j < cellLen; j++) {
                    // if mod 3 is 0 then gets next date index
                    data_counter = (j > 0 && j % 3 == 0) ? ++data_counter : data_counter;
                    // iterate through dates
                    var cur_date = dates[data_counter];
                    // make td name with date from array
                    var cur_date_formated = cur_date.getFullYear() + '-' + pad((cur_date.getMonth() + 1)) + '-' + pad(cur_date.getDate()) + '/' + machineMapping[i]['id'] + '/' + j % 3;
                    table2_html += '<td id="r' + (machineMapping[i]['id']) + 'c' + (j + 1) + '" name="' + cur_date_formated + '"></td>';
                }
                table2_html += '</tr>';
            }
            $('.left-header').html(left_header_html);
            $('#table2 tbody').html(table2_html);

            // update bttn-checkboxes
            initBttnCheckbox(0);
            drawTodaysSign(true);
        }

        function expandTable(c) {
            // last day date of current table
            getMachineCount();
            var lastDate = $('.header-day td').last()[0].id;
            lastDate = lastDate.slice(0, -1);

            var lastDateObj = new Date(lastDate);
            lastDateObj.setDate(lastDateObj.getDate() + 1);
            var lastDateObj_formated = lastDateObj.getFullYear() + '-' + pad((lastDateObj.getMonth() + 1)) + '-' + pad(lastDateObj.getDate());

            // create expandig table last date
            var expandDate = new Date(lastDate);
            expandDate.setDate(expandDate.getDate() + c);

            var expandDate_formated = expandDate.getFullYear() + '-' + pad((expandDate.getMonth() + 1)) + '-' + pad(expandDate.getDate());
            // get all dates for expanding table
            var dates = getDates(new Date(lastDate), expandDate);
            // delete first array element becouse it's unnecessery
            dates.shift();
            var header_time_html = '';
            var header_day_html = '';

            initBttnCheckbox(1); // fix for bttn-checkbox
            dates.forEach(function (d) // timeline header
            {
                for (var i = 0; i < tableHeaders.length; i++) { // time
                    checkBox_html = '<span class="button-checkbox-time">' +
                        '<button style="width:100%;" type="button" class="btn btn-xs" data-color="success">' + tableHeaders[i] + '</button>' +
                        '<input type="checkbox" class="hidden" />' +
                        '</span>';
                    header_time_html += '<td class="">' + checkBox_html + '</td>';
                }
                ;
                var dateStr = d.getDate() + "." + monthNamesShort[d.getMonth()] + "  " + d.getFullYear();
                // format date for header date id
                var date_formated = d.getFullYear() + '-' + pad((d.getMonth() + 1)) + '-' + pad(d.getDate()) + 'H';
                header_day_html += '<td id="' + date_formated + '" class="day-td" colspan="3">' + dateStr + '</td>'; //date
            });
            $('.table1-header .header-time').append(header_time_html);
            $('.table1-header .header-day').append(header_day_html);
            initBttnCheckbox(1);

            // add table main table cells
            // generate table cells
            var rowLastCellIndex = $("#table2 tbody tr:nth-child(1) td").last()[0].cellIndex + 1;
            for (var i = 0; i < machineCount; i++) {
                var table2_html = '';   // table cells
                // length for employee change times. There are 3 in 1 day
                var cellLen = dates.length * 3;
                // counts to 3
                var data_counter = 0;
                for (var j = 0; j < cellLen; j++) {
                    // if mod 3 is 0 then gets next date index
                    data_counter = (j > 0 && j % 3 == 0) ? ++data_counter : data_counter;
                    // iterate through dates
                    var cur_date = dates[data_counter];
                    // make td name with date from array
                    var cur_date_formated = cur_date.getFullYear() + '-' + pad((cur_date.getMonth() + 1)) + '-' + pad(cur_date.getDate()) + '/' + machineMapping[i]['id'] + '/' + j % 3;
                    table2_html += '<td id="r' + (machineMapping[i]['id']) + 'c' + (j + 1 + rowLastCellIndex) + '" name="' + cur_date_formated + '"></td>';
                }
                ;
                $('#table2 tr:nth-child(' + (i + 1) + ')').append(table2_html);
            }
            drawTodaysSign(false);
            loadTable(lastDateObj_formated, expandDate_formated, false);
        }

        function fillProducts(product, start, count, with_shifting) {
            if (Object.keys(markedMachines).length < 1) {
                showNotification('Nav atzīmēta neviena mašīna.', 'danger');
                return;
            }
            if (count > 50000) {
                showNotification('Maksimālais ievades daudzums ir 50000kg.', 'danger');
                return;
            }
            $.post("php/products_formula.php", {kg: count, prod: product})
                // when post is finished
                .done(function (data) {
                    var jsonCount = 0;
                    var usedShifts = {}; // set of columns where products is landed;
                    var firstPlacedIndex = -1;

                    if (data != '') {
                        updateUndo();
                        var jsonData = JSON.parse(data)
                        jsonCount = jsonData['shifts'];
                        var notAllowedMachines = jsonData['notAllowedMachines'];

                        // check if product is not added on unallowed machines
                        var wrongMachines = [];
                        for (var i = 0; i < notAllowedMachines.length; i++) {
                            if (notAllowedMachines[i] in markedMachines) {
                                wrongMachines.push(notAllowedMachines[i]);
                            }
                        }

                        if (wrongMachines.length !== 0) {
                            let mappedWrongMachines = wrongMachines.map(function(item) {
                                return $('.button-checkbox [data-id="' + item + '"]').text()
                            }),
                                machineString = wrongMachines.length > 1 ? "mašīnām" : "mašīnas";

                            $('#successModal .modal-content').removeClass('panel-danger').removeClass('panel-success').addClass('panel-danger');
                            $('#successModal .modal-title').html('Pievienošana liegta');
                            $('#successModal .modal-body p').html('Produkts nav atļauts pievienošanai uz ' + machineString + ': <b>' + mappedWrongMachines.join(', ') + '</b>');
                            $('#successModal').modal('show');
                            return;
                        }

                        var uneditable_jsonCount = jsonCount;
                        // row column length
                        var column_count = document.getElementById('table2').rows[0].cells.length;
                        var i = start;
                        if (jsonCount == 0) {
                            showNotification("Pārāk maza vērtība. Nekas netiks pievienots.", 'success');
                            return;
                        }
                        if (!with_shifting) {
                            updateColor(product);
                            // if products can be filled in table
                            // iterate over columns
                            var last_td;
                            for (i = i; i <= column_count; i++) {
                                // for each marked machine in left header column
                                for (var key in markedMachines) {
                                    // js lagging fix
                                    if (markedMachines.hasOwnProperty(key)) {
                                        // get cell
                                        var td_html = $("#r" + key + "c" + i);
                                        // if inner html in cell is empty
                                        if (i >= column_count) {
                                            expandTable(14);
                                            column_count = $("#table2 tbody tr:nth-child(1) td").last()[0].cellIndex;
                                        }
                                        if (td_html.hasClass('dark')) {
                                            continue;
                                        }

                                        if (td_html[0].innerHTML.trim() == "") {
                                            // put a product in cell
                                            td_html.html(setProductDiv(td_html.attr("name"), product, group, false, jsonData['kgPerShift'], 'false'));
                                            last_td = td_html;
                                            jsonCount--;
                                            usedShifts[i] = true;
                                            if (firstPlacedIndex == -1) firstPlacedIndex = i;

                                        }
                                    }
                                    if (0 >= jsonCount) { // if all products placed
                                        td_html.html(setProductDiv(td_html.attr("name"), product, group, false, jsonData['kgLastShift'], 'false'));
                                        break;
                                    }
                                }
                                if (0 >= jsonCount) {
                                    td_html.html(setProductDiv(td_html.attr("name"), product, group, false, jsonData['kgLastShift'], 'false'));
                                    break;
                                }
                            }
                            ;
                            updateMachineGroup();
                        }
                        // with shifting
                        else {
                            var endFill = false,
                                tileCounter = 1,
                                start_i = 0,
                                start_shift = start,
                                td_div;
                            markedProducts = {};

                            firstPlacedIndex = start;

                            if (jsonCount > 0) {
                                updateColor(product);

                                var allMachines = [];
                                for (var mac in markedMachines) {
                                    allMachines.push(parseInt(mac));
                                }
                                allMachines.sort(function (a, b) {
                                    return $('.button-checkbox [data-id="' + a + '"]').closest('tr')[0].rowIndex -
                                        $('.button-checkbox [data-id="' + b + '"]').closest('tr')[0].rowIndex;
                                })

                                var rowLastCellIndex = $("#table2 tbody tr:nth-child(1) td").last()[0].cellIndex;
                                var nextGroup = ++group;
                                while (true) {
                                    if (start_shift >= rowLastCellIndex) {
                                        expandTable(14);
                                        rowLastCellIndex = $("#table2 tbody tr:nth-child(1) td").last()[0].cellIndex;
                                    }

                                    for (var i = start_i; i < allMachines.length; i++) {
                                        td_div = $("#r" + allMachines[i] + "c" + start_shift + " div");

                                        if (td_div[0] !== undefined)
                                            if (td_div.attr('fixed') == 'true' || td_div.attr('fixed') == '1') {
                                                continue;
                                            }

                                        let markedRowIndex = $('.button-checkbox [data-id="' + allMachines[i] + '"]').closest('tr')[0].rowIndex + 1;

                                        markedProducts[allMachines[i] + '/' + start_shift] = {
                                            'r': markedRowIndex,
                                            'c': start_shift,
                                            'rEnd': markedRowIndex,
                                            'cEnd': start_shift,
                                            'rId': allMachines[i],
                                            'rEndId': allMachines[i],
                                            'product': product,
                                            'kg': (tileCounter == jsonCount) ? jsonData['kgLastShift'] : jsonData['kgPerShift'],
                                            'fixed': false,
                                            'groupid': nextGroup
                                        };
                                        usedShifts[start_shift] = true;

                                        if (tileCounter == jsonCount) {
                                            endFill = true;
                                            break;
                                        }
                                        tileCounter++;
                                    }
                                    if (endFill)
                                        break;
                                    start_i = 0;
                                    start_shift++;
                                }
                                i = start_shift;

                                Move.start_row = $('.button-checkbox [data-id="' + allMachines[0] + '"]').closest('tr')[0].rowIndex + 1;
                                Move.start_col = start;
                                var el = $('#r' + machineMapping[Move.start_row - 1]['id'] + 'c' + Move.start_col);

                                // simulate mouse move to get rEnd and cRow defined for markedProducts
                                triggeProductPlacement(el, el.find('div')[0], true, 300, 300, markedProducts, true);
                            }
                        }

                        // Preperation for message that is shown when products is added to plan.
                        i = (with_shifting) ? lastAddedTile : i; // global var that is set in 'mousemove' event
                        var endDateCol = (i % 3 == 0) ? i / 3 + 1 : parseInt(i / 3) + 2;
                        var startDateCol = (firstPlacedIndex % 3 == 0) ? firstPlacedIndex / 3 + 1 : parseInt(firstPlacedIndex / 3) + 2;

                        var endDate = new Date($('.header-day td:nth-child(' + endDateCol + ')')[0].id.slice(0, -1));
                        var startDate = new Date($('.header-day td:nth-child(' + startDateCol + ')')[0].id.slice(0, -1));

                        endDate_formated = endDate.getFullYear() + '.gada ' + endDate.getDate() + '.' + monthNamesShort[endDate.getMonth()];
                        if (firstPlacedIndex % 3 == 1) {
                            startDate.setDate(startDate.getDate() - 1);
                        }
                        startDate_formated = startDate.getFullYear() + '.gada ' + startDate.getDate() + '.' + monthNamesShort[startDate.getMonth()];

                        if (i % 3 == 0) endDate_formated += ' 22:00';
                        if (i % 3 == 1) endDate_formated += ' 06:00';
                        if (i % 3 == 2) endDate_formated += ' 14:00';

                        if (firstPlacedIndex % 3 == 0) startDate_formated += ' 14:00';
                        if (firstPlacedIndex % 3 == 1) startDate_formated += ' 22:00';
                        if (firstPlacedIndex % 3 == 2) startDate_formated += ' 06:00';

                        $('#successModal .modal-title').html('Kopsavilkums');
                        $('#successModal .modal-content').removeClass('panel-danger').addClass('panel-success');
                        $('#successModal .modal-body p').html('Pievienota/as <b>' + Object.keys(usedShifts).length + '</b> maiņa/as uz <b>' + Object.keys(markedMachines).length + '</b> mašīnas/ām (' + uneditable_jsonCount + ' vienības)<br />');
                        $('#successModal .modal-body p').append('<b>Sākums:</b> ' + startDate_formated + '<br />');
                        $('#successModal .modal-body p').append('<b>Beigas:</b> ' + endDate_formated);
                        $('#successModal').modal('show');

                        texturingTasks.push({
                            prod: product, kg: count,
                            machine_count: Object.keys(markedMachines).length,
                            startDate: formatDate(startDate),
                            endDate: formatDate(endDate)
                        });
                    } else {
                        showNotification("Nekorekts rezultāts.", 'danger');
                    }

                })
                .fail(function (data) {
                    showNotification("Nevar pievienot produktu.", 'danger');
                });
        }

        function loadTable(startD, endD, is_async, loadPlanLayout = 0) {
            $.ajax({
                type: 'POST',
                url: "php/load.php",
                data: {startDate: startD, endDate: endD, getPlanLayout: loadPlanLayout},
                success: function (data) {
                    // parse received php data to JSON
                    var jsonData = '';
                    try {
                        jsonData = jQuery.parseJSON(data);
                    } catch (e) {
                    }

                    if (loadPlanLayout) {
                        planLayout = jsonData['planLayout'] || {};

                        updatePlanMenu();
                    }

                    // if json is parsed
                    // place free shifts
                    if (jsonData != '' && jsonData['shifts'] !== undefined) {
                        // iterate over JSON array
                        jsonData['shifts'].forEach(function (shift) {
                            // make id for tile
                            var head_id = '#' + shift.week_day + 'H';
                            // update color for product name
                            var head_id_col = $(head_id)[0].cellIndex;
                            var calculated_id = head_id_col * 3 - 1;
                            // mark free shifts all columns with redips-mark dark
                            if (shift.shift1) {
                                $('.header-time td:nth-child(' + (calculated_id) + ')').html(tableHeaders[0]).addClass('free');
                                $('#table2 tr:nth-child(n) td:nth-child(' + (calculated_id - 1) + ')').addClass('redips-mark dark');
                            }
                            if (shift.shift2) {
                                $('.header-time td:nth-child(' + (calculated_id + 1) + ')').html(tableHeaders[1]).addClass('free');
                                $('#table2 tr:nth-child(n) td:nth-child(' + calculated_id + ')').addClass('redips-mark dark');
                            }
                            if (shift.shift3) {
                                $('.header-time td:nth-child(' + (calculated_id + 2) + ')').html(tableHeaders[2]).addClass('free');
                                $('#table2 tr:nth-child(n) td:nth-child(' + (calculated_id + 1) + ')').addClass('redips-mark dark');
                            }
                        });
                    }

                    // place products in table
                    if (jsonData != '' && jsonData['products'] !== undefined) {
                        if (jsonData['products'] !== undefined) {
                            // iterate over JSON array
                            jsonData['products'].forEach(function (plan) {
                                // make id for tile
                                var td_name = plan.p_date + '/' + plan.machine + '/' + plan.e_shift;
                                // update color for product name
                                updateColor(plan.product);
                                // update td witch have attr name with drag tile
                                var td_cell = $('[name="' + td_name + '"]');

                                if (td_cell.length) {
                                    td_cell.html(setProductDiv(td_name, plan.product, plan.groupId, false, plan.kg, plan.fixed_position.toString()));
                                    if (plan.fixed_position.toString() == 'true' || plan.fixed_position.toString() == '1') {
                                        td_cell.addClass('dark');
                                        td_cell.addClass('fixed-pos');
                                    }
                                    // add objects to loadedTiles
                                    loadedTiles[td_name] = {
                                        product: plan.product,
                                        kg: plan.kg,
                                        fixed: plan.fixed_position.toString()
                                    };
                                }
                            });
                        }
                    }
                    if (is_async) updateMachineGroup();
                    shiftFromFreeDays();
                    //if ( is_async )
                    updateMachineGroup();
                },
                async: is_async
            });

            setTimeout(function () {
                document.body.classList.add('table-loaded');
            }, 1000);
        }

        function getMachineCount() {
            $.ajax({
                type: 'POST',
                url: "php/machine_load.php",
                data: {action: "_get_all"},
                success: function (data) {
                    if (data != '') {
                        var jsonCount = jQuery.parseJSON(data);
                        machineCount = parseInt(jsonCount['count']);
                        machineMapping = jsonCount['mapping'];
                    }
                },
                async: false
            });
        }

        // Init actions on first start
        var endDate = new Date();
        endDate.setDate(endDate.getDate() + 21);
        var endDate_formated = endDate.getFullYear() + '/' + (endDate.getMonth() + 1) + '/' + endDate.getDate();

        var startDate = new Date();
        startDate.setDate(startDate.getDate() - 1);
        var startDate_formated = startDate.getFullYear() + '/' + (startDate.getMonth() + 1) + '/' + startDate.getDate();

        getMachineCount();
        drawTable(startDate_formated, endDate_formated);
        loadTable(startDate_formated, endDate_formated, false, 1);

        // first plan load till the first free column starting from 19th col
        var colCounter = 19;
        var lastCellIndex = $("#table2 tbody tr:nth-child(1) td").last()[0].cellIndex;
        var loadCell;//.hasClass('redips-mark')
        while (true) {
            colCounter++;
            if (colCounter > lastCellIndex) {
                expandTable(14);
                lastCellIndex = $("#table2 tbody tr:first td").last()[0].cellIndex + 1;
            }

            loadCell = $('[id$="c' + colCounter + '"]');
            if (!loadCell.hasClass('redips-mark') && loadCell.find('div')[0] === undefined)
                break;
        }

        /* ################################################
    ###################### EVENTS #######################
    */

        $('#gen-table-bttn').click(function () {
            var startDate = $('[name="start"]').val();
            var endDate = $('[name="end"]').val();
            // format to: mm/dd/yyyy input: dd/mm/yyyy
            startDate = startDate.split('/')[1] + '/' + startDate.split('/')[0] + '/' + startDate.split('/')[2];
            endDate = endDate.split('/')[1] + '/' + endDate.split('/')[0] + '/' + endDate.split('/')[2];

            var sd = new Date(startDate), ed = new Date(endDate);
            // if start date is less or equal to end date the draw table
            if (sd <= ed) {
                markedMachines = {};
                markedShift = 0;
                markedProducts = {};
                loadedTiles = {};
                groupMachines = {};

                // get postgre time format
                startDate = startDate.split('/')[2] + '-' + startDate.split('/')[0] + '-' + startDate.split('/')[1];
                endDate = endDate.split('/')[2] + '-' + endDate.split('/')[0] + '-' + endDate.split('/')[1];

                getMachineCount();
                drawTable(startDate, endDate);
                console.log('Start loading table...');
                loadTable(startDate, endDate, true);
                console.log('Finished loading!');
            } else {
                showNotification('Nav korekti ievadīts datums.', 'danger');
            }
        });


        $('#gen-prod-bttn').click(function () {
            var p = $('#product').val();
            var q = $('#quantity').val();
            if (markedShift < 1) {
                showNotification("Nav atzīmēta starta maiņa!", 'danger');
                return;
            }
            fillProducts(p, markedShift, q, false);
            $('.marked').removeClass('marked');
        });

        $('#gen-prod-shift-bttn').click(function () {
            var p = $('#product').val();
            var q = $('#quantity').val();
            if (markedShift < 1) {
                showNotification("Nav atzīmēta starta maiņa!", 'danger');
                return;
            }
            updateMachineGroup();
            fillProducts(p, markedShift, q, true);
            $('.marked').removeClass('marked');
        });

        $('#undo-gen-prod-bttn').click(function () {
            Move.move_products['start'] = false;
            Move.move_products['move'] = false;

            if (undoProducts.length > 0) {
                markedShift = 0;
                var undoHTML = undoProducts.pop();
                $('.table1-header').html(undoHTML['header']);
                $('#table2').html(undoHTML['table']);
                $('.left-header').html(undoHTML['lefth']);
                markedProducts = undoHTML['marked'];
                groupMachines = undoHTML['groups'];
                texturingTasks = undoHTML['texturingTask'];

                $('.button-checkbox-time').each(function () {
                    $(this).find('button').attr('class', 'btn btn-xs btn-default');
                    $(this).find('i').attr('class', 'state-icon glyphicon glyphicon-unchecked');
                })

                drawTodaysSign(true);
                initBttnCheckbox(0);
                $('.machine-row').removeClass('machine-row');
                $('.machine-col').removeClass('machine-col');
                markedMachines = {};
            }
            //$('#undo-text').html('Atcelt ('+undoProducts.length+')');
        });

        $('#save-bttn').click(function () {
            save();
        });

        $('#deselect-machine-bttn').click(function () {
            $('.button-checkbox').each(function () {
                // deselect checked machines bttn-chckbxs.
                if ($(this).find('input:checkbox').is(':checked')) {
                    $(this).find('button').click();
                }
            });
        });

        $("#production-bttn").click(function () {
            $('#produceModal').modal('show');
        });

        $("#interval-bttn").click(function () {
            $(this).text('Uzgaidiet');

            setTimeout(function (bttn) {
                var startDate = $('[name="produceStart"]').val();
                var endDate = $('[name="produceEnd"]').val();
                // format to: mm/dd/yyyy input: dd/mm/yyyy
                startDate = startDate.split('/')[1] + '/' + startDate.split('/')[0] + '/' + startDate.split('/')[2];
                endDate = endDate.split('/')[1] + '/' + endDate.split('/')[0] + '/' + endDate.split('/')[2];

                var sd = new Date(startDate), ed = new Date(endDate);
                // if start date is less or equal to end date the draw table
                if (sd <= ed) {
                    // get postgre time format
                    startDate = startDate.split('/')[2] + '-' + startDate.split('/')[0] + '-' + startDate.split('/')[1];
                    endDate = endDate.split('/')[2] + '-' + endDate.split('/')[0] + '-' + endDate.split('/')[1];
                    var nextShift = 1;

                    // if today is equal with intervals start day
                    if (formatDate(new Date()) == formatDate(new Date(startDate))) {
                        var todayDate = new Date();
                        var today_h = todayDate.getHours();
                        nextShift = getShiftNumber(today_h, true);
                        if (nextShift == 0) todayDate.setDate(todayDate.getDate() + 1);
                        nextShift = (nextShift == 0) ? 1 : nextShift + 1;
                        startDate = formatDate(todayDate);
                    }

                    var startDateIndex = ($('#' + startDate + 'H')[0].cellIndex - 1) * 3 + nextShift;
                    var endDateIndex = $('#' + endDate + 'H')[0].cellIndex * 3;

                    intervalDates = {startDate: startDateIndex, endDate: endDateIndex};

                    // get all products that ar in range of selected interval
                    var intervalProducts = $("#table2 tbody tr:nth-child(n+1):nth-child(-n+" + machineCount + ") td:nth-child(n+" + startDateIndex + "):nth-child(-n+" + endDateIndex + ") div");
                    Produce.uniqueProducts = {};

                    $.each(intervalProducts, function () {
                        Produce.uniqueProducts[$(this).attr('product')] = {
                            count: 0, kg: 0, lastCol: 0,
                            machines: {}, shifts: {}, groups: {}
                        };
                    });

                    var td_cell;
                    for (var col = startDateIndex; col <= endDateIndex; col++) {
                        for (var row = 0; row < machineCount; row++) {
                            td = $('#r' + machineMapping[row]['id'] + 'c' + col + ' div');
                            if (td[0] !== undefined) {
                                Produce.uniqueProducts[td.attr('product')].count += 1;
                                Produce.uniqueProducts[td.attr('product')].kg += parseFloat(td.attr('kg'));

                                if (Produce.uniqueProducts[td.attr('product')].fixed !== undefined) {
                                    if (Produce.uniqueProducts[td.attr('product')].fixed !== td.attr('fixed') &&
                                        Produce.uniqueProducts[td.attr('product')].fixed !== 'mid') {
                                        Produce.uniqueProducts[td.attr('product')].fixed = 'mid';
                                    } else if (Produce.uniqueProducts[td.attr('product')].fixed !== 'mid') {
                                        Produce.uniqueProducts[td.attr('product')].fixed = td.attr('fixed');
                                    }
                                } else {
                                    Produce.uniqueProducts[td.attr('product')].fixed = td.attr('fixed');
                                }

                                if (Produce.uniqueProducts[td.attr('product')].shifts[col] !== undefined) {
                                    Produce.uniqueProducts[td.attr('product')].shifts[col].push(machineMapping[row]['id']);
                                } else {
                                    Produce.uniqueProducts[td.attr('product')].shifts[col] = [machineMapping[row]['id']];
                                }

                                Produce.uniqueProducts[td.attr('product')].machines[machineMapping[row]['id']] = true;

                                if (Produce.uniqueProducts[td.attr('product')].lastCol < col)
                                    Produce.uniqueProducts[td.attr('product')].lastCol = col;

                                if (Produce.uniqueProducts[td.attr('product')].firstCol !== undefined) {
                                    if (Produce.uniqueProducts[td.attr('product')].firstCol > col)
                                        Produce.uniqueProducts[td.attr('product')].firstCol = col;
                                } else {
                                    Produce.uniqueProducts[td.attr('product')].firstCol = col;
                                }

                                Produce.uniqueProducts[td.attr('product')].groups[td.attr('groupid')] = true;
                            }
                        }
                    }

                    // delete products form table set that don't end in the interval
                    // iterate through last interval column
                    var endCellIndex = $('#table2 tr:nth-child(1) td').last()[0].cellIndex + 1;

                    var lastNextColProds = $("#table2 tbody tr:nth-child(n+1):nth-child(-n+" + machineCount + ") td:nth-child(" + (endDateIndex + 1) + ") div");
                    var lastNextColProducts = {};
                    // make set of products, so there is only unique product names
                    $.each(lastNextColProds, function (i, item) {
                        lastNextColProducts[item.innerHTML] = true;
                    });

                    var lastColProds = $("#table2 tbody tr:nth-child(n+1):nth-child(-n+" + machineCount + ") td:nth-child(" + endDateIndex + ") div");
                    var lastColProducts = {};
                    // make set of products, so there is only unique product names
                    $.each(lastColProds, function (i, item) {
                        lastColProducts[item.innerHTML] = true;
                    });

                    for (var row = 0; row < machineCount; row++) {
                        td = $('#r' + machineMapping[row]['id'] + 'c' + endDateIndex);
                        td_product = $(td).find('div');
                        td_next = $(td).next();

                        // if cell have product
                        if (td_product[0] !== undefined) {
                            // if the same product is in the next column somewhere
                            if (td_product[0].innerHTML in lastNextColProducts) {
                                delete Produce.uniqueProducts[td_product[0].innerHTML];
                            } else {
                                // if next cell is over table end boundries
                                if (td_next[0] === undefined) {
                                    delete Produce.uniqueProducts[td_product[0].innerHTML];
                                }
                            }
                        }
                    }
                    // check for every product that is in next col over interval last col
                    // if that product is not in the interval of the last col then delete it from the list
                    for (var key in lastNextColProducts) {
                        if (lastNextColProducts.hasOwnProperty(key)) {
                            if (!key in lastColProducts) {
                                delete Produce.uniqueProducts[key];
                            }
                        }
                    }

                    var produceTable_html = '';
                    Produce.table = {};
                    for (var key in Produce.uniqueProducts) {
                        // js lagging fix
                        if (Produce.uniqueProducts.hasOwnProperty(key)) {
                            produceTable_html += '<tr>';
                            produceTable_html += '<td><div class="checkbox"><label><label class="checkbox-inline">' +
                                '<input type="checkbox" id="checkbox' + key + '" value="option1" unchecked>' +
                                '</label></label></div></td>';
                            produceTable_html += '<td style="font-weight:bold;" class="planSummary-name">' + key + '</td><td contenteditable="true">' + Produce.uniqueProducts[key].kg.toFixed(2) + '</td>';
                            produceTable_html += '<td>' + Object.keys(Produce.uniqueProducts[key].groups).length + '</td>';
                            produceTable_html += '<td><button type="button" class="btn btn-success ladda-button" data-style="zoom-in" id="produce-change-bttn"><span class="ladda-label">Izmainīt</span></button></td>';
                            produceTable_html += '</tr>';
                            Produce.table[key] = {
                                kg: Produce.uniqueProducts[key].kg.toFixed(2),
                                fixed: Produce.uniqueProducts[key].fixed
                            };

                        }
                    }
                    if (produceTable_html === '') {
                        $('#produceTable tbody').html('<p>Nav produktu dotājā intervālā.</p>')
                    } else {
                        $('#produceTable tbody').html(produceTable_html);
                    }

                    for (var key in Produce.uniqueProducts) {
                        if (Produce.uniqueProducts.hasOwnProperty(key)) {
                            // mark checkboxes with tri-state
                            if (Produce.uniqueProducts[key].fixed === 'true' || Produce.uniqueProducts[key].fixed == '1') {
                                $('#checkbox' + key).prop('checked', true);
                            } else if (Produce.uniqueProducts[key].fixed === 'mid') {
                                $('#checkbox' + key)[0].indeterminate = true;
                            } else {
                                $('#checkbox' + key).prop('checked', false);
                            }
                        }
                    }
                } else {
                    showNotification('Nav korekti ievadīts datums.', 'danger');
                }
                $(bttn).text('Parādīt');
            }, 50, this);
        });

        $('#planSummaryFilter').on('input', function (e) {
            let $produceTable = $('#produceTable tbody tr');
            $produceTable.hide();

            if (e.currentTarget.value === '') {
                $produceTable.show();
            } else {
                $produceTable.filter(function (index, el) {
                    return ~$(el).find('.planSummary-name').text().toLowerCase().indexOf(e.currentTarget.value);
                }).show()
            }
        });

        $("#produceModal").on('click', '#produce-change-bttn', function () {
            $(this).text('Izmaina...');
            setTimeout(function (bttn) {
                var changedProducts = [];
                var curRow = $("#produceTable tbody tr:nth-child(" + bttn.parentElement.parentElement.rowIndex + ")");
                var keyProduct = curRow.find('td:nth-child(2)')[0].innerHTML;

                // if table before closing is not the same as it  was opened. some products value are changed. Or the fixed state checkbox is changed
                if (Produce.table[keyProduct].kg != curRow.find('td:nth-child(3)')[0].innerHTML || Produce.table[keyProduct].fixed != curRow.find('td:nth-child(1) input').is(':checked')) {
                    changedProducts.push({
                        startKg: Produce.table[keyProduct],
                        endKg: curRow.find('td:nth-child(3)')[0].innerHTML,
                        fixed: curRow.find('td:nth-child(1) input').is(':checked')
                    })
                }

                // the length either will be 0 or 1. So if this statement is true, then we can access changeProducts by index '0'.
                if (changedProducts.length > 0) {
                    error = false;
                    $.ajax({
                        type: 'POST',
                        url: "php/products_formula.php",
                        data: {kg: Math.round(parseFloat(changedProducts[0].endKg)), prod: keyProduct},
                        success: function (data) {
                            var jsonCount = 0;
                            var productTiles = $("#table2 tbody tr:nth-child(n+1):nth-child(-n+" + machineCount + ") td:nth-child(n+" + intervalDates.startDate + "):nth-child(-n+" + intervalDates.endDate + ") div[product='" + keyProduct + "']");
                            var productTilesCount = productTiles.length;
                            var groupId = productTiles.attr('groupid');

                            if (data !== '') {
                                updateUndo();
                                jsonData = JSON.parse(data);
                                jsonCount = jsonData['shifts'];
                                var notAllowedMachines = jsonData['notAllowedMachines'];
                                //return;
                                var newKey,
                                    firstMac,
                                    tileCounter = 1,
                                    endFill = false,
                                    last_i = 0,
                                    last_shift = 0,
                                    unplacedProducts = [],
                                    is_fixed = (changedProducts[0].fixed) ? 'true' : 'false';
                                markedProducts = {};

                                //  delete product divs from table
                                var td_cell_blabla;
                                for (var shift in Produce.uniqueProducts[keyProduct].shifts) {
                                    if (Produce.uniqueProducts[keyProduct].shifts.hasOwnProperty(shift)) {
                                        var machineRows = Produce.uniqueProducts[keyProduct].shifts[shift];
                                        for (var i = 0; i < machineRows.length; i++) {
                                            td_cell_blabla = $('#r' + machineRows[i] + 'c' + shift);
                                            td_cell_blabla[0].innerHTML = '';
                                            td_cell_blabla.removeClass('dark');
                                            td_cell_blabla.removeClass('fixed-pos');
                                        }
                                    }
                                }
                                var machineRows = Object.keys(Produce.uniqueProducts[keyProduct].machines);
                                var machLen = machineRows.length;
                                var testEmptyCell;
                                for (var shift in Produce.uniqueProducts[keyProduct].shifts) {
                                    firstMac = (firstMac === undefined) ? shift : firstMac;
                                    if (Produce.uniqueProducts[keyProduct].shifts.hasOwnProperty(shift)) {

                                        if (jsonCount == 0) {
                                            last_i = machineRows[0];
                                            last_shift = shift;
                                            endFill = true;
                                        }

                                        for (var i = 0; i < machLen; i++) {
                                            testEmptyCell = $("#r" + machineRows[i] + "c" + shift);
                                            if (testEmptyCell[0] !== undefined && testEmptyCell.find('div')[0] === undefined) {
                                                newKey = machineRows[i] + '/' + shift;
                                                // end fill is true if kg is given less then the original
                                                if (!endFill) {
                                                    let markedRowIndex = $('.button-checkbox [data-id="' + machineRows[i] + '"]').closest('tr')[0].rowIndex + 1;
                                                    markedProducts[newKey] = {
                                                        'r': markedRowIndex,
                                                        'c': shift,
                                                        'rEnd': markedRowIndex,
                                                        'cEnd': shift,
                                                        'rId': parseInt(machineRows[i]),
                                                        'rEndId': parseInt(machineRows[i]),
                                                        'product': keyProduct,
                                                        'kg': (tileCounter == jsonCount) ? jsonData['kgLastShift'] : jsonData['kgPerShift'],
                                                        'fixed': is_fixed,
                                                        'groupid': groupId
                                                    };
                                                    last_shift = shift;
                                                    last_i = machineRows[i];
                                                } else {
                                                    unplacedProducts.push([parseInt(machineRows[i]), parseInt(shift)]);
                                                }

                                                if (tileCounter == jsonCount) {
                                                    last_i = machineRows[i];
                                                    last_shift = shift;
                                                    endFill = true;
                                                }
                                                if (productTilesCount == tileCounter) {
                                                    tileCounter++;
                                                    break;
                                                }
                                                tileCounter++;
                                            }
                                        }
                                    }
                                }

                                Move.start_col = firstMac;
                                Move.start_row = $('.button-checkbox [data-id="' + Produce.uniqueProducts[keyProduct].shifts[firstMac][0] + '"]')
                                    .closest('tr')[0].rowIndex + 1;
                                var el = $('#r' + machineMapping[Move.start_row - 1]['id'] + 'c' + Move.start_col);

                                // simulate mouse move to get rEnd and cRow defined for markedProducts
                                triggeProductPlacement(el, el.find('div')[0], true, 300, 300, markedProducts, false);

                                // if kg is set bigger then original kg
                                if (!endFill) {
                                    var start_row_product = Produce.uniqueProducts[keyProduct].shifts[firstMac][0];

                                    for (var i = tileCounter; i <= jsonCount; i++) {
                                        let markedRowIndex = $('.button-checkbox [data-id="' + start_row_product + '"]').closest('tr')[0].rowIndex + 1;

                                        markedProducts = {};
                                        markedProducts[start_row_product + '/' + firstMac] = {
                                            'r': markedRowIndex,
                                            'c': firstMac,
                                            'rEnd': markedRowIndex,
                                            'cEnd': firstMac,
                                            'rId': start_row_product,
                                            'rEndId': start_row_product,
                                            'product': keyProduct,
                                            'kg': (i == jsonCount) ? jsonData['kgLastShift'] : jsonData['kgPerShift'],
                                            'fixed': is_fixed,
                                            'groupid': groupId
                                        };
                                        Move.start_col = firstMac;
                                        Move.start_row = $('.button-checkbox [data-id="' + Produce.uniqueProducts[keyProduct].shifts[firstMac][0] + '"]')
                                            .closest('tr')[0].rowIndex + 1;

                                        var el = $('#r' + machineMapping[Move.start_row - 1]['id'] + 'c' + Move.start_col);

                                        // simulate mouse move to get rEnd and cRow defined for markedProducts
                                        triggeProductPlacement(el, el.find('div')[0], true, 300, 300, markedProducts, false);

                                        tileCounter = i;
                                        if (i === jsonCount) {
                                            endFill = true;
                                            break;
                                        }
                                    }
                                } else {
                                    // get endppoints to where to pull backwards
                                    if (jsonCount < Produce.uniqueProducts[keyProduct].count) {
                                        var endPoints = {};
                                        for (var i = unplacedProducts.length - 1; i >= 0; i--) {
                                            if (endPoints[unplacedProducts[i][0]] !== undefined) {
                                                if (unplacedProducts[i][1] < endPoints[unplacedProducts[i][0]].col)
                                                    endPoints[unplacedProducts[i][0]].col = unplacedProducts[i][1];
                                            } else {
                                                endPoints[unplacedProducts[i][0]] = {
                                                    col: unplacedProducts[i][1],
                                                    move: false
                                                };
                                                // check if in the next tile there is a product, that meen line should be moved backwards.
                                                var c = unplacedProducts[i][1] + 1;
                                                var nextCell = $("#r" + unplacedProducts[i][0] + "c" + c);
                                                // skip the free shifts... get over free shifts
                                                while (nextCell.hasClass('dark')) {
                                                    nextCell = $("#r" + unplacedProducts[i][0] + "c" + c);
                                                    c++;
                                                }

                                                if (nextCell[0].innerHTML !== "") {
                                                    endPoints[unplacedProducts[i][0]].move = true;
                                                }
                                            }

                                        }

                                        // pull front backward's to endpoint
                                        moveBackwards(endPoints);

                                    } else {
                                        console.log('same');
                                    }
                                }
                                updateMachineGroup();
                            } else {
                                showNotification("Nekorekts rezultāts.", 'danger');
                                error = true;
                            }
                        },
                        async: false
                    });
                    if (!error) {
                        Produce.table[keyProduct] = {
                            kg: curRow.find('td:nth-child(3)')[0].innerHTML,
                            fixed: curRow.find('td:nth-child(1) input').is(':checked').toString()
                        };
                        showNotification('\'<strong>' + keyProduct + '</strong>\' izmaiņas pabeigtas.', 'success');
                    }
                } else {
                    showNotification('Bez izmaiņām.', 'success');
                }
                $(bttn).text('Izmainīt');
            }, 50, this);
        });

        function triggeProductPlacement(el, trgt, not_del, cx, cy, markedP, updateGroups) {
            // glitch fix when this function starts and i dunno why but markedProducts sudenlly is empty, so i pass it like argument
            if (markedP !== undefined) markedProducts = markedP;

            Move.move_products['start'] = true;
            Move.move_products['move'] = true;
            var event = jQuery.Event("mousemove", {
                target: trgt,
                which: 1,
                buttons: 1
            });
            el.trigger(event);

            Move.move_products['start'] = true;
            Move.move_products['move'] = true;
            // trigger mousdown to place products
            var event = jQuery.Event("mouseup", {
                target: trgt,
                which: 1,
                clientX: cx,
                clientY: cy,
                notDelete: not_del,
                updateGroups: updateGroups
            });
            el.trigger(event);
        }

        function moveBackwards(endPoints) {
            var nextCell,
                endCell,
                lastCell = $("#table2 tbody tr:nth-child(1) td").last()[0].cellIndex + 1;

            for (var end_point in endPoints) {
                if (endPoints.hasOwnProperty(end_point)) {
                    var start_col = endPoints[end_point].col,
                        end_col = endPoints[end_point].col,
                        tableEnd = false,
                        setterCell;
                    expanded = 0;
                    if (endPoints[end_point].move) {
                        nextCell = $("#r" + end_point + "c" + start_col);
                        while (true) {
                            while (nextCell.hasClass('dark') || nextCell[0].innerHTML == "") {
                                start_col++;
                                if (start_col > lastCell) {
                                    tableEnd = true;
                                    break;
                                }
                                nextCell = $("#r" + end_point + "c" + start_col);
                            }

                            setterCell = $("#r" + end_point + "c" + end_col);
                            // change the products id to cell's name attr.. it should be the same.
                            setterCell[0].innerHTML = nextCell[0].innerHTML;
                            if (setterCell.find('div')[0] !== undefined) {
                                setterCell.find('div').attr('id', setterCell.attr('name'));
                            }

                            nextCell[0].innerHTML = '';

                            // get next end point
                            endCell = $("#r" + end_point + "c" + end_col);
                            while (endCell.hasClass('dark') || endCell[0].innerHTML != "") {
                                end_col++;
                                if (end_col > lastCell) {
                                    tableEnd = true;
                                    break;
                                }
                                endCell = $("#r" + end_point + "c" + end_col);
                            }

                            if (tableEnd) break;
                        }
                    }
                }
            }
        }

        $('#produceModal').on('shown.bs.modal', function () {
            $('#produceTable tbody').html('');
            var today = new Date();
            var today_formated = today.getFullYear() + '-' + pad((today.getMonth() + 1)) + '-' + pad(today.getDate());
            var maxD, minD, today_td = $('#' + today_formated + 'H');

            // if there is no unchangable products in the plan
            if ($('.history').length == 0) {
                minD = $('.day-td').first()[0].id.slice(0, -1);
                maxD = $('.day-td').last()[0].id.slice(0, -1);
            } else {
                if ($('.day-td').length == parseInt($('.history').length / 3)) {
                    maxD = '0-0-0';
                    minD = '0-0-0';
                } else {
                    var temphist = historyEndCell;
                    var startInterval = parseInt(temphist / 3) + 2;
                    minD = $('.header-day td:nth-child(' + startInterval + ')')[0].id.slice(0, -1);
                    maxD = $('.day-td').last()[0].id.slice(0, -1);
                }
            }
            // if there is  today in the showed plan then set the min date to it
            if (today_td[0] !== undefined) {
                minD = today_td[0].id.slice(0, -1);
            }
            // if no today line but history is showed
            var disabledCalendar = false;
            if (today_td[0] === undefined && $('.history').length > 1) {
                maxD = '0-0-0';
                minD = '0-0-0';
                disabledCalendar = true;
            }

            $('.popupDatepickerProduce').datepick("destroy");
            $('.popupDatepickerProduce').datepick({
                minDate: new Date(minD),
                maxDate: new Date(maxD),
                dateFormat: 'dd/mm/yyyy'
            });

            // set values of date inputs in modal open
            if (!disabledCalendar) {
                var startDate = minD.split('-')[2] + '/' + minD.split('-')[1] + '/' + minD.split('-')[0];
                var endDate = maxD.split('-')[2] + '/' + maxD.split('-')[1] + '/' + maxD.split('-')[0];
                $('[name="produceStart"]').val(startDate);
                $('[name="produceEnd"]').val(endDate);
            } else {
                $('[name="produceStart"]').val('');
                $('[name="produceEnd"]').val('');
            }
        });

        function makePrintProducts(startCol, mCount, mStartPoint, showPlan, editPlan, planId) {
            var prodSummaryHtml;
            var prodSummaryArr = [];
            var diffProducts = [];
            var rowLastCellIndex = $("#table2 tbody tr:first td").last()[0].cellIndex + 1;

            for (var i = mStartPoint; i < mCount + mStartPoint; i++) {
                var col = startCol - 1,
                    prevProduct = '',
                    currentCell,
                    $currentMachine = $('.left-header tbody > tr:nth-child(' + (i + 1) + ')'),
                    isGrouped = $currentMachine.find('.button-checkbox').hasClass('grouped'),
                    isGroupStart = $currentMachine.find('.button-checkbox').hasClass('start'),
                    machineId = $currentMachine.find('.button-checkbox button').data('id'),
                    machineName = $currentMachine.find('button').text(),
                    stylingTag = '';

                var additionalClasses = isGrouped ? 'grouped' : '';
                additionalClasses = isGroupStart ? additionalClasses + ' start' : additionalClasses;

                if (showPlan && planLayout[planId] && planLayout[planId]['items'][machineId]) {
                    additionalClasses += ' moving';
                    stylingTag = 'style="left: ' + planLayout[planId]['items'][machineId].left + 'px; ' +
                        'top: ' + planLayout[planId]['items'][machineId].top + 'px"';
                }

                prodSummaryHtml = '<div class="print-product-row ' +
                    additionalClasses + '" data-item-id="' + machineId + '" ' + stylingTag + '>' +
                    '<div class="print-machine-head ">' + machineName + '</div>';
                diffProducts.push({
                    name: machineName,
                    products: [],
                    isGrouped: isGrouped,
                    isGroupStart: isGroupStart
                });

                if (showPlan && !editPlan) {
                    while (true) {
                        col++;

                        if (col > rowLastCellIndex) {
                            expandTable(mCount, 14);
                            rowLastCellIndex = $("#table2 tbody tr:first td").last()[0].cellIndex + 1;
                        }

                        currentCell = $('#r' + machineId + 'c' + col);

                        if (currentCell.hasClass('redips-mark')) {
                            continue;
                        }
                        if (!currentCell.hasClass('redips-mark') && currentCell.find('div')[0] === undefined) {
                            break;
                        }

                        var curCellDiv = currentCell.find('div');
                        if (curCellDiv.attr('product') !== prevProduct) {
                            diffProducts[i - mStartPoint].products.push([curCellDiv.attr('product'),
                                curCellDiv.css('background-color'),
                                curCellDiv.css('color')]);
                            prodSummaryHtml += '<div style="background-color: ' + curCellDiv.css('background-color') + '; ' +
                                'color: ' + curCellDiv.css('color') + ';" ' +
                                'class="print-product">' + curCellDiv.attr('product') + '</div>';
                            if (diffProducts[i - mStartPoint].products.length > 3) break;
                        }
                        prevProduct = curCellDiv.attr('product');
                    }
                }

                prodSummaryHtml += '</div>';
                prodSummaryArr.push(prodSummaryHtml);
            }

            return {'prodSummaryArr': prodSummaryArr, 'diffProducts': diffProducts};
        }

        $(document).on('click', '.dropdown-menu.plan-menu li[data-plan-id] .btn-danger', function () {
            let listItem = $(this).closest('[data-plan-id]'),
                id = listItem.data('plan-id');

            delete planLayout[id];
            listItem.remove()

            savePlanLayout("Plāns izdzēsts.");
        });


        $(document).on('click', '[class^="plan-"]', function (e) {
            var diffProductsPrint = {},
                planMachineCount = machineCount,
                machineStartingPoint = 0,
                listItem = $(this).closest('li'),
                createPlan = listItem.hasClass('plan-create'),
                planId = listItem.data('plan-id'),
                editPlan =  $(e.target).closest('.edit-plan').length > 0;

            e.stopImmediatePropagation();

            currentPlanSummary = planId;
            planHtmlHolder = planHtmlHolder === '' ? $('#plansummaryPriorities')[0].outerHTML + $('#plansummaryCommentWrapper')[0].outerHTML : planHtmlHolder;
            $('#plansummaryModal .modal-content').removeClass('modal-make-plan modal-edit-plan');

            if (
                (!editPlan && !createPlan && e.target.type !== '') ||
                $(e.target).data('dismiss') === 'confirmation' ||
                $(e.target).data('apply') === 'confirmation' ||
                $(e.target).closest('.btn-danger').length > 0
            ) {
                return;
            }

            if (markedShift < 1 && !editPlan && !createPlan) {
                showNotification("Nav atzīmēta starta maiņa!", 'danger');
                return false;
            }

            // make header of date
            if (!editPlan && !createPlan) {
                var cellDateArr = $(getFirstRowSelector() + markedShift).attr('name').split('/')[0].split('-');
                var cellDate = cellDateArr[2] + '.' + cellDateArr[1] + '.' + cellDateArr[0];
                $('#plansummaryDate').html('Teksturēšanas ražošanas plāns <span>' + cellDate + "</span>");
            }

            // make table content for machine products
            var gatheredProductInfo = makePrintProducts(
                markedShift,
                planMachineCount,
                machineStartingPoint,
                !createPlan,
                editPlan,
                planId
            );
            var planSummaryArr = gatheredProductInfo['prodSummaryArr'];

            var planSummaryHtmlLeft = '';

            for (var i = 0; i < planSummaryArr.length; i++) {
                planSummaryHtmlLeft += planSummaryArr[i];
            }

            if (!editPlan && !createPlan) {
                $('#plansummaryModal .print-left > div').remove();
                $('#plansummaryModal .print-left').append(planSummaryHtmlLeft);
                $('#dragabbleMachines').html('');
                $('#plansummaryModal .print-left').css('height', planLayout[planId]['height']);

                if (planLayout[planId]['html']) {
                    $('#plansummaryPriorities').remove();
                    $('#plansummaryCommentWrapper')[0].outerHTML = planLayout[planId]['html'];

                    let topPosAdjustmentPlan = planLayout[planId]['htmlTopPosAdjustment'] || 0;

                    $('#plansummaryPriorities').css('top', parseInt($('#plansummaryPriorities').css('top').replace('px', ''), 10) - topPosAdjustmentPlan);
                    $('#plansummaryCommentWrapper').css('top', parseInt($('#plansummaryCommentWrapper').css('top').replace('px', ''), 10) - topPosAdjustmentPlan);
                } else {
                    $('#plansummaryPriorities, #plansummaryCommentWrapper').html('');
                }

                if ($('#prioritiesTable').length) {
                    $('#prioritiesTable').append('<tfoot><tr><td colspan="2">' +
                        '<button type="button" class="btn btn-xs btn-default">Pievienot rindu+</button>' +
                        '</td></tr></tfoot>')
                }
            } else {
                $('#dragabbleMachines').html(planSummaryHtmlLeft);
                $('#dragabbleMachines').height('auto');
                setTimeout(function () {
                    $('#dragabbleMachines').height($('#dragabbleMachines').height());
                }, 300);
                $('#plansummaryModal .print-left > div').remove();
                $('#plansummaryModal .print-left').css('height', 'auto');
                $('#plansummaryModal .print-left textarea').css('height', 'auto');
                $('#plansummaryName input').val('');
                $('#plansummaryPriorities, #plansummaryCommentWrapper, #plansummaryComment').removeAttr('style');
                $('#prioritiesTable tfoot').remove();

                if (editPlan) {
                    $('#plansummaryModal .print-left textarea').css('height', planLayout[planId]['height']);
                    $('#plansummaryName input').val(planLayout[planId]['name']);

                    $('#plansummaryPriorities').remove();
                    $('#plansummaryCommentWrapper')[0].outerHTML = planLayout[planId]['html'] ? planLayout[planId]['html'] : planHtmlHolder;

                    let movingProduct = $('.print-product-row.moving');
                    movingProduct.hide();
                    $('#plansummaryPriorities, #plansummaryCommentWrapper').hide();

                    setTimeout(function () {
                        let topPosAdjustment = $("#dragabbleMachines").outerHeight() + 16,
                            topPosAdjustmentPlan = planLayout[planId]['htmlTopPosAdjustment'] || 0;

                        movingProduct.each(function () {
                            let item = $(this);
                            item.css('top', parseInt(item.css('top').replace('px', ''), 10) + topPosAdjustment);
                        })

                        $('#plansummaryPriorities').css('top', parseInt($('#plansummaryPriorities').css('top').replace('px', ''), 10) + topPosAdjustment - topPosAdjustmentPlan);
                        $('#plansummaryCommentWrapper').css('top', parseInt($('#plansummaryCommentWrapper').css('top').replace('px', ''), 10) + topPosAdjustment - topPosAdjustmentPlan);

                        movingProduct.show();
                        $('#plansummaryPriorities, #plansummaryCommentWrapper').show();
                    }, 300);
                }
            }

            if (editPlan || createPlan) {
                var printRows = $('.print-product-row'),
                    posX = 0,
                    posY = 0,
                    mouseX = 0,
                    mouseY = 0,
                    mouseXMod = 0,
                    mouseYMod = 0;

                function mouseDown(e) {
                    e.preventDefault();
                    window.moveElement = $(e.target).closest('.print-product-row')[0];
                    posX = e.clientX - window.moveElement.offsetLeft;
                    posY = e.clientY - window.moveElement.offsetTop;

                    window.addEventListener('mousemove', moveElement, false);
                }

                function mouseUp() {
                    window.removeEventListener('mousemove', moveElement, false);

                    if (window.moveElement) {
                        let machineDragItems = $('.print-left').offset().top,
                            moveElementTop = $(window.moveElement).offset().top;

                        // if inside the moving area
                        if (moveElementTop >= machineDragItems && moveElementTop <= machineDragItems + $('.print-left').height() - 10) {
                            // window.moveElement.classList.remove('moving');
                        } else {
                            // if outside the moving area
                            window.moveElement.classList.remove('moving');
                        }
                    }

                    window.moveElement = false;
                }

                function moveElement(e) {
                    mouseX = e.clientX - posX;
                    mouseY = e.clientY - posY;

                    mouseXMod = mouseX % 6;
                    mouseYMod = mouseY % 6;

                    mouseY = mouseYMod < 3 ? mouseY - mouseYMod : 6 - mouseYMod + mouseY;
                    mouseX = mouseXMod < 3 ? mouseX - mouseXMod : 6 - mouseXMod + mouseX;

                    window.moveElement.style.left = mouseX + 'px';
                    window.moveElement.style.top = mouseY + 'px';
                    window.moveElement.classList.add('moving');
                }

                printRows.each(function () {
                    this.addEventListener('mousedown', mouseDown, false);
                });

                window.addEventListener('mouseup', mouseUp, false);

                // Moving of comment and priority table
                function moveElementOther(e) {
                    mouseX = e.clientX - posX;
                    mouseY = e.clientY - posY;

                    window.moveElement.style.left = mouseX + 'px';
                    window.moveElement.style.top = mouseY + 'px';
                    window.moveElement.classList.add('moving');
                }

                $('#plansummaryPriorities span.glyphicon')[0].addEventListener(
                    'mousedown',
                    function (e) {
                        e.preventDefault();
                        window.moveElement = $(e.target).closest('#plansummaryPriorities')[0];
                        posX = e.clientX - window.moveElement.offsetLeft;
                        posY = e.clientY - window.moveElement.offsetTop;

                        window.addEventListener('mousemove', moveElementOther, false);
                    },
                    false
                );
                $('#plansummaryCommentWrapper span.glyphicon')[0].addEventListener(
                    'mousedown',
                    function (e) {
                        e.preventDefault();
                        window.moveElement = $(e.target).closest('#plansummaryCommentWrapper')[0];
                        posX = e.clientX - window.moveElement.offsetLeft;
                        posY = e.clientY - window.moveElement.offsetTop;

                        window.addEventListener('mousemove', moveElementOther, false);
                    },
                    false
                );

                window.addEventListener('mouseup', function () {
                    window.removeEventListener('mousemove', moveElementOther, false);
                    window.moveElement = false;
                }, false);
            }

            // make priorities content of products
            // gather different products
            var diffProductsBunch = gatheredProductInfo['diffProducts'];
            diffProductsPrint = {};
            for (var key in diffProductsBunch) {
                if (diffProductsBunch.hasOwnProperty(key)) {
                    for (var i = 0; i < diffProductsBunch[key].products.length; i++) {
                        var currentProduct = diffProductsBunch[key].products[i];
                        if (diffProductsPrint[currentProduct[0].toUpperCase()] === undefined) {
                            diffProductsPrint[currentProduct[0].toUpperCase()] = {
                                backgroundColor: currentProduct[1],
                                textColor: currentProduct[2]
                            }
                        }
                    }
                }
            }

            // show modal
            $('#plansummaryModal').modal('show');

            if (createPlan || editPlan) {
                $('#plansummaryModal .modal-content').addClass('modal-make-plan');
            }

            if (editPlan) {
                $('#plansummaryModal .modal-content').addClass('modal-edit-plan');
            }
        });

        $(document).on('blur keyup paste input', '#prioritiesTable tbody td', function () {
            var typeText = $(this).text().toUpperCase();
            if (productsColor[typeText] !== undefined) {
                $(this).css('background-color', productsColor[typeText]);
                $(this).css('color', generateTextColor(productsColor[typeText]));
            } else {
                $(this).css('background-color', '');
                $(this).css('color', '#000');
            }
        });

        $('#print-bttn').click(function () {
            window.print();
        });

        $('#bttn-save-plansummary').click(function () {
            var rawDate = $('#plansummaryDate span').text().split('.');
            var planDate = formatDate(new Date(rawDate[1] + '/' + rawDate[0] + '/' + rawDate[2]));
            var planContent = $('#plansummaryModal .modal-body .summary-body').html();
            var createPlan = $('#plansummaryModal .modal-content ').hasClass('modal-make-plan');
            var editPlan = $('#plansummaryModal .modal-content ').hasClass('modal-edit-plan');

            if (!createPlan) {
                $.post("php/plansummary_save.php", {
                    planDate: planDate,
                    planContent: planContent,
                    planType: currentPlanSummary ? planLayout[currentPlanSummary]['name'] : '',
                    action: '_n'
                })
                    // when post is finished
                    .done(function () {
                        showNotification("Plāna kopsavilkums saglabāts.", 'success');
                    })
                    .fail(function () {
                        showNotification('Kaut kas nogājis greizi.', 'danger');
                    });
            } else {
                let draggedMachines = $("#dragabbleMachines .print-product-row.moving"),
                    topPosAdjustment = $("#dragabbleMachines").outerHeight() + 16,
                    timestamp = new Date().getTime(),
                    planName = $('#plansummaryName input').val() || 'Plāns ' + (Object.keys(planLayout).length + 1),
                    newPlan = {
                        name: planName,
                        items: {},
                        height: $("#plansummaryProducts .print-left textarea").outerHeight() + 4,
                        html: $('#plansummaryPriorities')[0].outerHTML + $('#plansummaryCommentWrapper')[0].outerHTML,
                        htmlTopPosAdjustment: topPosAdjustment
                };

                draggedMachines.each(function () {
                    let item = $(this);

                    newPlan.items[item.data('item-id')] = {
                        left: parseInt(item.css('left').replace('px', ''), 10),
                        top: parseInt(item.css('top').replace('px', ''), 10) - topPosAdjustment
                    }
                });

                currentPlanSummary = editPlan ? currentPlanSummary : timestamp;
                planLayout[currentPlanSummary] = newPlan;

                savePlanLayout("Plāns saglabāts.");

                $('#plansummaryModal .modal-content ').addClass('modal-edit-plan');
            }
        });

        $(document).on('click', '#prioritiesTable tfoot button', function () {
            $('#prioritiesTable tbody').append($('#prioritiesTable tbody tr:last-child')[0].outerHTML);
            $('#prioritiesTable tbody tr:last-child td:last-child')
                .html(parseInt($('#prioritiesTable tbody tr:last-child td:last-child').html(), 10) + 1);
        });


        //// Rectangle draw, marking products
        /////////////////////////
        Move.mouseStillDown = false;
        Move.start_x = 0;
        Move.start_y = 0;
        Move.start_row = 1;
        Move.start_col = 1;
        Move.old_row = 0;
        Move.old_col = 0;
        Move.move_products = {'start': false, 'move': false};
        Move.modal_action = false;
        Move.mProdCountInRow = {};

        function drawRect(e, old_r, old_c) {
            // save original event
            old_e = e;

            // if mouse is over empty cell
            if (e.target.tagName == 'TD') {
                e = e.target;
            }
            // if mouse is over product
            else if (e.target.tagName == 'DIV') {
                e = e.target.parentElement;
            }
            // if draw cube right down
            if (old_e.pageY - Move.start_y >= 0 && old_e.pageX - Move.start_x >= 0) {
                Move.old_row = e.parentNode.rowIndex + 1;
                Move.old_col = e.cellIndex + 1;
                if ((old_r != Move.old_row || old_c != Move.old_col)) {
                    $('.temp-marked').remove();
                    var rows = [Move.start_row, e.parentNode.rowIndex + 1], cols = [Move.start_col, e.cellIndex + 1];
                    $("#table2 tr:nth-child(n+" + Math.min(...rows) + "):nth-child(-n+" + Math.max(...rows) + ") td:nth-child(n+" + Math.min(...cols) + "):nth-child(-n+" + Math.max(...cols) + ")").append('<div class="temp-marked"></div>');
                }
            }
            // draw cube left up
            else if (old_e.pageY - Move.start_y < 0 && old_e.pageX - Move.start_x < 0) {
                Move.old_row = e.parentNode.rowIndex + 1;
                Move.old_col = e.cellIndex + 1;
                if ((old_r != Move.old_row || old_c != Move.old_col)) {
                    $('.temp-marked').remove();
                    var rows = [e.parentNode.rowIndex + 1, Move.start_row], cols = [e.cellIndex + 1, Move.start_col];
                    $("#table2 tr:nth-child(n+" + Math.min(...rows) + "):nth-child(-n+" + Math.max(...rows) + ") td:nth-child(n+" + Math.min(...cols) + "):nth-child(-n+" + Math.max(...cols) + ")").append('<div class="temp-marked"></div>');
                }
            }
            // draw cube left down
            else if (old_e.pageX - Move.start_x <= 0) {
                Move.old_row = e.parentNode.rowIndex + 1;
                Move.old_col = e.cellIndex + 1;
                if ((old_r != Move.old_row || old_c != Move.old_col)) {
                    $('.temp-marked').remove();
                    var rows = [Move.start_row, e.parentNode.rowIndex + 1], cols = [e.cellIndex + 1, Move.start_col];
                    $("#table2 tr:nth-child(n+" + Math.min(...rows) + "):nth-child(-n+" + Math.max(...rows) + ") td:nth-child(n+" + Math.min(...cols) + "):nth-child(-n+" + Math.max(...cols) + ")").append('<div class="temp-marked"></div>');
                }
            }
            // draw right up
            else if (old_e.pageY - Move.start_y <= 0) {
                Move.old_row = e.parentNode.rowIndex + 1;
                Move.old_col = e.cellIndex + 1;
                if ((old_r != Move.old_row || old_c != Move.old_col)) {
                    $('.temp-marked').remove();
                    var rows = [e.parentNode.rowIndex + 1, Move.start_row], cols = [Move.start_col, e.cellIndex + 1];
                    $("#table2 tr:nth-child(n+" + Math.min(...rows) + "):nth-child(-n+" + Math.max(...rows) + ") td:nth-child(n+" + Math.min(...cols) + "):nth-child(-n+" + Math.max(...cols) + ")").append('<div class="temp-marked"></div>');
                }
            }
        }

        Move.prevMovePosProduct = [];
        $("#table2").mousemove(function (e) { // move rect on mousemove over table2
            if (e.which == 1 && e.buttons > 0) {
                if (Move.move_products['start'] && Move.move_products['move'] && Object.keys(markedProducts).length > 0) {
                    //////////////////////////////////////////////////
                    // MOVING ALREADY MARKED PRODUCTS!!
                    //////////////////////////////////////////////////
                    // unhighlight previous cells when moving products
                    $('.marked').removeClass('marked');
                    $('.td-marked').removeClass('td-marked');

                    Move.prevMovePosProduct = [];

                    // highligt cells on moving products
                    for (var key in markedProducts) {
                        if (markedProducts.hasOwnProperty(key)) {
                            var row, col;
                            if (e.target.tagName == "TD") {
                                row = markedProducts[key].r - (Move.start_row - e.target.parentNode.rowIndex) + 1;
                                col = markedProducts[key].c - (Move.start_col - e.target.cellIndex) + 1;
                            } else {
                                row = markedProducts[key].r - (Move.start_row - e.target.parentElement.parentNode.rowIndex) + 1;
                                col = markedProducts[key].c - (Move.start_col - e.target.parentElement.cellIndex) + 1;
                            }
                            markedProducts[key].rEnd = row;
                            markedProducts[key].cEnd = col;

                            if (row > 0 && row <= machineCount) {
                                markedProducts[key].rEndId = machineMapping[row - 1]['id'];

                                var td_html = $("#r" + machineMapping[row - 1]['id'] + "c" + col);
                                // highlight product
                                if (td_html.children()[0] !== undefined) {
                                    td_html.children().addClass('marked');
                                } else {
                                    td_html.addClass('td-marked');
                                }

                                Move.prevMovePosProduct.push(td_html);
                            }
                        }
                    }
                } else if (Move.mouseStillDown) { // marking products
                    Move.move_products['start'] = false;
                    markedProducts = {};
                    drawRect(e, Move.old_row, Move.old_col);
                }
            }
        });

        $("#table2").mousedown(function (e) {
            if (e.which == 1) {
                $('.temp-marked').remove();

                // update undo array on keydown and when there is no marked products.
                updateUndo();

                Move.mouseStillDown = true;
                Move.start_x = e.pageX, Move.start_y = e.pageY;
                if (!ctrlPressed) {
                    Move.start_row = $(e.target).closest('tr')[0].rowIndex + 1;
                    Move.start_col = $(e.target).closest('td').cellIndex + 1;
                } else {
                    $('.marked').removeClass('marked');
                    markedProducts = {};
                    Move.start_row = $(e.target).closest('tr')[0].rowIndex + 1;
                    Move.start_col = $(e.target).closest('td').cellIndex + 1;
                }
                if (!ctrlPressed) {
                    // button pressed on now marked products
                    if (e.target.className == 'blue marked' && Move.move_products['start']) {
                        Move.start_row = e.target.parentNode.parentNode.rowIndex + 1,
                            Move.start_col = e.target.parentNode.cellIndex + 1;
                        // if marked products start to move and it starts from product div not empty cell
                        if (Move.move_products['start']) {
                            Move.move_products['move'] = true;
                        }
                    }
                    // if buton pressed on already moved marked products
                    else if (e.target.className == 'blue marked' && !Move.move_products['start']) {
                        Move.move_products['start'] = true;
                        Move.move_products['move'] = true;
                        Move.old_row = Move.start_row = e.target.parentNode.parentNode.rowIndex + 1,
                            Move.old_col = Move.start_col = e.target.parentNode.cellIndex + 1;
                        // place it in marked cells not products
                        var curCell = e.target.parentNode;
                        // get cell in marked products
                        getMarkedProducts(e);
                    } else if (e.target.className == 'blue') {
                        Move.move_products['start'] = true; //Move.move_products['move'] = true;
                        Move.old_row = Move.start_row = e.target.parentNode.parentNode.rowIndex + 1,
                            Move.old_col = Move.start_col = e.target.parentNode.cellIndex + 1;
                        // place it in marked cells not products
                        var curCell = e.target.parentNode;
                        // get cell in marked products
                        $('.marked').removeClass('marked');
                        getMarkedProducts(e);
                    } else {
                        for (var i = Move.prevMovePosProduct.length - 1; i >= 0; i--) {
                            Move.prevMovePosProduct[i].children().removeClass('marked');
                        }
                        ;
                        for (var key in markedProducts) {
                            if (markedProducts.hasOwnProperty(key)) {
                                $('[name="' + key + '"] div').removeClass('marked');
                            }
                        }
                        $('.marked').removeClass('marked');
                        Move.start_row = e.target.parentNode.rowIndex + 1, Move.start_col = e.target.cellIndex + 1;
                        Move.move_products['start'] = false;
                        Move.move_products['move'] = false;
                        markedProducts = {};
                    }
                }
                statusBar['marked']['count'] = $('.marked').length;
                $('#status div').html(statusBar['marked']['label'] + ': ' + statusBar['marked']['count']);
            }
        });

        /*
============================================================
           ######### CONTEXT MENU #########
============================================================
*/
        $('#table2').contextmenu(function (e) {
            $('#marked-modal .modal-dialog').attr('style', 'left: ' + (e.clientX) + 'px; top: ' + (e.clientY) + 'px;');
            $('#marked-modal').modal('show');
            $(".modal-backdrop").remove();
            Move.start_x = e.pageX, Move.start_y = e.pageY;

            PBuffer.mousemove = e;
            return false;
        });
        // fix to pop up context menu when one already is shown
        $(document).on('mousedown', '#marked-modal', function (e) {
            if (e.which == 3) {
                $('#marked-modal').modal('hide');
            }
        });
        /*
============================================================
           ######### CONTEXT MENU END #########
============================================================
*/
        $('html').mouseup(function () {
            //if ($('.temp-marked')[0] !== undefined) {
            //    $("body").trigger(e);
            //}
            $('.td-marked').removeClass('td-marked');
        });
        $("#right").mouseup(function (e) {
            if (e.which === 1) {
                // if some cells is marked
                if ($('.temp-marked')[0] !== undefined) {
                    // delete marking drawing
                    $('.temp-marked').remove();
                    // get products
                    getMarkedProducts(e);
                    statusBar['marked']['count'] = $('.marked').length;
                    $('#status div').html(statusBar['marked']['label'] + ': ' + statusBar['marked']['count']);

                }
                if (Object.keys(markedProducts).length > 0) {
                    // true if modal is not canceled
                    Move.move_products['start'] = true;
                    //Move.move_products['move'] = true;
                } else {
                    Move.move_products['start'] = false;
                    Move.move_products['move'] = false;
                }
                // if released mouse when moving many products, then apply new pos, palce products in new pos
                if (Move.move_products['start'] && Move.move_products['move']) {
                    // check if products can be placed in new place
                    var is_valid = true;
                    // checks if product can be placed in new place
                    for (var key in markedProducts) {
                        if (markedProducts.hasOwnProperty(key)) {
                            // new place is out of table bounds then don't placed
                            if (markedProducts[key].rEnd < 1 || markedProducts[key].rEnd > machineCount ||
                                markedProducts[key].cEnd < historyEndCell /*|| markedProducts[key].cEnd > lastTdIndex*/) {
                                is_valid = false;
                                showNotification('Produkti neietilpst tabulā.', 'danger');
                                break;
                            }
                        }
                    }
                    // if copy than dont delete prev products. In case of cut and paste del proucts from prev place
                    if (!e.notDelete || e.notDelete === undefined) {
                        for (var key in markedProducts) {
                            if (markedProducts.hasOwnProperty(key)) {
                                var td_html = $("#r" + markedProducts[key].rId + "c" + markedProducts[key].c);
                                // if new products is not out of table bounds
                                if (is_valid) {
                                    td_html.html('');
                                }
                            }
                        }
                    }
                    $('.td-marked').removeClass('td-marked');

                    if (is_valid) {
                        if (e.updateGroups === undefined) updateMachineGroup();

                        var td_free, old_row, attrProduct, rowLastCellIndex,
                            td_cell, next_cell_html, next_cell, groupid, product, td_div,
                            markedProductsLen = Object.keys(markedProducts).length,
                            currentObjectIndex = 0, lastTileIsSet = false;
                        for (var key in markedProducts) {
                            if (markedProducts.hasOwnProperty(key)) {
                                currentObjectIndex++;
                                rowLastCellIndex = $("#table2 tbody tr:nth-child(" + markedProducts[key].rEnd + ") td").last()[0].cellIndex + 1;
                                next_cell = $('#r' + markedProducts[key].rEndId + 'c' + markedProducts[key].cEnd);
                                while (true) {
                                    if (markedProducts[key].cEnd >= rowLastCellIndex) {
                                        expandTable(14);
                                        rowLastCellIndex = $("#table2 tbody tr:nth-child(" + markedProducts[key].rEnd + ") td").last()[0].cellIndex + 1;
                                    }
                                    // landed on fixed cell
                                    if (next_cell.hasClass('dark') && next_cell.hasClass('fixed-pos')) {
                                        if (next_cell.find('div').attr('product') == markedProducts[key].product)
                                            break;
                                    }
                                    if (next_cell.hasClass('dark')) {
                                        markedProducts[key].cEnd++;
                                        next_cell = $('#r' + markedProducts[key].rEndId + 'c' + markedProducts[key].cEnd);
                                    } else {
                                        break;
                                    }
                                }
                                next_cell_html = next_cell.html();

                                groupid = next_cell.find('div').attr('groupid');
                                product = next_cell.find('div').attr('product');

                                if (product !== undefined && product == markedProducts[key].product) {
                                    next_cell.html(setProductDiv(next_cell.attr('name'), markedProducts[key].product, next_cell.find('div').attr('groupid'), true, markedProducts[key].kg, markedProducts[key].fixed));
                                } else {
                                    next_cell.html(setProductDiv(next_cell.attr('name'), markedProducts[key].product, markedProducts[key].groupid, true, markedProducts[key].kg, markedProducts[key].fixed));
                                    if (currentObjectIndex >= markedProductsLen) {
                                        if (!lastTileIsSet) {
                                            lastAddedTile = next_cell[0].cellIndex + 1;
                                            lastTileIsSet = true;
                                        }
                                    }
                                }
                                next_cell.find('div').addClass('ignore');

                                if (markedProducts[key].fixed == 'true' || markedProducts[key].fixed == '1') {
                                    next_cell.addClass('dark');
                                    next_cell.addClass('fixed-pos');
                                } else {
                                    next_cell.removeClass('dark');
                                    next_cell.removeClass('fixed-pos');
                                }
                                // product is landed on empty cell
                                if (product === undefined) {
                                    if (currentObjectIndex >= markedProductsLen) {
                                        if (!lastTileIsSet) {
                                            lastAddedTile = next_cell[0].cellIndex + 1;
                                            lastTileIsSet = true;
                                        }
                                    }
                                    continue;
                                }
                                if (groupMachines[groupid] === undefined) {
                                    updateMachineGroup();
                                    groupid = next_cell.find('div').attr('groupid');
                                }
                                var productMachines = groupMachines[groupid];

                                productMachines.sort(function (a, b) {
                                    return a - b;
                                });
                                var startColIndex = next_cell[0].cellIndex + 1;
                                var startRowIndex = $.inArray(markedProducts[key].rEnd, productMachines);
                                startRowIndex = (startRowIndex != -1) ? startRowIndex + 1 : 0;

                                var colCheck = startColIndex + 1;
                                td_free = $(getFirstRowSelector() + colCheck);
                                while (td_free.hasClass('dark')) {
                                    if (colCheck >= rowLastCellIndex) {
                                        expandTable(14);
                                        rowLastCellIndex = $("#table2 tbody tr:nth-child(" + markedProducts[key].rEnd + ") td").last()[0].cellIndex + 1;
                                    }
                                    colCheck++;
                                    td_free = $(getFirstRowSelector() + colCheck);
                                }
                                colCheck = (colCheck === rowLastCellIndex) ? startColIndex + 1 : colCheck;
                                var nextColProduct = $('#table2 tr:nth-child(n+' + productMachines[0] + '):nth-child(-n+' + productMachines[productMachines.length - 1] + ') td:nth-child(' + colCheck + ') div[product="' + product + '"]');

                                if (nextColProduct.length === 0 && startRowIndex !== 0) {
                                    startRowIndex = 0;
                                }
                                var exitLoop = false;

                                while (true) {
                                    var changeProduct = false;
                                    for (var col = startColIndex; col <= rowLastCellIndex; col++) {
                                        if (col >= rowLastCellIndex) {
                                            expandTable(14);
                                            rowLastCellIndex = $("#table2 tbody tr:nth-child(" + markedProducts[key].rEnd + ") td").last()[0].cellIndex + 1;
                                            productMachines = groupMachines[groupid];
                                            productMachines.sort(function (a, b) {
                                                return a - b;
                                            });
                                        }

                                        // free shift skip forward
                                        if ($(getFirstRowSelector() + col).hasClass('dark')) {
                                            continue;
                                        }
                                        for (var row = startRowIndex; row < productMachines.length; row++) {
                                            td_cell = $("#r" + machineMapping[productMachines[row] - 1]['id'] + "c" + col);
                                            td_div = td_cell.find('div');
                                            //if ( td_cell.find('div')[0] === undefined ){

                                            //}
                                            attrProduct = td_div.attr('product');
                                            if (attrProduct !== product && !td_cell.hasClass('dark')) {
                                                colCheck = col - 1;
                                                while (td_free.hasClass('dark')) {
                                                    colCheck--;
                                                    if (colCheck === 0) break;
                                                }
                                                td_free = $(getFirstRowSelector() + colCheck);
                                                colCheck = (colCheck === 0) ? col - 1 : colCheck;

                                                // check if this product is in previous column
                                                var prevColProduct = false, cellItem = [];
                                                for (var rowIndex = 0; rowIndex < productMachines.length; rowIndex++) {
                                                    cellItem = $('#r' + productMachines[rowIndex] + 'c' + colCheck + ' div[product="' + attrProduct + '"]');
                                                    if (cellItem.length !== 0) {
                                                        prevColProduct = true;
                                                        break;
                                                    }
                                                }
                                                // check if this product is in current column
                                                var curColProduct = false;
                                                for (rowIndex = 0; rowIndex < row; rowIndex++) {
                                                    cellItem = $('#r' + productMachines[rowIndex] + 'c' + col + ' div[product="' + attrProduct + '"]');
                                                    if (cellItem.length !== 0) {
                                                        curColProduct = true;
                                                        break;
                                                    }
                                                }
                                                if (attrProduct === undefined && !td_div.hasClass('ignore')) {
                                                    td_cell.html(next_cell_html);
                                                    td_cell.find('div').attr('id', td_cell.attr('name'));
                                                    if (currentObjectIndex >= markedProductsLen && !lastTileIsSet) {
                                                        if (td_cell.find('div').attr('product') == markedProducts[key].product) {
                                                            lastAddedTile = td_cell[0].cellIndex + 1;
                                                            lastTileIsSet = true;
                                                        }
                                                    }
                                                    changeProduct = true;
                                                    exitLoop = true;
                                                } else if (!(prevColProduct || curColProduct) && !td_div.hasClass('ignore')) {
                                                    var original_next_cell_html = next_cell_html;
                                                    next_cell_html = td_cell.html();
                                                    groupid = td_div.attr('groupid');
                                                    product = attrProduct;

                                                    td_cell.html(original_next_cell_html);
                                                    td_cell.find('div').attr('id', td_cell.attr('name'));

                                                    if (currentObjectIndex >= markedProductsLen && !lastTileIsSet) {
                                                        if (td_cell.find('div').attr('product') === markedProducts[key].product) {
                                                            lastAddedTile = td_cell[0].cellIndex + 1;
                                                            lastTileIsSet = true;
                                                        }
                                                    }
                                                    changeProduct = true;
                                                }
                                                if (changeProduct) {
                                                    old_row = productMachines[row];
                                                    productMachines = groupMachines[groupid];
                                                    productMachines.sort(function (a, b) {
                                                        return a - b;
                                                    });
                                                    startColIndex = td_cell[0].cellIndex + 1;
                                                    startColIndex = (row + 1 === productMachines[productMachines.length - 1]) ? startColIndex + 1 : startColIndex;
                                                    startRowIndex = $.inArray(old_row, productMachines);
                                                    startRowIndex = (startRowIndex !== -1 && productMachines.length - 1 >= startRowIndex) ? startRowIndex + 1 : 1;
                                                    break;
                                                }
                                            }
                                        }
                                        if (changeProduct) break;
                                        startRowIndex = 0;
                                    }
                                    if (exitLoop) break;
                                }

                            }
                        }
                        $('.ignore').removeClass('ignore');

                        // new array for new td ids
                        var newProds = {};
                        // get new row and column position for marked products when they are released to be able to move them again
                        for (var key in markedProducts) {
                            if (markedProducts.hasOwnProperty(key)) {
                                markedProducts[key].r = markedProducts[key].rEnd;
                                markedProducts[key].c = markedProducts[key].cEnd;
                                markedProducts[key].groupid = $("#r" + markedProducts[key].rEndId + "c" + markedProducts[key].c + " div").attr('groupid');
                                newProds[$('#r' + markedProducts[key].rEndId + 'c' + markedProducts[key].c).attr('name')] = markedProducts[key];
                            }
                        }
                        markedProducts = newProds;
                        if (Object.keys(markedProducts).length > 1) {
                            $('.marked').removeClass('marked');
                        }
                        markedProducts = {};
                        Move.move_products['start'] = false;
                        Move.move_products['move'] = false;
                        if (e.updateGroups || e.updateGroups === undefined) updateMachineGroup();
                    } else {
                        // remove marked class from products
                        $('.marked').removeClass('marked');
                        // old product place. remove all prev products
                        $('.td-marked').removeClass('td-marked');
                        for (var key in markedProducts) {
                            if (markedProducts.hasOwnProperty(key)) {
                                // mark products that was moved
                                $("#r" + markedProducts[key].rId + "c" + markedProducts[key].c + " div").addClass('marked');
                                // reset cell end position to start position because there is out of table bounds error
                                markedProducts[key].rEnd = markedProducts[key].r;
                                markedProducts[key].cEnd = markedProducts[key].c;
                                markedProducts[key].rEndId = markedProducts[key].rId;
                            }
                        }
                    }
                }
                if (e.initShifFreeDays) markedProducts = {};

                Move.mouseStillDown = false;
            }
        });

        $('#delete-marked-bttn').click(function () {
            Move.modal_action = true;

            updateUndo();

            for (var key in markedProducts) {
                // if new marked cell not in old then unmark it
                if (markedProducts.hasOwnProperty(key)) {
                    $('[name="' + key + '"]')[0].innerHTML = "";
                }
            }
            ;
            markedProducts = {};
            Move.move_products['start'] = false;
            Move.move_products['move'] = false;
            updateMachineGroup();
        });

        $('#marked-modal').on('hidden.bs.modal', function () {
            if (!Move.modal_action) {
                for (var key in markedProducts) {
                    if (markedProducts.hasOwnProperty(key)) {
                        if ($('[name="' + key + '"]')[0].innerHTML.trim() != '') {
                            $('[name="' + key + '"] div').removeClass('marked');
                        }
                    }
                }
                if (!Move.move_products['start']) {
                    markedProducts = {};
                }
            }
            Move.modal_action = false;
        });

        $('#cut-marked-bttn').click(function (e) {
            if (Object.keys(markedProducts).length > 0) {
                PBuffer.copy = {}
                PBuffer.cut = markedProducts;
            }
        });
        $('#copy-marked-bttn').click(function (e) {
            if (Object.keys(markedProducts).length > 0) {
                PBuffer.cut = {};
                PBuffer.copy = markedProducts;
            }
        });
        $('#paste-marked-bttn').click(function (e) {

            updateUndo();

            // get first hash key
            var clipBoard = (Object.keys(PBuffer.cut).length > 0) ? PBuffer.cut : PBuffer.copy;
            for (var firstKey in clipBoard) {
                break;
            }
            // if clipboard not empty
            if (firstKey !== undefined) {
                PBuffer.cornerY = clipBoard[firstKey].r;
                PBuffer.cornerX = clipBoard[firstKey].c;
                for (var key in clipBoard) {
                    if (clipBoard.hasOwnProperty(key)) {
                        PBuffer.cornerY = (PBuffer.cornerY > clipBoard[key].r) ? clipBoard[key].r : PBuffer.cornerY;
                        PBuffer.cornerX = (PBuffer.cornerX > clipBoard[key].c) ? clipBoard[key].c : PBuffer.cornerX;
                    }
                }

                Move.start_row = PBuffer.cornerY;
                Move.start_col = PBuffer.cornerX;
                // cut or paste
                notDelete = (Object.keys(PBuffer.cut).length > 0) ? false : true;

                // get copy or cut products from buffer, assign that to marked products
                markedProducts = (Object.keys(PBuffer.cut).length > 0) ? PBuffer.cut : PBuffer.copy;
                // simulate mouse move to get rEnd and cRow defined for markedProducts
                var el = $("#r" + machineMapping[PBuffer.cornerY - 1]['id'] + "c" + PBuffer.cornerX);

                triggeProductPlacement(el, PBuffer.mousemove.target, notDelete, PBuffer.mousemove.clientX, PBuffer.mousemove.clientY, markedProducts, true);

            }
        });

        // constructs the suggestion engine
        var prods = new Bloodhound({
            datumTokenizer: Bloodhound.tokenizers.whitespace,
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            remote: {
                url: 'php/products.php',

                prepare: function (query, settings) {
                    settings.dataType = 'json';
                    settings.type = 'POST';
                    settings.data = {q: query};

                    return settings;
                }
            }
        });
        $('#product').typeahead({
                hint: false,
                highlight: true,
                minLength: 1
            },
            {
                name: 'product',
                source: prods,
                limit: 7
            });

        $('#product').css("vertical-align", " middle");

        // info modal for products
        $("body").on('click', '.info', function () {
            if ($('#product').val().trim() === '') {
                showNotification("Nav ievadīts produkta nosaukums.", 'danger');
                return;
            }

            $.post("php/products.php", {product: $('#product').val()})
                .done(function (data) {
                    if (data != '[]') {
                        var jsonModal = JSON.parse(data);

                        $('#productModal .modal-title')[0].innerHTML = $('#product').val();
                        // set overll comment
                        if (jsonModal != null)
                            $('#overallComment').val(jsonModal['restricted']['info']);

                        var td_html = '';

                        for (const [ , value] of Object.entries(jsonModal['machines'])) {
                            var comment = '',
                                is_checked = 'checked';

                            if (jsonModal['restricted']['machines'][value.id]) {
                                if (!jsonModal['restricted']['machines'][value.id]['check']) {
                                    is_checked = '';
                                }

                                comment = jsonModal['restricted']['machines'][value.id]['comment'];
                            }

                            td_html += '<tr>';
                            td_html += '<td><div class="checkbox"><label><input type="checkbox" ' + is_checked + ' disabled>' + value.name + '</label></div></td>';
                            td_html += '<td><input type="text" class="form-control input-sm" value="' + comment + '"></td>';

                            td_html += '</tr>';
                        }

                        $('#productsInfoTable tbody').html(td_html);
                        $('#productModal').modal({'show': true});
                    } else {
                        showNotification("Šāds <b>'" + $('#product').val() + "'</b> produkts neeksistē.", 'danger');
                    }
                })
                .fail(function (data) {
                    showNotification("Nevar saglabāt datus.", 'danger');
                });

        });
        /* ####################### END OF EVENTS #############################
        #################################################################
    */

        save = function () {
            var addedTiles = {}, deletedTiles = {}, newLoadedTiles = {};

            var allProducts = $("#table2 tbody tr td div");
            $.each(allProducts, function () {
                if ($(this).attr('id') in loadedTiles) {
                    if (loadedTiles[$(this).attr('id')].product != $(this).attr('product') || loadedTiles[$(this).attr('id')].kg != $(this).attr('kg') || loadedTiles[$(this).attr('id')].fixed.toString() != $(this).attr('fixed').toString()) {
                        addedTiles[$(this).attr('id')] = {
                            product: $(this).attr('product'),
                            kg: $(this).attr('kg'),
                            fixed: $(this).attr('fixed')
                        };
                    }
                } else {
                    addedTiles[$(this).attr('id')] = {
                        product: $(this).attr('product'),
                        kg: $(this).attr('kg'),
                        fixed: $(this).attr('fixed')
                    };
                }
                newLoadedTiles[$(this).attr('id')] = {
                    product: $(this).attr('product'),
                    kg: $(this).attr('kg'),
                    fixed: $(this).attr('fixed')
                };
            })

            for (var key in loadedTiles) {
                if (loadedTiles.hasOwnProperty(key)) {
                    if (document.getElementsByName(key)[0].innerHTML == "") {
                        deletedTiles[key] = loadedTiles[key];
                    }
                }
            }

            $.post("php/save.php", {upsert: JSON.stringify(addedTiles), del: JSON.stringify(deletedTiles)})
                // when post is finished
                .done(function (data) {
                    showNotification("Plāns saglabāts.", 'success');
                    loadedTiles = newLoadedTiles;
                })
                .fail(function (data) {
                    showNotification('Kaut kas nogājis greizi.', 'danger');
                });


            $.post("php/texturing_save.php", {texturingTasks: JSON.stringify(texturingTasks), action: '_n'})
                .done(function (data) {
                    texturingTasks = [];
                })
                .fail(function (data) {
                    showNotification("Saražojamo palešu skaits nevar tikt saglabāts.", 'danger');
                });
        };
    });
})(jQuery);

function savePlanLayout (successMessage) {
    $.post("php/plan_layout/save.php", {
        plan: JSON.stringify(window.planLayout),
        action: '_u'
    })
        // when post is finished
        .done(function () {
            showNotification(successMessage, 'success');
            updatePlanMenu();

            $('#plansummaryModal [data-dismiss="modal"]').click();
        })
        .fail(function () {
            showNotification('Kaut kas nogājis greizi.', 'danger');
        });
}

function updatePlanMenu() {
    let planDropdown = $('.dropdown-menu.plan-menu'),
        confirmOptions = {
            title: "Dzēst?<br />(darbība ir neatgriezeniska)",
            btnOkLabel: "Jā",
            btnCancelLabel: "Nē",
            singleton: true,
            placement: "bottom"
        };

    planDropdown.find('[data-plan-id]').remove();

    for (const [key, value] of Object.entries(window.planLayout)) {
        $('' +
            '<li class="plan-' + key + '" data-plan-id="' + key + '"><a href="#">' + value["name"] +
            '<div><button type="button" class="btn btn-xs btn-default edit-plan">' +
            '<i class="glyphicon glyphicon-pencil"></i></button>' +
            '<button type="button" class="btn btn-xs btn-danger">' +
            '<i class="glyphicon glyphicon-trash"></i></button></div></a></li>').insertBefore(planDropdown.find('.plan-create'));
    }

    $(".dropdown-menu.plan-menu li[data-plan-id] div button:last-child").confirmation(confirmOptions);
}

function getMarkedProducts(e) {
    markedProducts = {};
    Move.mProdCountInRow = {};
    // get maximum and minimum values to be able to start the loop properly
    var maxR = Math.max(Move.start_row, Move.old_row), minR = Math.min(Move.start_row, Move.old_row),
        maxC = Math.max(Move.start_col, Move.old_col), minC = Math.min(Move.start_col, Move.old_col);

    var markedDivs = $("#table2 tbody tr:nth-child(n+" + minR + "):nth-child(-n+" + maxR + ") td:nth-child(n+" + minC + "):nth-child(-n+" + maxC + ") div");

    if (markedDivs.length > 0) { // checks if there is some marked product
        // get corner of marked place area. It's the min col and row combination. Paste from buffer will start there
        PBuffer.cornerY = markedDivs[0].parentNode.parentNode.rowIndex + 1;
        PBuffer.cornerX = markedDivs[0].parentNode.cellIndex + 1;
    }
    for (var col = minC; col <= maxC; col++) {
        for (var i = minR; i <= maxR; i++) {
            markedDivs = $("#r" + machineMapping[i - 1]['id'] + "c" + col + ' div');
            if (markedDivs[0] === undefined)
                continue;
            if (markedDivs.attr('fixed') != 'true' && markedDivs.attr('fixed') != '1') {
                markedDivs.addClass('marked');
                // update marked product count in row
                var row = markedDivs.parent()[0].rowIndex + 1;
                if (Move.mProdCountInRow[row] === undefined)
                    Move.mProdCountInRow[row] = [markedDivs[0].cellIndex + 1];
                else
                    Move.mProdCountInRow[row].push(markedDivs[0].cellIndex + 1);

                markedProducts[markedDivs.attr('id')] = {
                    'r': markedDivs.parent().parent()[0].rowIndex + 1,
                    'c': markedDivs.parent()[0].cellIndex + 1,
                    'rEnd': markedDivs.parent().parent()[0].rowIndex + 1,
                    'cEnd': markedDivs.parent()[0].cellIndex + 1,
                    'rId': machineMapping[markedDivs.parent().parent()[0].rowIndex]['id'],
                    'rEndId': machineMapping[markedDivs.parent().parent()[0].rowIndex]['id'],
                    'product': markedDivs.attr('product'),
                    'kg': markedDivs.attr('kg'),
                    'fixed': markedDivs.attr('fixed'),
                    'groupid': markedDivs.attr('groupid')
                };

                PBuffer.cornerY = (PBuffer.cornerY > markedProducts[markedDivs.attr('id')].r) ? markedProducts[markedDivs.attr('id')].r : PBuffer.cornerY;
                PBuffer.cornerX = (PBuffer.cornerX > markedProducts[markedDivs.attr('id')].c) ? markedProducts[markedDivs.attr('id')].c : PBuffer.cornerX;
            }
            // if the fixed tile is selected then throw an error
            else {
                $('.marked').removeClass('marked');
                markedProducts = {};
                showNotification('Nedrīkst iezīmēt fiksēto plānu.', 'danger');
                return;
            }
        }
    }
    ;
}

function shiftFromFreeDays() {
    // make a copy of marked products becouse new marked products will be modified
    markedProductsCopy = markedProducts;
    markedProducts = {};
    var rowLastCellIndex = $("#table2 tbody tr:nth-child(1) td").last()[0].cellIndex + 1;

    var allFreeDivs;// = $('.dark div');

    var fdlen;// = allFreeDivs.length;

    for (var col = rowLastCellIndex; col > 0; col--) {
        allFreeDivs = $("#table2 tbody tr:nth-child(n) td:nth-child(" + col + ").dark div");
        fdlen = allFreeDivs.length;
        for (var i = 0; i < fdlen; i++) {
            if (!$(allFreeDivs[i].parentNode).hasClass('fixed-pos') || $(allFreeDivs[i].parentNode).hasClass('redips-mark')) {
                markedProducts[allFreeDivs[i].id] = {
                    'r': allFreeDivs[i].parentNode.parentNode.rowIndex + 1,
                    'c': allFreeDivs[i].parentNode.cellIndex + 1,
                    'rEnd': allFreeDivs[i].parentNode.parentNode.rowIndex + 1,
                    'cEnd': allFreeDivs[i].parentNode.cellIndex + 1,
                    'product': allFreeDivs[i].getAttribute('product'),
                    'kg': allFreeDivs[i].getAttribute('kg'),
                    'fixed': allFreeDivs[i].getAttribute('fixed'),
                    'groupid': allFreeDivs[i].getAttribute('groupid')
                };
            }
        }
    }
    // triger mouseup event to move products from free sifts
    allFreeDivs = $('.dark div');
    Move.move_products = {'start': true, 'move': true};
    var event = jQuery.Event("mouseup", {
        which: 1,
        buttons: 1,
        clientX: 500,
        clientY: 300,
        initShifFreeDays: true
    });
    allFreeDivs.trigger(event);
    $('.marked').removeClass('marked');
    Move.move_products = {'start': false, 'move': false};
    // get back markedProducts value
    markedProducts = markedProductsCopy;
}

// add leading zero function
function pad(n) {
    return n < 10 ? '0' + n : n;
}

function getShiftNumber(h, nextShift) {
    var shift;
    if ((h >= 22 && h <= 23) || h <= 5) shift = 0;
    else if (h >= 6 && h <= 13) shift = 1;
    else if (h >= 14 && h <= 21) shift = 2;

    return (nextShift) ? (shift + 1) % 3 : shift;
}

function formatDate(d) {
    return d.getFullYear() + '-' + pad((d.getMonth() + 1)) + '-' + pad(d.getDate());
}

function drawTodaysSign(draw_history) {
    var today = new Date();
    var whichShift = 0;
    var h = today.getHours();
    // fix a bug when, for example today is 2015/11/20  22:52, but 2015/11/21 first shift starts on 2015/11/20 22:00
    // so today in this time interval is already next day
    if (h >= 22 && h <= 23) today.setDate(today.getDate() + 1);

    whichShift = getShiftNumber(h, false)

    // mark today in the table
    var today_formated = today.getFullYear() + '-' + pad((today.getMonth() + 1)) + '-' + pad(today.getDate());
    if ($('#' + today_formated + 'H')[0] === undefined) {
        $('#today-line').remove();
    } else {
        if ($('#today-line')[0] === undefined) {
            $('#right').prepend('<div id="today-line"></div>');
        }

        var date_cellIndex = $('#' + today_formated + 'H')[0].cellIndex;
        var time_index = (date_cellIndex * 3 - 1) + whichShift;
        // iterate through currrent shift column in planning table
        for (var i = 0; i < machineCount; i++) {
            $('#r' + machineMapping[i]['id'] + 'c' + (time_index - 1)).addClass('today');
        }

        var lineHeight = parseInt($('.left-header').css('height')) + 60;

        $('#today-line').css('height', lineHeight.toString() + 'px');
        $('#today-line').offset({top: $('#today-line').offset().top, left: $('.today').offset().left});
    }

    if (draw_history) drawHistoryDiv(today, whichShift);
}

function drawHistoryDiv(today, whichShift) {
    var disableButtons = false;
    if ($(".today")[0] !== undefined) {
        var todaysCol = $(".today")[0].cellIndex + 1;
        if (todaysCol > 7) {
            var todaysPos = $(".today").position().left - $("#table2").position().left - (70 * 3 * 7) + whichShift * 70 + 2;
            $('#history-mark').css('height', $('#table2').css('height'));
            $('#history-mark').css('width', todaysPos);
            historyEndCell = $(".today")[0].cellIndex - $("#table2 tr:nth-child(1) td")[0].cellIndex - 3 * 7 + 1 + whichShift;
            disableButtons = true;
        } else {
            $('#history-mark').css('height', 0);
            $('#history-mark').css('width', 0);
            historyEndCell = 1;
        }
    } else {
        // there is not today in the plan
        var oneDay = 24 * 60 * 60 * 1000; // hours*minutes*seconds*milliseconds
        var lastTDname = $('#table2 tr:nth-child(1) td').last().attr('name').split('/')[0].replace(/-/g, '/');
        var lastDate = new Date(lastTDname);
        var daysBetween = Math.round((today.getTime() - lastDate.getTime()) / oneDay);

        if (daysBetween > 0) {
            $('#history-mark').css('height', $('#table2').css('height'));
            var back = 7 - daysBetween;
            var endOffset = (70 * 3 * back) + whichShift * 70;
            $('#history-mark').css('width', $('#table2 tr:nth-child(1) td').last().position().left - endOffset);
            historyEndCell = $('#table2 tr:nth-child(1) td').last()[0].cellIndex - 3 * (back + 1) + 1 + whichShift;
            disableButtons = true;
        } else {
            $('#history-mark').css('height', 0);
            $('#history-mark').css('width', 0);
            historyEndCell = 1;
        }
    }
    if (disableButtons) {
        if (historyEndCell >= 2) {
            var rangeBttns = $('.header-time td:nth-child(n+2):nth-child(-n+' + (historyEndCell - 1) + ') button');
            $.each(rangeBttns, function () {
                var parent = this.parentNode.parentNode;
                $(parent).addClass('history');
                parent.style.backgroundColor = "#EEE";
                // get a shift text and place it in td
                parent.innerHTML = this.innerHTML.split('i>')[1];
            })
        }
    }
}

function updateUndo() {
    var h = $('.table1-header').html();
    var t = $('#table2').html();
    var lefth = $('.left-header').html();

    if (undoProducts.length > 0) {
        // if last saved undo is not equal with this table, then update
        if (undoProducts[undoProducts.length - 1]['table'] != t) {
            if (undoProducts.length > 29) {
                undoProducts.shift();
            }
            undoProducts.push({
                'header': h,
                'table': t,
                'lefth': lefth,
                'marked': markedProducts,
                'groups': groupMachines,
                'texturingTask': texturingTasks.slice()
            });
        }
    } else {
        if (undoProducts.length > 29) {
            undoProducts.shift();
        }
        // JSON is needed to clone the loadedTitles value, becouse otherwise it's passed by ref and this line loses a meaning
        undoProducts.push({
            'header': h,
            'table': t,
            'lefth': lefth,
            'marked': markedProducts,
            'groups': groupMachines,
            'texturingTask': texturingTasks.slice()
        });
    }
    //$('#undo-text').html('Atcelt ('+undoProducts.length+')');
}

function showNotification(text, type) {
    var title = '';
    if (type == 'danger') title = 'Kļūda! ';
    else if (type == 'success') title = 'Paziņojums! ';

    $.toaster({priority: type, title: title, message: text});
}

/* ###### COLOR FUNCTIONS #######
###################################
*/

function hexToRgb(hex) { // converts color from hex to rgb
    // Expand shorthand form (e.g. "03F") to full form (e.g. "0033FF")
    var shorthandRegex = /^#?([a-f\d])([a-f\d])([a-f\d])$/i;
    hex = hex.replace(shorthandRegex, function (m, r, g, b) {
        return r + r + g + g + b + b;
    });

    var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
    return result ? {
        r: parseInt(result[1], 16),
        g: parseInt(result[2], 16),
        b: parseInt(result[3], 16)
    } : null;
}

function generateTextColor(color) { // generates
    var color = hexToRgb(color);
    if (color != null) { // 'ok' is true when the parsing was a success
        var brightness = Math.sqrt(
            color.r * color.r * .299 +
            color.g * color.g * .587 +
            color.b * color.b * .114);
        var foreColor = (brightness < 130) ? "#FFFFFF" : "#000000";
        return foreColor;
    }
}

function updateColor(product) { // get random color
    var letters = '0123456789ABCDEF'.split('');
    var random_color = '#';
    for (var i = 0; i < 6; i++) {
        random_color += letters[Math.round(Math.random() * 15)];
    }
    if (productsColor[product] === undefined) {
        productsColor[product] = random_color;
    }
}

/* ### END OF COLOR FUNCTIONS ###
    ###########################
*/
function setProductDiv(name, product, groupId, marked, kg, is_fixed) {
    cls = (marked) ? ' marked' : '';
    is_static = (is_fixed == 'true' || is_fixed == '1') ? 'true' : 'false';
    return '<div id="' + name + '" class="blue' + cls + '" product="' + product + '" kg="' + kg + '" fixed="' + is_static + '"' +
        ' groupId="' + groupId + '" style="background-color:' + productsColor[product] + '; color:' + generateTextColor(productsColor[product]) + '">' + product + '</div';
}

function getURLParameter(name) {
    return decodeURIComponent((new RegExp('[?|&]' + name + '=' + '([^&;]+?)(&|#|;|$)').exec(location.search) || [, ""])[1].replace(/\+/g, '%20')) || null;
}

function updateMachineGroup() {
    var rowLastCellIndex = $("#table2 tbody tr:nth-child(1) td").last()[0].cellIndex + 1;
    var columns, cell, row, productGroups = [], currentColumn = {}, prevColumn = {};
    groupMachines = {};
    group = 0;

    var start = new Date().getTime();

    for (var i = 1; i <= rowLastCellIndex; i++) {
        cell = $(getFirstRowSelector() + i);
        if (!cell.hasClass('dark')) {
            prevColumn = currentColumn;
            currentColumn = {};
            columns = $('td[id$=c' + i + '] div');
            $.each(columns, function () {
                if (!($(this).attr('product') in prevColumn) && !($(this).attr('product') in currentColumn)) {
                    group++;
                    productGroups[$(this).attr('product')] = group;
                }
                currentColumn[$(this).attr('product')] = true;
                $(this).attr('groupid', productGroups[$(this).attr('product')]);

                rowIndex = this.parentElement.parentElement.rowIndex + 1;
                if (groupMachines[$(this).attr('groupid')] !== undefined) {
                    if ($.inArray(rowIndex, groupMachines[$(this).attr('groupid')]) == -1) {
                        groupMachines[$(this).attr('groupid')].push(rowIndex);
                    }
                } else {
                    groupMachines[$(this).attr('groupid')] = [];
                    groupMachines[$(this).attr('groupid')].push(rowIndex);
                }
            })
        }
    }
    d = new Date();
    endTime = d.getTime() - start;
    console.log('[Test] Loop with double array. CSS selector each time : ' + endTime + 's');
}

function getFirstRowSelector() {
    return '#r' + $('.left-header tbody tr:first-child button').data('id') + 'c';
}

// checks if Ctrl button is pressed
var ctrlPressed = false;
$(window).keydown(function (evt) {
    if (evt.which == 17) { // ctrl
        ctrlPressed = true;
    }
    if (ctrlPressed && evt.which == 90) {
        var event = jQuery.Event("click", {
            which: 1,
            buttons: 1
        });
        $('#undo-gen-prod-bttn').trigger(event);
    }
}).keyup(function (evt) {
    if (evt.which == 17) { // ctrl
        ctrlPressed = false;
    }

});