/* For rights see LICENSE.TXT */

// scope the jQuery
(function ($) {

    $(document).ready(function () {

        var openedTaskId;
        var openedProduct;
        var confirmOptions = {
            title: "Dzēst uzdevumu?<br />(darbība ir neatgriezeniska) ",
            btnOkLabel: "Jā",
            btnCancelLabel: "Nē",
            singleton: true
        };
        $(".delete").confirmation(confirmOptions);

        $('.popupDatepicker').datepick({dateFormat: 'dd/mm/yyyy'});

        // shows delete icon in the end of the row
        $('#productsTable').on('click', 'tbody tr', function (e) {
            if ($(this).hasClass('errorRow')) {
                $('#productsTable tr').removeClass('selectedRow');
            } else {
                $('#productsTable tr').removeClass('selectedRow, errorRow');
                $(this).addClass('selectedRow');
            }

            $('#productsTable tr').removeClass('selectedRow');
            $(this).addClass('selectedRow');
        });

        $('#productsTable').on('focus', '[contenteditable]', function () {

        }).on('blur keyup paste input', '[contenteditable]', function () {
            var t = $(this).parent();
            var kg = t.find('td:nth-child(3)').html();
            var palletWeight = t.find('td:nth-child(4)').html();

            var palletC = Math.ceil(kg / palletWeight);
            t.find('td:nth-child(5)').html(palletC);
        });


        $("#productsTable").on('click', '.info', function () {
            //tinyMCE.activeEditor.setContent('');
            openedTaskId = $(this).attr('name');
            openedProduct = $(this).attr('product');

            $.post("php/texturing_load.php", {id: $(this).attr('name')})
                // when post is finished
                .done(function (data) {
                    var $modal = $('#texturingModal .modal-body');
                    $modal.find('.texturing-container').html(data).addClass('active');

                    if (!$modal.hasClass('initial')) {
                        $modal.addClass('active-texturing').removeClass('active-sticker');

                        tinymce.init({
                            selector: 'div#texturingCommentEditor',
                            plugins: "textcolor colorpicker",
                            toolbar: "forecolor backcolor styleselect  undo redo  removeformat  bold italic underline  bullist numlist outdent indent  fontsizeselect "
                        });

                        try {
                            improveTexturingTable();
                        } catch (err) {
                            console.log(err);
                        }
                    }


                    if ($modal.hasClass('initial')) {
                        $('#print-sticekrs-bttn').trigger('click');
                    }

                    $('#texturingModal').modal('show');
                })
                .fail(function (data) {
                    showNotification("Nevar saglabāt datus.", 'danger');
                });
        });

        $("#texturingModal").on('click', '#print-sticekrs-bttn', function () {
            $.post("php/sticker_load.php",
                {
                    name: $('#texturingModal .texturing-container .p-name').text()
                }).done(function (data) {
                try {
                    var json = $.parseJSON(data);
                    if (json.length === 2) {
                        if (json[0] === null || json[1] === null) {
                            showNotification("Produkta krāsas nav uzstādīta.", 'danger');
                            showStickerView();
                        } else {
                            showStickerView(json);
                        }
                    } else {
                        showNotification("Nevar ielādēt produkta krāsas.", 'danger');
                        showStickerView();
                    }
                } catch (err) {
                    showNotification("Nevar ielādēt produkta krāsas.", 'danger');
                    showStickerView();
                }
            })
                .fail(function (data) {
                    showNotification("Kļūda datu ielādē.", 'danger');
                    showStickerView();
                });
        });

        $("#texturingModal").on('click', '#sticker-back', function () {
            $('#texturingModal .modal-body').addClass('active-texturing').removeClass('active-sticker');
        });


        $("#productsTable").on('click', '.save', function () {
            var row = $(this).parent().parent();
            var rowCells = $(this).parent().parent().find('td');
            var error = false;
            $.each(rowCells, function () {
                var td = $(this);
                if (td.hasClass('td1') || td.hasClass('td2')) {
                    if (!isNumber(td.text())) {
                        showNotification("Neatļauta vērtība '" + td.text() + "'", 'danger', row);
                        error = true;
                        return;
                    } else {
                        if (parseFloat(td.text()) < 1) {
                            showNotification("Neatļauta vērtība '" + td.text() + "'", 'danger', row);
                            error = true;
                            return;
                        }
                    }
                }
            });

            if (error) return;

            var pid = ($(this).attr('name') !== undefined) ? $(this).attr('name') : "";

            $.post("php/texturing_save.php", {
                kg: row.find('.td1').text(),
                pallet_weight: row.find('.td2').text(), pid: pid, action: '_u_table'
            })
                // when post is finished
                .done(function (data) {
                    try {
                        var json = $.parseJSON(data);
                        if (json['error'] == 0) {
                            showNotification("Izmaiņas saglabātas.", 'success');
                        } else {
                            showNotification("Nevar saglabāt datus.", 'danger');
                        }
                    } catch (err) {
                        showNotification("Nevar saglabāt datus.", 'danger');
                    }
                })
                .fail(function (data) {
                    showNotification("Nevar saglabāt datus.", 'danger');
                });

        });

        $('#load-interval-bttn').click(function () {
            var startDate = $('[name="start"]').val(),
                endDate = $('[name="end"]').val(),
                product = $('[name="texturing-product"]').val();
            // format to: mm/dd/yyyy input: dd/mm/yyyy
            var startDateFormatted = startDate.split('/')[1] + '/' + startDate.split('/')[0] + '/' + startDate.split('/')[2],
                endDateFormatted = endDate.split('/')[1] + '/' + endDate.split('/')[0] + '/' + endDate.split('/')[2];

            var sd = new Date(startDateFormatted),
                ed = new Date(endDateFormatted);

            // if start date is less or equal to end date the draw table
            if ((startDate === '' && endDate !== '') || (startDate !== '' && endDate === '') || sd > ed) {
                showNotification('Nav korekti ievadīts datums.', 'danger');
            } else {
                // get postgre time format
                startDateFormatted = startDateFormatted.split('/')[2] + '-'
                    + startDateFormatted.split('/')[0] + '-'
                    + startDateFormatted.split('/')[1];
                endDateFormatted = endDateFormatted.split('/')[2] + '-'
                    + endDateFormatted.split('/')[0] + '-'
                    + endDateFormatted.split('/')[1];

                $.post(
                    "php/texturing_load.php", {
                        startDate: startDate === '' ? startDate : startDateFormatted,
                        endDate: endDate === '' ? endDate : endDateFormatted,
                        product: product
                    })
                    .done(function (data) {
                        $('#productsTable tbody').html(data);
                        $(".delete").confirmation(confirmOptions);
                    })
                    .fail(function (data) {
                        showNotification("Nevar ielādēt ražošanas uzdevumus.", 'danger');
                    });
            }
        });


        function improveTexturingTable() {
            var rowCount = $('#texturingTable tr').length - 1;
            var colCount = $('#texturingTable th').last()[0].cellIndex + 1;
            var palletCount = parseInt($('.pallet-count').text());
            var defaultRowStart = '<tr><td class="nr">';
            var defaultRowEnd = '</td><td class="shift"></td>';

            for (var i = 0; i < colCount - 3; i++) {
                defaultRowEnd += '<td class="new-td" contenteditable="true"></td>';
            }
            defaultRowEnd += '<td class="mtd"></td></tr>';

            if (palletCount > rowCount) {
                for (var i = rowCount; i < palletCount; i++) {
                    $('#texturingTable tbody').append(defaultRowStart + (i + 1) + '.' + defaultRowEnd);
                }
            } else if (palletCount < rowCount) {
                for (var i = rowCount; i > palletCount; i--) {
                    $('#texturingTable tr').last().remove();
                }
            }
        }

        function showStickerView(colors) {
            var $modal = $('#texturingModal .modal-body').removeClass('active-texturing').addClass('active-sticker'),
                $stickerContainer = $modal.find('.sticker-container');

            if (colors) {
                $stickerContainer.find('.sticker-mid-up').css('background-color', colors[0]);
                $stickerContainer.find('.sticker-mid-bottom').css('background-color', colors[1]);
            }

            $stickerContainer.find('.sticker-name').html($('#texturingModal h2 .p-name').text());
            $stickerContainer.find('.sticker-description').html($('#texturingModal h2 .p-description').text());

            var stickerHtml = $stickerContainer.find('.sticker').eq(0)[0].outerHTML.repeat(9);
            $stickerContainer.html(stickerHtml);

            $('#stickerModal').modal('show');
        }


        $("#bttn-save-modal").click(function () {
            $.post("php/texturing_save.php", {
                id: openedTaskId,
                comment: tinyMCE.activeEditor.getContent(),
                revision: $('#texturingModal .revision-info').text(),
                palletText: $('#texturingModal .pallet-text').text(),
                table: $('#texturingTable').html(),
                action: '_u'
            })

                .done(function (data) {
                    showNotification("Ražošanas uzdevums saglabāts.", 'success');
                })
                .fail(function (data) {
                    showNotification("Nevar saglabāt datus.", 'danger');
                });
        });

        $('#texturingModal').on('click', '#save-signature-bttn', function () {
            $.post("php/products_save.php", {
                product: openedProduct,
                comment: tinyMCE.activeEditor.getContent({format: 'text'}),
                action: '_signature'
            })

                .done(function (data) {
                    showNotification("Produkta teksts saglabāts.", 'success');
                })
                .fail(function (data) {
                    showNotification("Nevar saglabāt tekstu.", 'danger');
                });
        });

        $('#texturingModal').on('click', '#load-signature-bttn', function () {
            $.post("php/products_load.php", {
                product: openedProduct, action: '_signature_l'
            })

                .done(function (data) {
                    tinyMCE.activeEditor.setContent(data);
                    showNotification("Produkta teksts ielādēts.", 'success');
                })
                .fail(function (data) {
                    showNotification("Nevar ielādēt tekstu.", 'danger');
                });
        });


        $('#productsTable').on('click', '.delete', function () {
            var clickedBtn = $(this);
            $.post("php/texturing_save.php", {id: $(this).attr('name'), action: '_d'})
                .done(function (data) {
                    clickedBtn.parent().parent().remove();
                    showNotification("Ražošanas uzdevums idzēsts.", 'success');
                })
                .fail(function (data) {
                    showNotification("Problēmas dzēšanā.", 'danger');
                });
        });

        $("#texturingModal").on('click', '#add-col-bttn', function () {
            var colCount = $('#texturingTable td').last()[0].cellIndex + 1;
            if (colCount < 5) {
                var a = parseInt($('#texturingTable .shift').css('width'));
                var b = a - 100;
                $('#texturingTable .shift').css('width', b + 'px');
                $('#texturingTable .shift').after('<td class="new-td" contenteditable="true"></td>');
                $('#texturingTable .shift-th').after('<th class="new-td">' +
                    '<span><button type="button" class="btn btn-xs btn-danger" id="remove-col-bttn"><span class="glyphicon glyphicon-remove"></span></button></span>' +
                    $('#add-col-input').val() + '</th>');
            }
        });

        $("#texturingModal").on('click', '#remove-col-bttn', function () {
            var cellIndex = $(this).parent().parent()[0].cellIndex + 1;

            $('#texturingTable td:nth-child(' + cellIndex + '), #texturingTable th:nth-child(' + cellIndex + ')').each(function () {
                $(this)[0].outerHTML = '';
            });

            var a = parseInt($('#texturingTable .shift').css('width'));
            var b = a + 100;
            $('#texturingTable .shift').css('width', b + 'px');
        });

        function addStickerPositionCorrection() {
            $('.sticker-container-position input').each(function () {
                var correctionValue = (this.value || 0) + 'px',
                    sticker = $($(this).attr('data-target')),
                    leftPart = sticker.find('.sticker-left'),
                    midPart = sticker.find('.sticker-mid'),
                    rightPart = sticker.find('.sticker-right');

                if (this.classList.contains('col3')) {
                    leftPart.css('left', 'calc(' + correctionValue + ' + ' + leftPart.attr('data-position-last') + ')');
                    midPart.css('left', 'calc(' + correctionValue + ' + ' + midPart.attr('data-position-last') + ')');
                    rightPart.css('left', 'calc(' + correctionValue + ' + ' + rightPart.attr('data-position-last') + ')');
                } else {
                    leftPart.css('left', 'calc(' + correctionValue + ' + ' + leftPart.attr('data-position') + ')');
                    midPart.css('left', 'calc(' + correctionValue + ' + ' + midPart.attr('data-position') + ')');
                    rightPart.css('left', 'calc(' + correctionValue + ' + ' + rightPart.attr('data-position') + ')');
                }
            });
        }

        $('#print-bttn').click(function () {
            if ($('.modal-body.active-sticker').length) {
                var stickerCount = $('#sticker-count').val();

                if (!isNaN(stickerCount) && stickerCount !== '' && stickerCount > '0') {
                    stickerCount = Math.ceil(parseInt(stickerCount) / 21) * 21;

                    var $stickerContainer = $('#texturingModal .modal-body .sticker-container'),
                        stickerHtml = $stickerContainer.find('.sticker:first-child')[0].outerHTML.repeat(stickerCount);

                    $stickerContainer.html(stickerHtml);
                    addStickerPositionCorrection();
                } else {
                    showNotification('Norādīts nepareizs uzlīmju skaits.', 'danger');
                    return false;
                }

                $('<span class="sticker-page-break"></span>').insertAfter($stickerContainer.find('.sticker:nth-child(21n)'));
            } else {
                $('#texturingComment').html(tinyMCE.activeEditor.getContent());
                if ($('.printing-table')[0] === undefined) {
                    $('#texturingTable').after('<div class="printing-table">' + prepareToPrint() + '</div>');
                } else {
                    $('.printing-table').html(prepareToPrint());
                }
            }

            window.print();
        });

        var clonedHeaderRow;
        // for static header on top. clone
        $("#productsTable").each(function () {
            clonedHeaderRow = $(".persist-header", this);
            clonedHeaderRow
                .before(clonedHeaderRow.clone())
                .css("width", clonedHeaderRow.width())
                .addClass("floatingHeader");
        });

        $(window)
            .scroll(UpdateTableHeaders)
            .trigger("scroll");
    })

})(jQuery);

function prepareToPrint() {
    var firstThis, widthCorrection = 0, adjust = false, tableHTML = "";
    $('#texturingTable tbody tr, #texturingTable thead tr').each(function () {
        firstThis = $(this);
        tableHTML += '<div class="divRow">';
        $(firstThis.find('td, th')).each(function () {
            widthCorrection += $(this).width();
        })
        $(firstThis.find('td, th')).each(function () {
            if ($(this)[0].cellIndex + 1 == 2 && widthCorrection > 600) {
                widthCorrection = $(this).width() - (widthCorrection - 600);
                adjust = true;
            } else {
                adjust = false;
            }
            tableHTML += '<div class="divCell ' + this.className + '" style="width:' + (adjust ? widthCorrection : $(this).width()) + 'px;' +
                'height:' + $(this).css('height') + '">' + $(this).text() + '</div>';
        });
        tableHTML += "</div>";
        widthCorrection = 0
    });
    return '<div class="divTable">' + tableHTML + '</div>';
}

function isNumber(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
}

function UpdateTableHeaders() {
    $("#productsTable").each(function () {

        var el = $(this),
            offset = el.offset(),
            scrollTop = $(window).scrollTop(),
            floatingHeader = $(".floatingHeader", this)

        if ((scrollTop > offset.top - 36) && (scrollTop < offset.top - 36 + el.height())) {
            // get satic thead the same width as original table thead
            for (var i = 1; i <= 8; i++) {
                $('.floatingHeader th:nth-child(' + i + ')').width($('.persist-header th:nth-child(' + i + ')').width());
            }

            $('.floatingHeader').width($('.persist-header').width());
            floatingHeader.css('height', '40px');

            floatingHeader.css({
                "visibility": "visible"
            });
        } else {
            floatingHeader.css({
                "visibility": "hidden"
            });
        }
    });
}

function showNotification(text, type) {
    var title = '';
    if (type === 'danger') title = 'Kļūda! ';
    else if (type === 'success') title = 'Paziņojums! ';

    $.toaster({priority: type, title: title, message: text});
}