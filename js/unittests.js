/* For rights see LICENSE.TXT */
function testTableSpeed() {
    // -----
    var d = new Date();
    var start = d.getTime();
    var node, columns, endTime, colLen, rowLen;

    try {
        rowLen = $('#table2 tr:first td').last()[0].cellIndex + 1;
        colLen = $('#table2 tr:last td').parent()[0].rowIndex + 1;
    } catch (err) {
    }

    /*
    for (var i = 1; i < rowLen; i++) {
        for (var j = 1; j < colLen; j++) {
            node = $('#table2 tbody tr:nth-child('+j+') td:nth-child('+i+')');
        };
    };
    d = new Date();
    endTime = d.getTime() - start;
    console.log('[Test] Loop with double array. CSS nth-child selector each time : '+endTime+'s ('+rowLen+', '+colLen+')') ;
    return endTime;

    // --
    d = new Date();
    start = d.getTime();
    for (var i = 1; i < 72; i++) {
        var a = $('#table2 tr td div[groupid="10"]');
    }
    d = new Date();
    endTime = d.getTime() - start;
    console.log('[Test] CSS selector with attribute "groupid" and long path : '+endTime+'s');

    // --
    d = new Date();
    start = d.getTime();
    for (var i = 1; i < 72; i++) {
        var a = $('div[groupid="10"]');
    }
    d = new Date();
    endTime = d.getTime() - start;
    console.log('[Test] CSS selector with attribute "groupid" and short path : '+endTime+'s');
    */

    /*
    // -----
    d = new Date();
    start = d.getTime();
    for (var i = 1; i < rowLen; i++) {
        columns = $("[id$=c"+i+"]");
        $.each( columns, function() {
            node = this;
            //$(this).html($(this).attr('groupid'));
        })
    };
    d = new Date();
    endTime = d.getTime() - start;
    console.log('[Test] Loop through columns obtained with CSS selector by ID: '+endTime+' ms ('+rowLen+', '+colLen+')');
    return endTime;

    /*
    // -----
    d = new Date();
    start = d.getTime();
    for (var i = 1; i < 72; i++) {
        columns = $('#table2 tbody tr:nth-child(n+1):nth-child(-n+26) td:nth-child('+i+') div');
        $.each( columns, function() {
            node = this;
            $(this).html($(this).attr('groupid'));
        })
    };
    d = new Date();
    endTime = d.getTime() - start;
    console.log('[Test] Loop through columns obtained with CSS selector : '+endTime+'s');
    */
    /*
    // -----
    d = new Date();
    start = d.getTime();
    for (var i = 1; i < rowLen; i++) {
        columns = $('#table2 tr:nth-child(n) td:nth-child('+i+')');
        $.each( columns, function() {
            node = this;
            //$(this).html($(this).attr('groupid'));
        })
    };
    d = new Date();
    endTime = d.getTime() - start;
    console.log('[Test] Loop through columns obtained with CSS selector with tr:nth-child(n): '+endTime+'ms ('+rowLen+', '+colLen+')');
    return endTime;
    */


    // -----
    d = new Date();
    start = d.getTime();
    for (var i = 1; i < rowLen; i++) {
        for (var j = 1; j < colLen; j++) {
            node = $('#r' + j + 'c' + i);
            //node.html(node.attr('groupid'));
        }
        ;
    }
    ;
    d = new Date();
    endTime = d.getTime() - start;
    console.log('[Test] Loop with double array. CSS id selector each time : ' + endTime + ' ms (' + rowLen + ', ' + colLen + ')');
    return endTime;


    /*
    d = new Date();
    start = d.getTime();
    for (var i = 1; i < rowLen; i++) {
        for (var j = 1; j < colLen; j++) {
            node = $('.r'+j+'c'+i);
            //node.html(node.attr('groupid'));
        };
    };
    d = new Date();
    endTime = d.getTime() - start;
    console.log('[Test] Loop with double array. CSS class selector each time : '+endTime+' ms ('+rowLen+', '+colLen+')');
    return endTime;
    */


    // -----
    /*
    var strHtml = '';
    d = new Date();
    start = d.getTime();
    for (var i = 1; i < 36; i++) {
        strHtml = '<tr>';
        for (var j = 1; j < 100; j++) {
            strHtml += '<td></td>';
        };
        $('#table2 tbody').append(strHtml + '</tr>');
    };
    //$('#table2 tbody').html(strHtml);
    d = new Date();
    endTime = d.getTime() - start;
    console.log('[Test] Loop with double array. CSS class selector each time : '+endTime+' ms');
    return endTime;
    */
}

function runTest(number) {
    var totalTime = 0;
    for (var i = 0; i < number; i++) {
        totalTime += testTableSpeed();
    }
    console.log('Final test result for ' + number + 'x test runs is ' + totalTime / number + 'ms');
}