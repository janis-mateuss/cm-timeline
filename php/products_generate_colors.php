<?php
/* For rights see LICENSE.TXT */

if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

function randColor()
{
    return '#' . str_pad(dechex(mt_rand(0, 0xFFFFFF)), 6, '0', STR_PAD_LEFT);
}

function generateColors($colorCount, $usedColors)
{
    $colorArray = [];
    for ($i = 0; $colorCount > $i; $i++) {
        $generatedColor = randColor();

        if ($colorArray[$generatedColor] || $usedColors[$generatedColor]) {
            $i--;
            continue;
        }

        $colorArray[$generatedColor] = 1;
    }

    return array_keys($colorArray);
}

function getUsedColors($pgc)
{
    $stickerCheck1 = "SELECT sticker_color1 FROM products WHERE sticker_color1 IS NOT NULL";
    $stickerCheck2 = "SELECT sticker_color2 FROM products WHERE sticker_color2 IS NOT NULL";

    $usedColors = [];

    $pdo = $pgc->prepare($stickerCheck1);
    $pdo->execute();
    $res = $pdo->fetchAll(PDO::FETCH_NUM);

    foreach ($res as $key => $value) {
        $usedColors[strtolower($value[0])] = 1;
    }

    $pdo = $pgc->prepare($stickerCheck2);
    $pdo->execute();
    $res = $pdo->fetchAll(PDO::FETCH_NUM);

    foreach ($res as $key => $value) {
        $usedColors[strtolower($value[0])] = 1;
    }

    return $usedColors;
}

if (isset($_POST['action'])) {
    $action = $_POST['action'];

    if ($_SESSION['login_user'] === 'admin' && $action === '_gen') {
        require('../h/postgres_cmp.php');

        $stickerCheck1 = "SELECT pid FROM products WHERE sticker_color1 IS NULL";
        $stickerCheck2 = "SELECT pid FROM products WHERE sticker_color2 IS NULL";
        $updateSticker1 = "UPDATE products SET sticker_color1 = :sticker_color1 WHERE pid = :pid";
        $updateSticker2 = "UPDATE products SET sticker_color2 = :sticker_color2 WHERE pid = :pid";

        $colorCount = 0;
        $colorArray = [];
        $sticker1Ids = [];
        $sticker2Ids = [];

        try {
            $pdo = $pgc->prepare($stickerCheck1);
            $pdo->execute();
            $res = $pdo->fetchAll(PDO::FETCH_NUM);

            foreach ($res as $key => $value) {
                $sticker1Ids[] = $value[0];
            }

            $colorCount = $pdo->rowCount();


            $pdo = $pgc->prepare($stickerCheck2);
            $pdo->execute();
            $res = $pdo->fetchAll(PDO::FETCH_NUM);

            foreach ($res as $key => $value) {
                $sticker2Ids[] = $value[0];
            }

            $colorCount += $pdo->rowCount();

            $colorArray = generateColors($colorCount, getUsedColors($pgc));

            foreach ($sticker1Ids as $key => $value) {
                if (count($colorArray) > 0) {
                    $pdo = $pgc->prepare($updateSticker1);
                    $pdo->bindValue(':sticker_color1', array_pop($colorArray));
                    $pdo->bindValue(':pid', $value, PDO::PARAM_INT);
                    $pdo->execute();
                }
            }

            foreach ($sticker2Ids as $key => $value) {
                if (count($colorArray) > 0) {
                    $pdo = $pgc->prepare($updateSticker2);
                    $pdo->bindValue(':sticker_color2', array_pop($colorArray));
                    $pdo->bindValue(':pid', $value, PDO::PARAM_INT);
                    $pdo->execute();
                }
            }
        } catch (PDOException $e) {
            $pgc = NULL;
            die('error in gc function => ' . $e->getMessage());
        }

        $pdo = NULL;
        $pgc = NULL;
    }
}
