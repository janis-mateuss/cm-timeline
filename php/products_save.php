<?php
/* For rights see LICENSE.TXT */

session_start();

if (isset($_POST['action'])) {
    $action = $_POST['action'];

    if (isset($_POST['table']) && $_SESSION['login_user'] == 'admin' && $action == '_u') {
        require('../h/postgres_cmp.php');

        $table = json_decode($_POST['table'], true);

        $stickerColor1 = $table['stickerColor1'] !== '-' ? ', sticker_color1 = :stickerColor1' : '';
        $stickerColor2 = $table['stickerColor2'] !== '-' ? ', sticker_color2 = :stickerColor2' : '';

        $updateQ = "UPDATE products SET p_name = UPPER(:p_name), weigth = :weigth, m_speed = :m_speed, 
				efficiency_percentage = :efficiency_percentage, record_time = :record_time, 
				pallet_weight = :pallet_weight, description = :description " . $stickerColor1 . $stickerColor2 . " 
				WHERE pid = :pid";
        $selectQ = "SELECT pid FROM products WHERE p_name = UPPER(:p_name) AND pid != :pid";
        try {
            // check needed in order to prevent renaming product to a name which is already in use for different product
            $pdo = $pgc->prepare($selectQ);
            $pdo->bindValue(':p_name', $table['name']);
            $pdo->bindValue(':pid', $table['pid']);
            $pdo->execute();

            if ($pdo->rowCount() < 1) {
                $pdo = $pgc->prepare($updateQ);
                $pdo->bindValue(':pid', $table['pid'], PDO::PARAM_INT);
                $pdo->bindValue(':p_name', $table['name']);
                $pdo->bindValue(':weigth', $table['weight'], PDO::PARAM_INT);
                $pdo->bindValue(':m_speed', $table['m_min'], PDO::PARAM_INT);
                $pdo->bindValue(':efficiency_percentage', $table['eff'], PDO::PARAM_INT);
                $pdo->bindValue(':record_time', date("Y-m-d H:i:s", strtotime(date('Y-m-d H:i:s'))));
                $pdo->bindValue(':pallet_weight', $table['pallet'], PDO::PARAM_INT);
                $pdo->bindValue(':description', $table['descript']);
                $pdo->bindValue(':stickerColor1', $table['stickerColor1']);
                $pdo->bindValue(':stickerColor2', $table['stickerColor2']);

                $pdo->execute();
                echo json_encode(array('error' => 0, 'msg' => $table['pid']));
            } else {
                echo json_encode(array('error' => 1, 'msg' => 'Produkts "' . $table['name'] . '" jau eksistē.'));
            }
        } catch (PDOException $e) {
            $pgc = NULL;
            die('error in gc function => ' . $e->getMessage());
        }

        $pdo = NULL;
        $pgc = NULL;
    } else if (isset($_POST['modal']) && isset($_POST['pid']) &&
        $_SESSION['login_user'] == 'admin' && $action == '_u_modal') {
        require('../h/postgres_cmp.php');

        $table = json_decode($_POST['modal'], true);
        $allowed_machines = json_encode($table);

        $updateQ = "UPDATE products SET allowed_machines = :allowed_machines WHERE pid = :pid";
        try {
            $pdo = $pgc->prepare($updateQ);
            $pdo->bindValue(':pid', $_POST['pid'], PDO::PARAM_INT);
            $pdo->bindValue(':allowed_machines', $allowed_machines);
            $pdo->execute();

            echo json_encode(array('error' => 0));
        } catch (PDOException $e) {
            $pgc = NULL;
            die('error in gc function => ' . $e->getMessage());
        }

        $pdo = NULL;
        $pgc = NULL;
    } else if (isset($_POST['table']) && $_SESSION['login_user'] == 'admin' && $action == '_i') {
        require('../h/postgres_cmp.php');

        $table = json_decode($_POST['table'], true);

        $insertQ = "INSERT INTO products (p_name, weigth, m_speed, efficiency_percentage, record_time, pallet_weight, description, allowed_machines)
				VALUES (UPPER(:p_name), :weigth, :m_speed, :efficiency_percentage,
				:record_time, :pallet_weight, :description, :allowed_machines)";
        $selectQ = "SELECT pid FROM products WHERE p_name = UPPER(:p_name)";

        try {
            $pdo = $pgc->prepare($selectQ);
            $pdo->bindValue(':p_name', $table['name']);
            $pdo->execute();

            if ($pdo->rowCount() < 1) {
                $pdo = $pgc->prepare($insertQ);
                $pdo->bindValue(':p_name', $table['name']);
                $pdo->bindValue(':weigth', $table['weight'], PDO::PARAM_INT);
                $pdo->bindValue(':m_speed', $table['m_min'], PDO::PARAM_INT);
                $pdo->bindValue(':efficiency_percentage', $table['eff'], PDO::PARAM_INT);
                $pdo->bindValue(':record_time', date("Y-m-d H:i:s", strtotime(date('Y-m-d H:i:s'))));
                $pdo->bindValue(':pallet_weight', $table['pallet'], PDO::PARAM_INT);
                $pdo->bindValue(':description', $table['descript']);
                $pdo->bindValue(':allowed_machines', '{"machines": {}, "info": ""}');
                $pdo->execute();

                $pdo = $pgc->prepare($selectQ);
                $pdo->bindValue(':p_name', $table['name']);
                $pdo->execute();
                $res = $pdo->fetchAll(PDO::FETCH_NUM);
                echo json_encode(array('error' => 0, 'msg' => $res[0][0]));
            } else {
                echo json_encode(array('error' => 1, 'msg' => 'Produkts "' . $table['name'] . '" jau eksistē.'));
            }
        } catch (PDOException $e) {
            $pgc = NULL;
            die('error in gc function => ' . $e->getMessage());
        }

        $pdo = NULL;
        $pgc = NULL;
    } else if (isset($_POST['pid']) && $_SESSION['login_user'] == 'admin' && $action == '_d') {
        require('../h/postgres_cmp.php');

        $deleteQ = "DELETE FROM products WHERE pid = :pid";
        $pid = (int)$_POST['pid'];

        try {
            $pdo = $pgc->prepare($deleteQ);
            $pdo->bindValue(':pid', $pid, PDO::PARAM_INT);
            $pdo->execute();
        } catch (PDOException $e) {
            $pgc = NULL;
            die('error in gc function => ' . $e->getMessage());
        }

        $pdo = NULL;
        $pgc = NULL;
    } else if (isset($_POST['product']) && $_SESSION['login_user'] == 'admin' && $action == '_signature') {
        require('../h/postgres_cmp.php');

        $updateMachineInfoQ = "UPDATE products SET allowed_machines = JSON_SET(allowed_machines, '$.info', :comment) WHERE p_name = UPPER(:p_name)";

        try {
            $pdo = $pgc->prepare($updateMachineInfoQ);
            $pdo->bindValue(':p_name', $_POST['product']);
            $pdo->bindValue(':comment', $_POST['comment']);
            $pdo->execute();
        } catch (PDOException $e) {
            $pgc = NULL;
            die('error in gc function => ' . $e->getMessage());
        }

        $pdo = NULL;
        $pgc = NULL;
    } else {
        die();
    }
}

?>