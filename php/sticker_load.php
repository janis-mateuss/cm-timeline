<?php
/* For rights see LICENSE.TXT */

if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

if (isset($_POST['name'])) {
    if ($_SESSION['login_user'] === 'admin' || $_SESSION['login_user'] === 'uzlimes') {
        require('../h/postgres_cmp.php');

        $selectQ = "SELECT sticker_color1, sticker_color2 FROM products WHERE p_name = :name LIMIT 1";

        try {
            $pdo = $pgc->prepare($selectQ);
            $pdo->bindValue(':name', $_POST['name']);
            $pdo->execute();
            $res = $pdo->fetchAll(PDO::FETCH_NUM);

            if ($pdo->rowCount() > 0) {
                $json = json_encode($res[0]);
                echo $json;
            } else {
                echo json_encode([]);
            }
        } catch (PDOException $e) {
            $pgc = NULL;
            die('error in gc function => ' . $e->getMessage());
        }

        $pdo = NULL;
        $pgc = NULL;
    }
}