<?php

if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

if (isset($_POST['id'])) {
    if ($_SESSION['login_user'] == 'admin' || $_SESSION['login_user'] == 'uzlimes') {
        require('../h/postgres_cmp.php');

        $selectQ = "SELECT * FROM cm_texturing_pallets WHERE id = :id ORDER BY record_time DESC LIMIT 1";

        try {
            $pdo = $pgc->prepare($selectQ);
            $pdo->bindValue(':id', $_POST['id'], PDO::PARAM_INT);
            $pdo->execute();
            $res = $pdo->fetchAll(PDO::FETCH_ASSOC);

            if ($pdo->rowCount() > 0) {
                // paleshu skaits izrekinaats
                $pallet_count = ($res[0]['kg'] > 0) ? ceil($res[0]['kg'] / $res[0]['pallet_weight']) : 0;

                echo '<table id="texturingHeaderTable">
		      		<tr><td class="header-img"><img src="css/images/logoLogin.png"></td><td class="header-mid">FB-851-056</td>
		      		<td class="header-left">
		      		<span class="level-1">Aizpildīts no: ___.___.______.<br />
		      		<span class="level-2">(DD . MM . GGGG )</span><br />
		      		<span class="level-3">līdz  ___.___.______.</span><br />
		      		<span class="level-4">(DD . MM . GGGG )</span>
		      		</span>
		      		</td></tr>
		      	</table>

		      	<table id="footer">
		      	<tr><td class="left-foot">SIA "Culimeta Baltics" formulārs<br />
		      	Pavairot un nodot uzņēmumam nepiederošām personām, arī daļēji, tikai ar rakstisku uzņēmuma vadības atļauju.
		      	</td>
		      	<td class="right-foot"><b><span class="revision-info" contenteditable="true">' . $res[0]['revision'] . '</span></b></td></tr>
		      	</table>

		      	<h2><b>Ir nepieciešams saražot <span class="pallet"><span class="pallet-count">' . $pallet_count . '</span> <span class="pallet-text" contenteditable="true">' . $res[0]['pallet_text'] . '</span></span><br />
		      	<u><span class="p-name">' . $res[0]["p_name"] . '</span> (<span class="p-description"> ' . $res[0]["description"] . '</span>)</u></b></h2>
		      	<p>Lūdzu, pie katras pilnās saražotās paletes aizpildīt šādu tabulu.</p>

		      	<div id="texturingCommentEditor">' . $res[0]['comment_text'] . '</div>
		      	<div id="texturingComment"></div>

		      	<div class="form-group add-col">
					<div class="form-inline">
		      			<button type="button" class="btn btn-sm btn-default" id="add-col-bttn">Pievienot kolonu</button>
		      			<input id="add-col-input" type="text" class="input-sm form-control" placeholder="Kolonas nosaukums" />
		      			<button type="button" class="btn btn-sm btn-default" id="save-signature-bttn">Saglabāt tekstu</button>
		      			<button type="button" class="btn btn-sm btn-default" id="load-signature-bttn">Ielādēt tekstu</button>
		      		</div>
		      	</div>
		      	<table id="texturingTable">' . $res[0]['texturing_table'] . '</table>
		      	<div id="footer-wrapper">
		      	<table id="texturingDateTable">
		      		<tr><td>Plānotais sākums:</td><td>' . date("d.m.Y", strtotime($res[0]['start_time'])) . '</td></tr>
		      		<tr><td>Beigas:</td><td>' . date("d.m.Y", strtotime($res[0]['end_time'])) . '</td></tr>
		      		<tr><td>Mašīnu skaits:</td><td>' . $res[0]['machine_count'] . '</td></tr>
		      		<tr><td>Daudzums:</td><td>~' . round(floatval($res[0]['kg']) / 1000, 2) . 't</td></tr>
		      	</table>
		      	<p>' . date("d.m.Y", strtotime($res[0]['record_time'])) . '
		      	</p>
		      	</div';

            }
        } catch (PDOException $e) {
            $pgc = NULL;
            die('error in gc function => ' . $e->getMessage());
        }

        $pdo = NULL;
        $pgc = NULL;
    }
} else if (
    ($_SESSION['login_user'] == 'admin' || $_SESSION['login_user'] == 'uzlimes') &&
    (empty($_POST) || (isset($_POST['startDate']) && isset($_POST['endDate'])))
) {
    $statement = "";
    $queryLimit = "LIMIT 50";

    if (isset($_POST['startDate'])) {
        require('../h/postgres_cmp.php');
    } else {
        require('h/postgres_cmp.php');
    }

    if (isset($_POST['startDate']) && isset($_POST['endDate']) && $_POST['startDate'] !== '' && $_POST['endDate'] !== '') {
        $statement = "WHERE record_time >= :startDate AND record_time <= :endDate";
        $queryLimit = "";
    }

    if (isset($_POST['product']) && $_POST['product'] !== '') {
        $statement .= $statement === '' ? "WHERE p_name LIKE :product" : " AND p_name LIKE :product";
    }

    $selectQ = "SELECT * FROM cm_texturing_pallets $statement ORDER BY record_time DESC $queryLimit";

    try {
        $pdo = $pgc->prepare($selectQ);
        if ($statement != "") {
            $pdo->bindValue(':startDate', $_POST['startDate']);
            $pdo->bindValue(':endDate', $_POST['endDate']);
            if (isset($_POST['product']) && $_POST['product'] !== '') {
                $pdo->bindValue(':product', '%' . strtoupper(trim($_POST['product'])) . '%');
            }

        }

        $pdo->execute();
        $res = $pdo->fetchAll(PDO::FETCH_ASSOC);

        foreach ($res as $key => $value) {
            // paleshu skaits izrekinaats
            $pallet_count = ($value['pallet_weight'] > 0) ? ceil($value['kg'] / $value['pallet_weight']) : 0;

            $html = '<tr>
				<td><button type="button" class="btn btn-sm btn-success info" aria-label="Left Align" name="' . $value['id'] . '" product="' . $value['p_name'] . '"><span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span> Atvērt</button>
		       	</td>
		        <td class="product-name not-editable">' . $value['p_name'] . '</td>';

            if ($_SESSION['login_user'] === 'admin') {
                $html .= '<td contenteditable="true" class="td1">' . $value['kg'] . '</td>
		        <td contenteditable="true" class="td2">' . $value['pallet_weight'] . '</td>';
            } else {
                $html .= '<td class="td1 not-editable">' . $value['kg'] . '</td>
		        <td class="td2 not-editable">' . $value['pallet_weight'] . '</td>';
            }

            $html .= '<td class="not-editable">' . $pallet_count . '</td>
		        <td class="not-editable">' . date("d.m.Y", strtotime($value['record_time'])) . '</td>
		        <td class="not-editable">' . date("d.m.Y", strtotime($value['start_time'])) . '</td>
		        <td class="not-editable">' . date("d.m.Y", strtotime($value['end_time'])) . '</td>';

            if ($_SESSION['login_user'] === 'admin') {
                $html .= '<td><button type="button" 
		        class="btn btn-success save" aria-label="Left Align" name="' . $value['id'] . '">
		        <span class="glyphicon glyphicon-floppy-disk"></span></button>
		       </td>
		        <td><button type="button" 
		        class="btn btn-danger delete" aria-label="Left Align" name="' . $value['id'] . '">
		        <span class="glyphicon glyphicon-remove"></span></button>
		       </td>';
            }

            $html .= '</tr>';

            echo $html;
        }
    } catch (PDOException $e) {
        $pgc = NULL;
        die('error in gc function => ' . $e->getMessage());
    }

    $pdo = NULL;
    $pgc = NULL;
}