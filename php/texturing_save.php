<?php
/* For rights see LICENSE.TXT */

session_start();

if (!isset($_POST['action'])) {
    die();
}
$action = $_POST['action'];

if (isset($_POST['texturingTasks']) && $action == '_n' && $_SESSION['login_user'] == 'admin') {
    require('../h/postgres_cmp.php');
    include 'variables.php';

    $selectQ = "SELECT pallet_weight, description FROM products WHERE p_name = UPPER(:p_name)";
    $insertQ = "INSERT INTO cm_texturing_pallets (p_name, kg, machine_count, description, pallet_weight, start_time, end_time, record_time, revision, pallet_text, texturing_table) 
			VALUES (UPPER(:p_name), :kg, :machine_count, :description, :pallet_weight, :start_time, :end_time, :record_time, :revision, :pallet_text, :texturing_table)";
    $tasks = json_decode($_POST['texturingTasks'], true);

    try {
        foreach ($tasks as $value) {
            // select query
            $pdo = $pgc->prepare($selectQ);
            $pdo->bindValue(':p_name', $value['prod']);
            $pdo->execute();
            $res = $pdo->fetchAll(PDO::FETCH_ASSOC);

            if ($pdo->rowCount() > 0) {
                $pallet_weight = $res[0]['pallet_weight'];
                $description = $res[0]['description'];

                $prodKg = $value['kg'];
                $pallet_count = ($pallet_weight > 0) ? ceil($prodKg / $pallet_weight) : 0;
                $table_html = $planSettings["texturing_table_head_default"];

                $table_html .= '<tbody>';
                for ($i = 0; $i < $pallet_count; $i++) {
                    $table_html .= '<tr><td class="nr">' . ($i + 1) . '.</td><td class="shift"></td><td class="mtd"></td></tr>';
                }
                $table_html .= '</tbody>';


                $pdo = $pgc->prepare($insertQ);
                $pdo->bindValue(':p_name', $value['prod']);
                $pdo->bindValue(':kg', $value['kg'], PDO::PARAM_INT);
                $pdo->bindValue(':machine_count', $value['machine_count'], PDO::PARAM_INT);
                $pdo->bindValue(':description', $description);
                $pdo->bindValue(':pallet_weight', (int)$pallet_weight, PDO::PARAM_INT);
                $pdo->bindValue(':start_time', $value['startDate']);
                $pdo->bindValue(':end_time', $value['endDate']);
                $pdo->bindValue(':record_time', date("Y-m-d H:i:s", strtotime(date('Y-m-d H:i:s'))));
                $pdo->bindValue(':revision', 'Rev. 10.08.2011');
                $pdo->bindValue(':pallet_text', 'pilnas Pal');
                $pdo->bindValue(':texturing_table', $table_html);
                $pdo->execute();
            }
        }
    } catch (PDOException $e) {
        $pgc = NULL;
        die('error in gc function => ' . $e->getMessage());
    }

    $pdo = NULL;
    $pgc = NULL;
} else if (isset($_POST['id']) && isset($_POST['comment']) &&
    $action == '_u' && $_SESSION['login_user'] == 'admin') {
    require('../h/postgres_cmp.php');
    include 'variables.php';

    $updateQ = "UPDATE cm_texturing_pallets SET comment_text = :comment_text, revision = :revision,
				pallet_text = :pallet_text, texturing_table = :texturing_table WHERE id = :id";

    $defaulTable = $planSettings["texturing_table_default"];

    try {
        $pdo = $pgc->prepare($updateQ);
        $pdo->bindValue(':id', $_POST['id'], PDO::PARAM_INT);
        $pdo->bindValue(':comment_text', (isset($_POST['comment']) ? $_POST['comment'] : ''));
        $pdo->bindValue(':pallet_text', (isset($_POST['palletText']) ? $_POST['palletText'] : ''));
        $pdo->bindValue(':revision', (isset($_POST['revision']) ? $_POST['revision'] : ''));
        $pdo->bindValue(':texturing_table', ($_POST['table'] != '' ? $_POST['table'] : $defaulTable));
        $pdo->execute();

    } catch (PDOException $e) {
        $pgc = NULL;
        die('error in gc function => ' . $e->getMessage());
    }

    $pdo = NULL;
    $pgc = NULL;
} else if (isset($_POST['id']) && $action == '_d' && $_SESSION['login_user'] == 'admin') {
    require('../h/postgres_cmp.php');
    print_r($_POST);
    $deleteQ = "DELETE FROM cm_texturing_pallets WHERE id = :id";

    try {
        $pdo = $pgc->prepare($deleteQ);
        $pdo->bindValue(':id', $_POST['id'], PDO::PARAM_INT);
        $pdo->execute();

    } catch (PDOException $e) {
        $pgc = NULL;
        die('error in gc function => ' . $e->getMessage());
    }

    $pdo = NULL;
    $pgc = NULL;
} else if (isset($_POST['kg']) && isset($_POST['pallet_weight']) &&
    $action == '_u_table' && $_SESSION['login_user'] == 'admin') {
    require('../h/postgres_cmp.php');

    $updateQ = "UPDATE cm_texturing_pallets SET kg = :kg, pallet_weight = :pallet_weight WHERE id = :id";

    try {
        $pdo = $pgc->prepare($updateQ);
        $pdo->bindValue(':id', $_POST['pid'], PDO::PARAM_INT);
        $pdo->bindValue(':kg', (int)$_POST['kg'], PDO::PARAM_INT);
        $pdo->bindValue(':pallet_weight', (int)$_POST['pallet_weight'], PDO::PARAM_INT);
        $pdo->execute();

        echo json_encode(array('error' => 0));

    } catch (PDOException $e) {
        $pgc = NULL;
        die('error in gc function => ' . $e->getMessage());
    }

    $pdo = NULL;
    $pgc = NULL;
}
?>