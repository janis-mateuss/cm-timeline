<?php
/* For rights see LICENSE.TXT */

$planSettings = [
    "texturing_table_default" => '<thead><tr><th>Nr.</th><th class="shift-th">Maiņa, kura noslēdz paleti</th><th>MTD / NMTD</th></tr></thead><tbody></tbody>',
    "texturing_table_head_default" => '<thead><tr><th>Nr.</th><th class="shift-th">Maiņa, kura noslēdz paleti</th><th>MTD / NMTD</th></tr></thead>'
];