<?php
/* For rights see LICENSE.TXT */

if (isset($_POST['action'])) {
    session_start();
    if ($_POST['action'] == '_get_all') {
        require('../h/postgres_cmp.php');

        $selectQ = "SELECT id, name, grouped FROM machines WHERE active = 1 ORDER BY sort_order ASC";

        try {
            $pdo = $pgc->prepare($selectQ);
            $pdo->execute();
            $res = $pdo->fetchAll(PDO::FETCH_ASSOC);

            echo json_encode(
                array(
                    'count' => $pdo->rowCount(),
                    'mapping' => $res
                )
            );
        } catch (PDOException $e) {
            $pgc = NULL;
            die('error in gc function => ' . $e->getMessage());
        }

        $pdo = NULL;
        $pgc = NULL;
    }
} else {
    $is_admin = false;

    if ($_SESSION['login_user'] == 'admin') {
        $is_admin = true;
    }

    require('h/postgres_cmp.php');

    $selectQ = "SELECT id, name, sort_order, active, grouped, description FROM machines ORDER BY sort_order";

    try {
        $pdo = $pgc->prepare($selectQ);
        $pdo->execute();
        $res = $pdo->fetchAll(PDO::FETCH_ASSOC);

        foreach ($res as $key => $value) {
            echo '<tr draggable="true" ondragstart="start()" ondragover="dragover()">
                <td class="td3 not-editable"></td>
                <td ' . ($is_admin ? 'class="td2" contenteditable="true"' : '') . '>' . $value['name'] . '</td>
                <td ' . ($is_admin ? 'class="td5" contenteditable="true"' : '') . '>' . $value['description'] . '</td>
                <td class="td1">
                    <div class="checkbox">
					  <label>
					    <label class="checkbox-inline">
						  <input type="checkbox" value="1" ' . ($value['active'] ? "checked" : "") . '
						  	' . (!$is_admin ? "disabled" : "") . '>
						</label>
					  </label>
					</div>
                </td>
                <td class="td4">
                    <select>
                        <option value="no" ' . ($value['grouped'] == "no" ? "selected" : "") . '>Nē</option>
                        <option value="yes" ' . ($value['grouped'] == "yes" ? "selected" : "") . '>Jā</option>
                        <option value="start" ' . ($value['grouped'] == "start" ? "selected" : "") . '>Grupas sākums</option>
                        <option value="end" ' . ($value['grouped'] == "end" ? "selected" : "") . '>Grupas beigas</option>
				    </select>
				</td>
		        <td>' . ($is_admin ? '<button type="button" 
		        class="btn btn-success save" aria-label="Left Align" name="' . $value['id'] . '">
		        <span class="glyphicon glyphicon-floppy-disk"></span></button>' : '') . '
		       	</td>
		        <td>' . ($is_admin ? '<button type="button" 
		        class="btn btn-danger delete" aria-label="Left Align" name="' . $value['id'] . '">
		        <span class="glyphicon glyphicon-remove"></span></button>' : '') . '
		       </td>
		      </tr>';
        }
    } catch (PDOException $e) {
        $pgc = NULL;
        die('error in gc function => ' . $e->getMessage());
    }

    $pdo = NULL;
    $pgc = NULL;
}

?>