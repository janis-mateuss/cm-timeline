<?php
/* For rights see LICENSE.TXT */

if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

if (isset($_POST['id'])) {
    if ($_SESSION['login_user'] == 'admin') {
        require('../h/postgres_cmp.php');

        $selectQ = "SELECT plan_content FROM cm_plan_summary WHERE id = :id LIMIT 1";

        try {
            $pdo = $pgc->prepare($selectQ);
            $pdo->bindValue(':id', $_POST['id'], PDO::PARAM_INT);
            $pdo->execute();
            $res = $pdo->fetchAll(PDO::FETCH_NUM);
            if ($pdo->rowCount() > 0) {
                echo $res[0][0];
            }
        } catch (PDOException $e) {
            $pgc = NULL;
            die('error in gc function => ' . $e->getMessage());
        }

        $pdo = NULL;
        $pgc = NULL;
    }
} else if ($_SESSION['login_user'] == 'admin' && (empty($_POST) || (isset($_POST['startDate']) && isset($_POST['endDate'])))) {

    $statement = "";
    $queryLimit = "LIMIT 50";
    if (isset($_POST['startDate']) && isset($_POST['endDate'])) {
        require('../h/postgres_cmp.php');
        $statement = "WHERE plan_date >= :startDate AND plan_date <= :endDate";
        $queryLimit = "";
    } else {
        require('h/postgres_cmp.php');
    }

    $selectQ = "SELECT * FROM cm_plan_summary $statement ORDER BY plan_date DESC $queryLimit";

    try {
        $pdo = $pgc->prepare($selectQ);
        if ($statement != "") {
            $pdo->bindValue(':startDate', $_POST['startDate']);
            $pdo->bindValue(':endDate', $_POST['endDate']);
        }
        $pdo->execute();
        $res = $pdo->fetchAll(PDO::FETCH_ASSOC);

        foreach ($res as $key => $value) {
            echo '<tr>
				<td><button type="button" class="btn btn-sm btn-success info" aria-label="Left Align" name="' . $value['id'] . '"><span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span> Atvērt</button>
		       	</td>
		       	<td class="not-editable">' . date("d.m.Y", strtotime($value['plan_date'])) . '</td>
		        <td class="not-editable">' . $value['plan_type'] . '</td>
		        <td class="not-editable">' . date("d.m.Y", strtotime($value['record_time'])) . '</td>
		        <td><button type="button" 
		        class="btn btn-success hidden" aria-label="Left Align">
		        </button>
		       </td>
		        <td><button type="button" 
		        class="btn btn-danger delete" aria-label="Left Align" name="' . $value['id'] . '">
		        <span class="glyphicon glyphicon-remove"></span></button>
		       </td>
		      </tr>';
        }
    } catch (PDOException $e) {
        $pgc = NULL;
        die('error in gc function => ' . $e->getMessage());
    }

    $pdo = NULL;
    $pgc = NULL;
}
?>