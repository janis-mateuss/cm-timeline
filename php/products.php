<?php
/* For rights see LICENSE.TXT */

if (isset($_POST['q'])) {

    require('../h/postgres_cmp.php');

    $results = array();
    $query = $_POST['q'] . '%'; // add % for LIKE query later

    $selectQ = 'SELECT p_name FROM products WHERE  UPPER(p_name) LIKE UPPER(:query)';

    try {

        $pdo = $pgc->prepare($selectQ);
        $pdo->bindParam(':query', $query);
        $pdo->execute();
        $res = $pdo->fetchAll(PDO::FETCH_NUM);

        foreach ($res as $key => $value) {
            $results[] = $value[0];
        }
        echo json_encode($results);
    } catch (PDOException $e) {
        $pgc = NULL;
        die('error in gc function => ' . $e->getMessage());
    }
    $pgc = NULL;
} else if (isset($_POST['product'])) {

    require('../h/postgres_cmp.php');

    $prod = $_POST['product'];
    $resMachines = array();

    $selectQ = 'SELECT allowed_machines FROM products WHERE UPPER(p_name) = UPPER(:product) LIMIT 1';
    $selectMachineQ = "SELECT id, name FROM machines ORDER BY sort_order";

    try {
        $pdo = $pgc->prepare($selectQ);
        $pdo->bindParam(':product', $prod);
        $pdo->execute();
        $resMachines = $pdo->fetchAll(PDO::FETCH_NUM);

        $pdo = $pgc->prepare($selectMachineQ);
        $pdo->execute();
        $allMachines = $pdo->fetchAll(PDO::FETCH_ASSOC);

        echo json_encode(['machines' => $allMachines, 'restricted' => json_decode($resMachines[0][0], true)]);

    } catch (PDOException $e) {
        $pgc = NULL;
        die('error in gc function => ' . $e->getMessage());
    }

    $pgc = NULL;
}
?>