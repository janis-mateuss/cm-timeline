<?php
/* For rights see LICENSE.TXT */

session_start();

if (!isset($_POST['action'])) {
    die();
}
$action = $_POST['action'];

if (isset($_POST['planDate']) && isset($_POST['planType']) && $action == '_n' && $_SESSION['login_user'] == 'admin') {
    require('../h/postgres_cmp.php');

    $insertQ = "INSERT INTO cm_plan_summary (plan_content, plan_type, plan_date, record_time) 
			VALUES (:plan_content, :plan_type, :plan_date, :record_time)";
    try {
        $pdo = $pgc->prepare($insertQ);
        $pdo->bindValue(':plan_content', $_POST['planContent']);
        $pdo->bindValue(':plan_type', $_POST['planType']);
        $pdo->bindValue(':plan_date', $_POST['planDate']);
        $pdo->bindValue(':record_time', date("Y-m-d H:i:s", strtotime(date('Y-m-d H:i:s'))));
        $pdo->execute();
    } catch (PDOException $e) {
        $pgc = NULL;
        die('error in gc function => ' . $e->getMessage());
    }

    $pdo = NULL;
    $pgc = NULL;
} else if (isset($_POST['id']) && isset($_POST['planContent']) &&
    $action == '_u' && $_SESSION['login_user'] == 'admin') {
    require('../h/postgres_cmp.php');

    $updateQ = "UPDATE cm_plan_summary SET plan_content = :plan_content WHERE id = :id";

    try {
        $pdo = $pgc->prepare($updateQ);
        $pdo->bindValue(':id', $_POST['id'], PDO::PARAM_INT);
        $pdo->bindValue(':plan_content', $_POST['planContent']);
        $pdo->execute();

    } catch (PDOException $e) {
        $pgc = NULL;
        die('error in gc function => ' . $e->getMessage());
    }

    $pdo = NULL;
    $pgc = NULL;
} else if (isset($_POST['id']) && $action == '_d' && $_SESSION['login_user'] == 'admin') {
    require('../h/postgres_cmp.php');
    $deleteQ = "DELETE FROM cm_plan_summary WHERE id = :id";

    try {
        $pdo = $pgc->prepare($deleteQ);
        $pdo->bindValue(':id', $_POST['id'], PDO::PARAM_INT);
        $pdo->execute();

    } catch (PDOException $e) {
        $pgc = NULL;
        die('error in gc function => ' . $e->getMessage());
    }

    $pdo = NULL;
    $pgc = NULL;
}

?>