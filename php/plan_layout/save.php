<?php
/* For rights see LICENSE.TXT */

session_start();

if (isset($_POST['action']) && isset($_POST['plan'])) {
    $action = $_POST['action'];

    if ($_SESSION['login_user'] == 'admin' && $action == '_u') {
        require('../../h/postgres_cmp.php');


        $updatePlanLayout = "UPDATE cm_config SET config_value = :plan_layout WHERE config_name = 'plan_layout'";

        try {
            $pdo = $pgc->prepare($updatePlanLayout);
            $pdo->bindValue(':plan_layout', $_POST['plan']);
            $pdo->execute();
        } catch (PDOException $e) {
            $pgc = NULL;
            die('error in gc function => ' . $e->getMessage());
        }

        $pdo = NULL;
        $pgc = NULL;
    }
}

?>