<?php
/* For rights see LICENSE.TXT */

if (isset($_POST['pid'])) {
    require('../h/postgres_cmp.php');

    $selectQ = "SELECT allowed_machines FROM products WHERE pid = :pid";
    $selectMachineQ = "SELECT id, name FROM machines ORDER BY sort_order";

    try {
        $pdo = $pgc->prepare($selectQ);
        $pdo->bindValue(':pid', $_POST['pid']);
        $pdo->execute();
        $res = $pdo->fetchAll(PDO::FETCH_NUM);

        if ($pdo->rowCount() > 0) {
            $json = json_decode($res[0][0], true);

            echo '<textarea class="form-control custom-control" rows="3" style="resize:none" 
			id="overallComment">' . $json['info'] . '</textarea>
			<div class="table-responsive">        
			  <table class="table table-striped" id="productsInfoTable">
			    <thead>
			      <tr>
			        <th>Mašīna</th>
			        <th>Komentārs</th>
			      </tr>
			    </thead>
			    <tbody>';

            $pdo = $pgc->prepare($selectMachineQ);
            $pdo->execute();
            $machines = $pdo->fetchAll(PDO::FETCH_ASSOC);

            for ($i = 0; $i < count($machines); $i++) {
                $restricted = isset($json['machines'][$machines[$i]['id']]['check']) && $json['machines'][$machines[$i]['id']]['check'] == 0 ? "" : "checked";

                echo '<tr><td><div class="checkbox"><label data-id="' . $machines[$i]['id'] .'"><input type="checkbox" ' . $restricted . '>' . $machines[$i]['name'] . '</label></div></td>';
                echo '<td><input type="text" class="form-control input-sm" value="' . ($json['machines'][$machines[$i]['id']]['comment'] ?? "") . '"></td></tr>';
            }

            echo '</tbody>
			  </table>
			  </div>';
        }

    } catch (PDOException $e) {
        $pgc = NULL;
        die('error in gc function => ' . $e->getMessage());
    }

    $pdo = NULL;
    $pgc = NULL;
}

?>