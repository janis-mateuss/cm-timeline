<?php
/* For rights see LICENSE.TXT /*

/* script-3.2.4.php is example install script.

install_scripts folder should contain php sql script files

Install script file naming is script-[version number].php

Script will be executed only if [version number] is equal
or greater then version which is in version.txt file in project root.

Example:
version.txt file contains 3.2.4 (which is system version overall) and
php install script file name script-3.2.5.php it will be executed.
If file name would be script-3.2.3.php then file is not executed
because 3.2.4 (system version) is greater then 3.2.3 (script version).

Install scripts will be run when system update is actioned through web.

*/