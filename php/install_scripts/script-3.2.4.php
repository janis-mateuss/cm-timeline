<?php
/* For rights see LICENSE.TXT */

if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

if ($_SESSION['login_user'] === 'admin') {
    require('../h/postgres_cmp.php');

    $colCheck1 = "SELECT * FROM information_schema.columns WHERE TABLE_NAME = 'products' AND COLUMN_NAME = 'sticker_color1'";

    try {
        $pdo = $pgc->prepare($colCheck1);
        $pdo->execute();
        $pdo->fetchAll(PDO::FETCH_NUM);

        if ($pdo->rowCount() < 1) {
            // column does not exist in products table
        } else {
            // column exists in products table
        }
    } catch (PDOException $e) {
        $pgc = NULL;
        die('error in gc function => ' . $e->getMessage());
    }

    $pdo = NULL;
    $pgc = NULL;
}
?>