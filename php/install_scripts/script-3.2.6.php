<?php
/* For rights see LICENSE.TXT */

if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

if ($_SESSION['login_user'] === 'admin') {
    require('../h/postgres_cmp.php');

    $colCheck1 = "SELECT * FROM information_schema.columns WHERE TABLE_NAME = 'products' AND COLUMN_NAME = 'sticker_color1'";
    $colCheck2 = "SELECT * FROM information_schema.columns WHERE TABLE_NAME = 'products' AND COLUMN_NAME = 'sticker_color2'";
    $addCol1Sql = "ALTER TABLE products ADD sticker_color1 VARCHAR (30) NULL";
    $addCol2Sql = "ALTER TABLE products ADD sticker_color2 VARCHAR (30) NULL";

    try {
        $pdo = $pgc->prepare($colCheck1);
        $pdo->execute();
        $pdo->fetchAll(PDO::FETCH_NUM);

        if ($pdo->rowCount() < 1) {
            $pdo = $pgc->prepare($addCol1Sql);
            $pdo->execute();
        }

        $pdo = $pgc->prepare($colCheck2);
        $pdo->execute();
        $pdo->fetchAll(PDO::FETCH_NUM);

        if ($pdo->rowCount() < 1) {
            $pdo = $pgc->prepare($addCol2Sql);
            $pdo->execute();
        }
    } catch (PDOException $e) {
        $pgc = NULL;
        die('error in gc function => ' . $e->getMessage());
    }

    $pdo = NULL;
    $pgc = NULL;
}
?>