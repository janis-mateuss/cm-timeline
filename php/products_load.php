<?php
/* For rights see LICENSE.TXT */

if (isset($_POST['product']) && isset($_POST['action'])) {
    session_start();
    if ($_POST['action'] == '_signature_l' && $_SESSION['login_user'] == 'admin') {
        require('../h/postgres_cmp.php');

        $selectQ = "SELECT JSON_VALUE(allowed_machines, '$.info') FROM products WHERE p_name = UPPER(:p_name)";

        try {
            $pdo = $pgc->prepare($selectQ);
            $pdo->bindValue(':p_name', $_POST['product']);
            $pdo->execute();
            $res = $pdo->fetchAll(PDO::FETCH_NUM);

            echo $res[0][0] ?? '';
        } catch (PDOException $e) {
            $pgc = NULL;
            die('error in gc function => ' . $e->getMessage());
        }

        $pdo = NULL;
        $pgc = NULL;
    }
} else {

    $is_admin = false;
    if ($_SESSION['login_user'] == 'admin') {
        $is_admin = true;
    }

    require('h/postgres_cmp.php');

    $selectQ = "SELECT pid, p_name, weigth, m_speed, m_count, efficiency_percentage,
				pallet_weight, description, sticker_color1, sticker_color2 FROM products ORDER BY p_name";

    try {
        $pdo = $pgc->prepare($selectQ);
        $pdo->execute();
        $res = $pdo->fetchAll(PDO::FETCH_ASSOC);

        foreach ($res as $key => $value) {
            // =C117*60*B117/1000/1000*L117/100
            $kg_h = number_format($value['m_speed'] * 60 * $value['weigth'] / 1000 / 1000 * $value['efficiency_percentage'] / 100, 3);
            $total_kg_h = number_format($kg_h * $value['m_count'], 3);

            echo '<tr>
				<td><button type="button" class="btn btn-sm btn-success info" aria-label="Left Align" id="' . $value['p_name'] . '" name="' . $value['pid'] . '">
				  <span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span> Atvērt
				</button>
				</td>
		        <td ' . ($is_admin ? 'class="td1" contenteditable="true"' : '') . '>' . $value['p_name'] . '</td>
		        <td ' . ($is_admin ? 'class="td2" contenteditable="true"' : '') . '>' . $value['weigth'] . '</td>
		        <td ' . ($is_admin ? 'class="td3" contenteditable="true"' : '') . '>' . $value['m_speed'] . '</td>
		        <td class="td4" style="color: #888">' . $kg_h . '</td>
		        <td class="td5" style="color: #888">' . $value['m_count'] . '</td>
		        <td class="td6" style="color: #888">' . $total_kg_h . '</td>
		        <td ' . ($is_admin ? 'class="td7" contenteditable="true"' : '') . '>' . $value['efficiency_percentage'] . '</td>
		        <td ' . ($is_admin ? 'class="td8" contenteditable="true"' : '') . '>' . $value['pallet_weight'] . '</td>
		        <td ' . ($is_admin ? 'class="td9" contenteditable="true"' : '') . '>' . $value['description'] . '</td>
		        <td ' . ($is_admin ? 'class="td10 select-color" contenteditable="true"' : 'class="td10"') . '>' . $value['sticker_color1'] . '</td>
		        <td ' . ($is_admin ? 'class="td11 select-color" contenteditable="true"' : 'class="td11"') . '>' . $value['sticker_color2'] . '</td>
		        <td>' . ($is_admin ? '<button type="button" 
		        class="btn btn-success save" aria-label="Left Align" name="' . $value['pid'] . '">
		        <span class="glyphicon glyphicon-floppy-disk"></span></button>' : '') . '
		       	</td>
		        <td>' . ($is_admin ? '<button type="button" 
		        class="btn btn-danger delete" aria-label="Left Align" name="' . $value['pid'] . '">
		        <span class="glyphicon glyphicon-remove"></span></button>' : '') . '
		       </td>
		      </tr>';
        }
    } catch (PDOException $e) {
        $pgc = NULL;
        die('error in gc function => ' . $e->getMessage());
    }

    $pdo = NULL;
    $pgc = NULL;
}

?>