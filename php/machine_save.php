<?php
/* For rights see LICENSE.TXT */

session_start();

if (isset($_POST['action'])) {
    $action = $_POST['action'];

    if (isset($_POST['table']) && $_SESSION['login_user'] == 'admin' && $action == '_u') {
        require('../h/postgres_cmp.php');

        $table = json_decode($_POST['table'], true);

        $updateQ = "UPDATE machines SET name = :name, active = :active, grouped = :grouped, description = :description WHERE id = :id";

        $selectQ = "SELECT id FROM machines WHERE name = :name AND id != :id";
        try {
            // check needed in order to prevent renaming product to a name which is already in use for different product
            $pdo = $pgc->prepare($selectQ);
            $pdo->bindValue(':name', $table['name']);
            $pdo->bindValue(':id', $table['id']);
            $pdo->execute();

            if ($pdo->rowCount() < 1) {
                $pdo = $pgc->prepare($updateQ);
                $pdo->bindValue(':id', $table['id'], PDO::PARAM_INT);
                $pdo->bindValue(':name', $table['name']);
                $pdo->bindValue(':active', $table['active'], PDO::PARAM_BOOL);
                $pdo->bindValue(':grouped', $table['grouped']);
                $pdo->bindValue(':description', $table['description']);

                $pdo->execute();
                echo json_encode(array('error' => 0, 'msg' => $table['id']));
            } else {
                echo json_encode(array('error' => 1, 'msg' => 'Mašīna "' . $table['name'] . '" jau eksistē.'));
            }
        } catch (PDOException $e) {
            $pgc = NULL;
            die('error in gc function => ' . $e->getMessage());
        }

        $pdo = NULL;
        $pgc = NULL;
    } else if (isset($_POST['table']) && $_SESSION['login_user'] == 'admin' && $action == '_i') {
        require('../h/postgres_cmp.php');

        $table = json_decode($_POST['table'], true);

        $insertQ = "INSERT INTO machines (name, active, sort_order, grouped) VALUES (:name, :active, :sort_order, 'no')";
        $selectQ = "SELECT id FROM machines WHERE name = UPPER(:name)";

        try {
            $pdo = $pgc->prepare($selectQ);
            $pdo->bindValue(':name', $table['name']);
            $pdo->execute();

            if ($pdo->rowCount() < 1) {
                $pdo = $pgc->prepare($insertQ);
                $pdo->bindValue(':name', $table['name']);
                $pdo->bindValue(':active', $table['active'], PDO::PARAM_BOOL);
                $pdo->bindValue(':sort_order', $table['sort_order'], PDO::PARAM_INT);
                $pdo->execute();

                $pdo = $pgc->prepare($selectQ);
                $pdo->bindValue(':name', $table['name']);
                $pdo->execute();
                $res = $pdo->fetchAll(PDO::FETCH_NUM);
                echo json_encode(array('error' => 0, 'msg' => $res[0][0]));
            } else {
                echo json_encode(array('error' => 1, 'msg' => 'Produkts "' . $table['name'] . '" jau eksistē.'));
            }
        } catch (PDOException $e) {
            $pgc = NULL;
            die('error in gc function => ' . $e->getMessage());
        }

        $pdo = NULL;
        $pgc = NULL;
    } else if (isset($_POST['id']) && $_SESSION['login_user'] == 'admin' && $action == '_d') {
        require('../h/postgres_cmp.php');

        $deleteQ = "DELETE FROM machines WHERE id = :id";
        $deletePlanQ = "DELETE FROM plan WHERE machine = :machine";
        $deleteAllowedMachineQ = "UPDATE products SET allowed_machines = JSON_REMOVE(allowed_machines, :machine_path)";

        $id = (int)$_POST['id'];

        try {
            $pdo = $pgc->prepare($deleteQ);
            $pdo->bindValue(':id', $id, PDO::PARAM_INT);
            $pdo->execute();

            $pdo = $pgc->prepare($deletePlanQ);
            $pdo->bindValue(':machine', $id, PDO::PARAM_INT);
            $pdo->execute();

            $pdo = $pgc->prepare($deleteAllowedMachineQ);
            $pdo->bindValue(':machine_path', '$.machines.' . $id);
            $pdo->execute();
        } catch (PDOException $e) {
            $pgc = NULL;
            die('error in gc function => ' . $e->getMessage());
        }

        $pdo = NULL;
        $pgc = NULL;

    } else if (isset($_POST['table']) && $_SESSION['login_user'] == 'admin' && $action == '_order') {
        require('../h/postgres_cmp.php');

        $table = json_decode($_POST['table'], true);

        $updateQ = "UPDATE machines SET sort_order = :sort_order WHERE id = :id";

        foreach ($table as $key => $value) {
            try {
                $pdo = $pgc->prepare($updateQ);
                $pdo->bindValue(':id', $key, PDO::PARAM_INT);
                $pdo->bindValue(':sort_order', $value, PDO::PARAM_INT);
                $pdo->execute();
            } catch (PDOException $e) {
                $pgc = NULL;
                die('error in gc function => ' . $e->getMessage());
            }
        }

        $pdo = NULL;
        $pgc = NULL;
    } else {
        die();
    }
}

?>