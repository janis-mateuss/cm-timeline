<?php
/* For rights see LICENSE.TXT */
include('php/session.php');
$title = 'Plānošana · Culimeta';
include("header.php");

if ($_SESSION['login_user'] == 'admin') {
    echo '<link rel="stylesheet" href="css/noselect.css" type="text/css" media="screen"/>';
    echo '<link rel="stylesheet" href="css/paper-header.css" type="text/css" media="screen"/>';
    echo '<link rel="stylesheet" href="css/print.css" type="text/css" media="print"/>';
    echo '<link rel="stylesheet" href="css/paper-header-print.css" type="text/css" media="print"/>';
}

include("plan.php");
?>

<?php include("footer.html"); ?>

<script src="js/jquery-ui.min.js"></script>
<script type="text/javascript" src="bootstrap/js/typeahead.bundle.js"></script>
<script type="text/javascript" src="datepicker/jquery.plugin.js"></script>
<script type="text/javascript" src="datepicker/jquery.datepick.min.js"></script>
<script type="text/javascript" src="datepicker/jquery.datepick-lv.js"></script>
<script type="text/javascript" src="js/jquery.toaster.js"></script>
<script type="text/javascript" src="js/scripts.js"></script>
<?php
if ($_SESSION['login_user'] != 'admin') {
    echo '<script type="text/javascript" src="js/unbind.js"></script>';
}
?>
</html>