<?php
/* For rights see LICENSE.TXT */
include('php/session.php');
$title = 'Teksturēšana · Culimeta';
include("header.php");
?>
<div class="container-other">

    <div id="productsTableWrapper" class="table-responsive" style="margin-top: 20px;">
        <div class="form-inline" style="border-bottom: 1px solid #DDD;">
            <label for="bttn-add-product" id="labelProduct">Ražošanas uzdevumi
                <div class="form-inline" style="margin-left: 10px; float: right;;">
                    <div class="input-daterange input-group" id="datepicker">
                        <span class="input-group-addon">no</span>
                        <input id="datepickerFrom"
                               type="text"
                               class="input-sm form-control popupDatepicker"
                               name="start"
                               autocomplete="off"/>
                        <span class="input-group-addon">līdz</span>
                        <input id="datepickerTo"
                               type="text"
                               class="input-sm form-control popupDatepicker"
                               name="end"
                               autocomplete="off"/>
                    </div>
                    <input id="texturing-product"
                           type="text"
                           class="input-sm form-control"
                           name="texturing-product"
                           autocomplete="off"
                           placeholder="Produkts">
                    <button type="button" class="btn btn-sm btn-default" id="load-interval-bttn" data-placement="bottom"
                            title="">Atlasīt
                    </button>
                </div>
        </div>
        <p style="margin-top: -20px;text-align: right;font-size: 12px;color: #AAA;">Jaunākie 50 ražošanas uzdevumi</p>

        <div id="message"></div>

        <table class="table table-striped" id="productsTable">
            <div id="ajax_loader">
                <div class="spinner">
                    <div class="rect1"></div>
                    <div class="rect2"></div>
                    <div class="rect3"></div>
                    <div class="rect4"></div>
                    <div class="rect5"></div>
                </div>
            </div>
            <thead class="persist-header">
            <tr>
                <th style="width: 100px;">Pārskats</th>
                <th class="product-name">Produkts</th>
                <th>Kg</th>
                <th>Paletes svars</th>
                <th>Palešu skaits</th>
                <th>Pievienošanas datums</th>
                <th>Sākuma datums</th>
                <th>Beigu datums</th>
                <?php if ($_SESSION['login_user'] === 'admin'): ?>
                    <th style="width: 50px;"></th>
                    <th style="width: 50px;"></th>
                <?php endif; ?>
            </tr>
            </thead>
            <tbody>
            <?php
            include('php/texturing_load.php');
            ?>
            </tbody>
        </table>
    </div>
</div><!-- main container -->


<div class="modal" id="texturingModal" data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Teksturēšana</h4>
            </div>
            <div class="modal-body <?php echo $_SESSION['login_user'] === 'uzlimes' ? 'active-sticker initial' : ''; ?>">
                <div class="texturing-container"></div>
                <div class="sticker-container-position">
                    <input type="number" class="form-control col1"
                           data-target=".sticker-container .sticker:nth-of-type(3n + 1)" placeholder="Nobīde (px)">
                    <input type="number" class="form-control col2"
                           data-target=".sticker-container .sticker:nth-of-type(3n + 2)" placeholder="Nobīde (px)">
                    <input type="number" class="form-control col3"
                           data-target=".sticker-container .sticker:nth-of-type(3n)" placeholder="Nobīde (px)">
                </div>
                <div class="sticker-container">
                    <div class="sticker">
                        <div class="sticker-left" data-position="18px" data-position-last="24px">
                            <div class="sticker-name"></div>
                            <div class="sticker-description"></div>
                        </div>
                        <div class="sticker-mid" data-position="35% + 8px" data-position-last="35% + 8px">
                            <div class="sticker-mid-up"></div>
                            <div class="sticker-mid-bottom"></div>
                        </div>
                        <div class="sticker-right" data-position="61%" data-position-last="61% + 3px">
                            <div class="sticker-name"></div>
                            <div class="sticker-description"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button style="margin-right: 20px;" type="button" id="texturing-back" class="btn btn-default"
                        data-dismiss="modal">Aizvērt
                </button>
                <?php if ($_SESSION['login_user'] !== 'uzlimes'): ?>
                    <button style="margin-right: 20px;" type="button" id="sticker-back" class="btn btn-default">
                        Atpakaļ
                    </button>
                <?php endif; ?>
                <button id="print-sticekrs-bttn" type="button" class="btn btn btn-default">Drukāt uzlīmes<span
                            class="glyphicon glyphicon-paste" aria-hidden="true"></span></button>
                <button id="print-bttn" type="button" class="btn btn btn-default">Drukāt <span
                            class="glyphicon glyphicon-print" aria-hidden="true"></span></button>
                <input type="text" class="form-control" id="sticker-count" maxlength="4" placeholder="Uzlīmju skaits">
                <?php if ($_SESSION['login_user'] === 'admin'): ?>
                    <button type="button" class="btn btn-success" id="bttn-save-modal">Saglabāt</button>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>

<?php include("footer.html"); ?>

<link rel="stylesheet" href="css/texturing.css" type="text/css" media="screen"/>
<link rel="stylesheet" href="css/paper-header.css" type="text/css" media="screen"/>
<link rel="stylesheet" href="css/texturing-print.css" type="text/css" media="print"/>
<link rel="stylesheet" href="css/paper-header-print.css" type="text/css" media="print"/>
<script type="text/javascript" src="datepicker/jquery.plugin.js"></script>
<script type="text/javascript" src="datepicker/jquery.datepick.min.js"></script>
<script type="text/javascript" src="datepicker/jquery.datepick-lv.js"></script>
<script type="text/javascript" src="js/tinymce/tinymce.min.js"></script>
<script type="text/javascript" src="js/jquery.toaster.js"></script>
<script type="text/javascript" src="js/texturing.js"></script>
<script type="text/javascript" src="js/totop.js"></script>

</html>