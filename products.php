<?php
/* For rights see LICENSE.TXT */
include('php/session.php');
$title = 'Produkti · Culimeta';
include("header.php");
?>
<div class="container-other">

    <div id="productsTableWrapper" class="table-responsive">
        <div id="ajax_loader">
            <div class="spinner">
                <div class="rect1"></div>
                <div class="rect2"></div>
                <div class="rect3"></div>
                <div class="rect4"></div>
                <div class="rect5"></div>
            </div>
        </div>

        <div class="form-inline" style="margin: 0px auto; border-bottom: 1px solid #DDD;">
            <label for="bttn-add-product" id="labelProduct">Produkti
                <?php if ($_SESSION['login_user'] == 'admin'): ?>
                    <button type="button" class="btn btn-default" id="bttn-add-product"><span
                                class="glyphicon glyphicon-plus" aria-hidden="true"></span></button>
                <?php endif; ?>
            </label>
            <?php if ($_SESSION['login_user'] == 'admin'): ?>
                <button id="generate-colors" type="button" class="btn btn btn-default">Ģenerēt krāsas saglabātajiem
                    produktiem
                    <span class="glyphicon glyphicon-random" aria-hidden="true"></span>
                </button>
            <?php endif; ?>

        </div>

        <div id="message"></div>

        <table class="table table-striped" id="productsTable">
            <thead class="persist-header">
            <tr>
                <th>Mašīnas</th>
                <th>Produkts</th>
                <th>Svars</th>
                <th>m/min</th>
                <th>Kg/h</th>
                <th>Maš. skaits</th>
                <th>Kopējais kg/h</th>
                <th>Efekt. %</th>
                <th>Paletes svars</th>
                <th>Apraksts</th>
                <th>1. krāsa</th>
                <th>2. krāsa</th>
                <th style="width: 50px;"></th>
                <th style="width: 50px;"></th>
            </tr>
            </thead>
            <tbody>
            <?php
            include('php/products_load.php');
            ?>
            </tbody>
        </table>
    </div>
</div><!-- main container -->


<div class="modal" id="productModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Mašīnu saraksts</h4>
            </div>
            <div class="modal-body">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Aizvērt</button>
                <?php
                if ($_SESSION['login_user'] == 'admin') {
                    echo '<button type="button" class="btn btn-success" data-dismiss="modal" id="bttn-save-modal">Saglabāt</button>';
                }
                ?>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<?php include("footer.html"); ?>

<script type="text/javascript" src="js/jquery.toaster.js"></script>
<script type="text/javascript" src="js/product.js"></script>
<script type="text/javascript" src="js/totop.js"></script>
<script type="text/javascript" src="js/jscolor.js"></script>

</html>